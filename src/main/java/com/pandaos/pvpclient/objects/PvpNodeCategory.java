package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@link PvpNodeCategory} class represents a category object on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpNodeCategory {

//    public List<PvpNodeCategory> items;
    public String type;
    public String title;
    public String link;
    public String nodeCategoryName;
    public String link_title;
    public List<PvpNode> content;

    private Map<String, Object> unknownProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        unknownProperties.put(name, value);
    }

    public boolean hasUnknownProperties() {
        return !unknownProperties.isEmpty();
    }

    /**
     * @param node the target object to parse
     * @return PvpEntry the parse result
     */
    public static PvpNodeCategory parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpNodeCategory pvpNodeCategory = mapper.treeToValue(node, PvpNodeCategory.class);
            return pvpNodeCategory;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param node the target object to parse
     * @return List the parse result
     */
    public static List<PvpNodeCategory> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<List<PvpNodeCategory>> typeRef = new TypeReference<List<PvpNodeCategory>>(){};
            List<PvpNodeCategory> pvpNodeCategories = mapper.readValue(node.traverse(), typeRef);
            return pvpNodeCategories;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
