package com.pandaos.pvpclient.objects;

/**
 * Created by orenkosto on 7/10/17.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@link PvpPurchase} class represents a category object on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpPurchase {

    public int type;
    public int service;
    public String price;
    public String objectId;
    public int status;
    public long time;
    public long purchaseTime;
    public HashMap<String, Object> info;

    private Map<String, Object> unknownProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        unknownProperties.put(name, value);
    }

    public boolean hasUnknownProperties() {
        return !unknownProperties.isEmpty();
    }

    /**
     * @param node the target object to parse
     * @return PvpEntry the parse result
     */
    public static PvpPurchase parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpPurchase pvpPurchase = mapper.treeToValue(node, PvpPurchase.class);
            return pvpPurchase;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<PvpPurchase> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<List<PvpPurchase>> typeRef = new TypeReference<List<PvpPurchase >>(){};
            List<PvpPurchase> pvpPurchases = mapper.readValue(node.traverse(), typeRef);
            return pvpPurchases;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static List<PvpPurchase> getListFromString(String str) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(str);
            List<PvpPurchase> pvpPurchaseList = PvpPurchase.parseListFromJsonNode(jsonNode.get("data"));
            return pvpPurchaseList;
        } catch (Exception e){
            return null;
        }
    }

}
