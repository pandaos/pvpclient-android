package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

/**
 * The {@link PvpYoutubeId} object represents a Mongo ID.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpYoutubeId implements Serializable {

    /**
     * The Youtube Id.
     */
    public String id = "";

    /**
     * The Youtube Url.
     */
    public String url = "";

    /**
     * The Youtube Duration.
     */
    public String duration = "";

//    public PvpYoutubeId(){
//        this.id = "";
//        this.url = "";
//        this.duration = "";
//    }
//
//    public PvpYoutubeId(String id, String url, String duration){
//        this.id = id;
//        this.url = url;
//        this.duration = duration;
//    }

    /**
     * @param node the target object to parse
     * @return PvpYoutubeId the parse result
     */
    public static PvpYoutubeId parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpYoutubeId pvpYoutubeId = mapper.treeToValue(node, PvpYoutubeId.class);
            return pvpYoutubeId;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PvpYoutubeId parseFromString(String string) {
        if(string == null) return null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        PvpYoutubeId result = null;
        try {
            JsonNode node = mapper.readTree(string);
            result = parseOneFromJsonNode(node);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        String jsonString = "";
        try {
            JsonNode node = mapper.valueToTree(this);
            jsonString = node.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonString;
    }

}