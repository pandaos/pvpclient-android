package com.pandaos.pvpclient.objects;

import android.os.Parcelable;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pandaos.pvpclient.utils.PvpMapHelper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  The {@link PvpChannel} object represents a channel on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpChannel implements Serializable {

    static final int CHANNEL_TYPE_DEFAULT = 0;
    static final int CHANNEL_TYPE_YOUTUBE = 1;

    private String id;
    /**
     * The Mongo Id.
     */
    public HashMap<String, String> _id;
    /**
     * The Name.
     */
    public String name;
    /**
     * The Instance id.
     */
    public String instanceId;
    /**
     * The Creator.
     */
    public String creator;
    /**
     * The Owner.
     */
    public String owner;
    /**
     * The Mosaic.
     */
    public String mosaic;
    /**
     * The Hls url.
     */
    public String hlsUrl;
    /**
     * The Wowza app.
     */
    public String wowzaApp;

    /**
     * The Categories ids.
     */
    public long[] categoriesIds;
    /**
     * The Updated at.
     */
    public long updatedAt;
    /**
     * The Created at.
     */
    public long createdAt;

    /**
     * The Type.
     */
    public int type;
    /**
     * The Status.
     */
    public int status;

    /**
     * The Entry.
     */
    public PvpLiveEntry entry;

    private List<PvpChannelScheduleItem> scheduleItems;

    public List<PvpChannelSchedule> schedules;

    private Map<String, Object> unknownProperties = new HashMap<String, Object>();

    /**
     * Gets the channel id.
     *
     * @return the id
     */
    public String getId() {
        try {
            return this._id.get("$id");
        } catch (Exception e) {
            return this.entry.id != null ? this.entry.id : "";
        }
    }

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        unknownProperties.put(name, value);
    }

    public boolean hasUnknownProperties() {
        return !unknownProperties.isEmpty();
    }

    public String getChannelNumberStr() {
        String result = "0";
        if (hasUnknownProperties()) {
            result = PvpMapHelper.getValueForKeyPath(new HashMap(unknownProperties), "info.ChannelNumber", "0");
        }
        return result;
    }

    public int getChannelNumber() {
        return Integer.parseInt(getChannelNumberStr());
    }

    public List<PvpChannelScheduleItem> getScheduleItems() {
        return getScheduleItems(false);
    }

    public List<PvpChannelScheduleItem> getScheduleItems(final boolean includeAds) {
        if (scheduleItems == null || scheduleItems.size() == 0) {
            processScheduleItems();
//            if (includeAds) { //TODO: add handling of ads
//            } else {
//                return scheduleItems.where().equalTo("isAd", false).findAll();
//            }
        }
        return scheduleItems;
    }

    public void setScheduleItems(List<PvpChannelScheduleItem> items) {
        scheduleItems = new ArrayList<>(items);
    }

    private void processScheduleItems() {
        boolean isEmpty = false;
        if (this.scheduleItems == null) {
            this.scheduleItems = new ArrayList<>();
            isEmpty = true;
        } else {
//            this.scheduleItems.clear();
//            isEmpty = true;
        }

        if(this.scheduleItems.size() == 0) {
            isEmpty = true;
        }


        if (this.schedules != null && this.schedules.size() > 0) {
            for (PvpChannelSchedule schedule : this.schedules) {
                if (schedule.scheduleItems != null && schedule.scheduleItems.size() > 0) {
                    for (PvpChannelScheduleItem item : schedule.scheduleItems) {
//                        if (includeAds || (item.entry != null && !item.entry.isAd())) { //TODO: add handling of ads
                            item.scheduleStartTime = schedule.date;
                            if(isEmpty ) {
                                this.scheduleItems.add(item);
                            }
//                        }
                    }
                }
            }
        }
    }

    public int getCurrentItemPlayingPosition(long serverTimeOffset) {
        long currentTime = (System.currentTimeMillis() / 1000L) - serverTimeOffset;
        int position = 0;
        for (PvpChannelScheduleItem item : scheduleItems) {
            if (item.getStartTimeFromSchedule() < currentTime && item.getEndTimeFromSchedule() > currentTime) {
                return position;
            }
            position++;
        }
        return 0;
    }

    public int getItemPlayingPosition(long serverTimeOffset, long currentOffset) {
        long currentTime = (System.currentTimeMillis() / 1000L) - serverTimeOffset - currentOffset;
        int position = 0;
        for (PvpChannelScheduleItem item : scheduleItems) {
            if (item.getStartTimeFromSchedule() < currentTime && item.getEndTimeFromSchedule() > currentTime) {
                return position;
            }
            position++;
        }
        return 0;
    }


   public PvpChannelScheduleItem getCurrentItem(long serverTimeOffset) {
        return scheduleItems.get(getCurrentItemPlayingPosition(serverTimeOffset));
    }

    public boolean isYoutubeChannel() {
        return  type == CHANNEL_TYPE_YOUTUBE;
    }


    public PvpYoutubeId getScheduledYoutubeId(long serverTimeOffset) {
        PvpChannelScheduleItem currentItem = getCurrentItem(serverTimeOffset);
        if(currentItem.entry!=null && currentItem.entry.info !=null && !TextUtils.isEmpty(currentItem.entry.info.youtube)) {
            return  PvpYoutubeId.parseFromString(currentItem.entry.info.youtube);
        }

        return null;
    }

    public PvpYoutubeId getScheduledYoutubeId(int position) {
        PvpChannelScheduleItem currentItem = scheduleItems.get(position);
        if(currentItem.entry!=null && currentItem.entry.info !=null && !TextUtils.isEmpty(currentItem.entry.info.youtube)) {
            return  PvpYoutubeId.parseFromString(currentItem.entry.info.youtube);
        }

        return null;
    }

    /**
     * Parse list from json node array list.
     *
     * @param node the target object to parse
     * @return List the parse result
     */
    public static ArrayList<PvpChannel> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<ArrayList<PvpChannel>> typeRef = new TypeReference<ArrayList<PvpChannel>>(){};
            ArrayList<PvpChannel> channelList = mapper.readValue(node.traverse(), typeRef);
            for (PvpChannel channel : channelList) {
                channel.processScheduleItems();
            }

            return channelList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse one from json node pvp channel.
     *
     * @param node the target object to parse
     * @return PvpEntry the parse result
     */
    public static PvpChannel parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpChannel channel = mapper.treeToValue(node, PvpChannel.class);
            channel.processScheduleItems();
            return channel;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse from string pvp channel.
     *
     * @param string the string
     * @return the pvp config
     */
    public static PvpChannel parseFromString(String string) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        PvpChannel result = null;
        try {
            JsonNode node = mapper.readTree(string);
            result = parseOneFromJsonNode(node);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @NonNull
    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        String jsonString = "";
        try {
            JsonNode node = mapper.valueToTree(this);
            jsonString = node.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    public boolean hasEpgLabel () {
        boolean hasInfo = unknownProperties.containsKey("info");
        if(hasInfo) {
            Object info = unknownProperties.get("info");
            if(info instanceof  HashMap) {
                boolean hasEpgLabel = ((HashMap) info).containsKey("epgLabel");
                if(hasEpgLabel) {
                    return true;
                }
            }

        }
        return false ;
    }

    public String getEpgLabel() {
        if(!hasEpgLabel()) return  null;
        HashMap info = (HashMap) unknownProperties.get("info");
        return (String) info.get("epgLabel");
    }
}
