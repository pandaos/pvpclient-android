package com.pandaos.pvpclient.objects;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@link PvpNode} class represents a category object on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpNode {

    public String viewType;
    public String type;
    public String categoryType;
    public String categoryName;
    public String overrideAppLogo;
    public int uid;
    public int nid;
    public String url;
    public String created;
    public String text;
    public String title;
    public String link;
    public String link_title;
    public String primaryTitle;

    public String imageUrl;
    public String imageBy;

    public String secondaryTitle;
    public String author;
    public String eventDate;
    public String eventDateText;
    public String eventLocation;
    public String eventLink;
    public String promotionText;
    public String promotionTextType;
    public String registrationText;
    public String authorImageUrl;
    public String authorPageUrl;
    public String authorTitle;
    public String authorDescription;
    public String nodeTag;
    public String nodeTagCategory;
    public String videoId;
    public ArrayList<HashMap<String,String>> tags;
    public PvpEntry entry;
    public boolean firstNodeCategory = false;
    public HashMap<Integer,String> marqueeNodes;

    public HashMap<String, String> info;

    private Map<String, Object> unknownProperties = new HashMap<String, Object>();

    public PvpNode linkedNode;

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        unknownProperties.put(name, value);
    }

    public boolean hasUnknownProperties() {
        return !unknownProperties.isEmpty();
    }

    /**
     * @param node the target object to parse
     * @return PvpNode the parse result
     */
    public static PvpNode parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpNode pvpNode = mapper.treeToValue(node, PvpNode.class);
            return pvpNode;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param node the target object to parse
     * @return List the parse result
     */
    public static List<PvpNode> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<List<PvpNode>> typeRef = new TypeReference<List<PvpNode>>(){};
            List<PvpNode> nodeList = mapper.readValue(node.traverse(), typeRef);
            return nodeList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


}
