package com.pandaos.pvpclient.objects;

import android.content.Context;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaltura.client.types.KalturaMediaEntry;
import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.models.PvpHelper_;
import com.pandaos.pvpclient.utils.PvpLocalizationHelper_;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The {@link PvpEntry} object represents a media entry on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpEntry extends KalturaMediaEntry implements Serializable {
    /**
     * The Last played timestamp.
     */
    public String lastPlayedAt;
    /**
     * The Redirect entry id.
     */
    public String redirectEntryId;

    public int status = 0;

    /**
     * The Additional Info.
     */
    public PvpEntryInfo info;
    /**
     * The Description.
     */
    public String description;
    /**
     * The Cue point list.
     */
    public List<PvpCuePoint> cuePointList;

    private Map<String, Object> unknownProperties = new HashMap<String, Object>();

    public PvpEntry() {
        super();
    }

    public PvpEntry(String entryId) {
        this.id = entryId;
    }

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        unknownProperties.put(name, value);
    }

    public boolean hasUnknownProperties() {
        return !unknownProperties.isEmpty();
    }

    @Nullable
    public String getColor() {
        if (info != null && info.getUnknownProperties().get("colorPallet") != null) {
            return (String) info.getUnknownProperties().get("colorPallet");
        } else {
            return null;
        }
    }

    public String getName(@NonNull Context context) {
        String language = PvpLocalizationHelper_.getInstance_(context).getLanguage();
        String finalName;
        try {
            finalName = this.info.getUnknownProperties().get(language + "_name");
        } catch (Exception e) {
            finalName = this.name;
        }
        if (finalName == null || finalName.length() == 0) {
            finalName = this.name;
        }
        return finalName;
    }

    public String getDescription(@NonNull Context context) {
        String language = PvpLocalizationHelper_.getInstance_(context).getLanguage();
        String finalDesc;
        try {
            finalDesc = this.info.getUnknownProperties().get(language + "_description");
        } catch (Exception e) {
            finalDesc = this.description;
        }
        if (finalDesc == null || finalDesc.length() == 0) {
            finalDesc = this.description;
        }
        return finalDesc;
    }

    public String getTags(@NonNull Context context) {
        String language = PvpLocalizationHelper_.getInstance_(context).getLanguage();
        String finalTags;
        try {
            finalTags = this.info.getUnknownProperties().get(language + "_tags");
        } catch (Exception e) {
            finalTags = this.tags;
        }
        if (finalTags == null || finalTags.length() == 0) {
            finalTags = this.name;
        }
        return finalTags;
    }

    /**
     * Parse list from json node list.
     *
     * @param node the target object to parse
     * @return List the parse result
     */
    public static List<PvpEntry> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<List<PvpEntry>> typeRef = new TypeReference<List<PvpEntry>>(){};
            List<PvpEntry> entryList = mapper.readValue(node.traverse(), typeRef);
            return entryList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse one from json node pvp entry.
     *
     * @param node the target object to parse
     * @return PvpEntry the parse result
     */
    public static PvpEntry parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpEntry entry = mapper.treeToValue(node, PvpEntry.class);
            return entry;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PvpEntry initFromLiveEntry(PvpLiveEntry liveEntry) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.valueToTree(liveEntry);
        return PvpEntry.parseOneFromJsonNode(node);
    }

    /**
     * Generates and returns a download Uri for the entry's video.
     *<pre>
     * Example:
     *{@code
     * PvpEntry entry = ...; //retrieve the required entry from somewhere
     * Uri downloadUrl = entry.generateVideoDownloadURL(this, 0);
     *}
     *</pre>
     *
     * @param context - App context
     * @param flavor  - The flavor represents the quality of the video. Use 0 for the full resolution source file.
     * @return Uri for downloading the video
     */
    public Uri generateVideoDownloadURL(Context context, int flavor) {
        final String selectedFlavor = Integer.toString(flavor);
        PvpHelper pvpHelper = PvpHelper_.getInstance_(context);
        PvpConfig config = pvpHelper.getConfig();

        String kalturaServiceUrl = (String) config.kaltura.get("serviceUrl");
        String kalturaServicePartnerId = (String) config.kaltura.get("partnerId");

        String resultString = kalturaServiceUrl + "p/" + kalturaServicePartnerId + "/sp/0/playManifest/entryId/" + this.id + "/format/url/flavorParamId/" + selectedFlavor + "/userKs/" + config.userKs +  "/video.mp4";

        return Uri.parse(resultString);
    }

    /**
     * Generate thumbnail url uri.
     *
     * @param context - App context
     * @param width   - The required width for the image
     * @param height  - The required height for the image.
     * @param type    - The type of method by which the image is generated in Kaltura(crop/stretch etc..)
     * @return uri
     */
    public Uri generateThumbnailURL(Context context, int width, int height, int type) {
        PvpHelper pvpHelper = PvpHelper_.getInstance_(context);
        PvpConfig config = pvpHelper.getConfig();

        String kalturaCdnUrl = (String) config.kaltura.get("cdnUrl");
        if(!kalturaCdnUrl.endsWith("/")) {
            kalturaCdnUrl = kalturaCdnUrl + "/";
        }
        String kalturaServicePartnerId = (String) config.kaltura.get("partnerId");

        String resultString = this.thumbnailUrl;

        if(resultString != "") {
            resultString = resultString + "/width/" +Integer.toString(width) +"/height/"+ Integer.toString(height) + "/type/" + Integer.toString(type);
        } else {
            resultString = kalturaCdnUrl + "p/" + kalturaServicePartnerId + "/thumbnail/entry_id/" + this.id + "/width/" +Integer.toString(width) +"/height/"+ Integer.toString(height) + "/type/" + Integer.toString(type);
        }

        return Uri.parse(resultString);
    }

    /**
     * Generates the entry duration in HH:MM:SS or MM:SS format.
     *
     * @return the entry duration in HH:MM:SS or MM:SS format, as a string.
     */
    public String formattedTime() {
        String result;
        long hours = TimeUnit.SECONDS.toHours(duration);
        
        if (hours > 0) {
            result = String.format(Locale.ENGLISH, "%02d:%02d:%02d",
                    TimeUnit.SECONDS.toHours(duration),
                    TimeUnit.SECONDS.toMinutes(duration) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(duration)),
                    TimeUnit.SECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(duration)));
        } else {
            result = String.format(Locale.ENGLISH, "%02d:%02d",
                    TimeUnit.SECONDS.toMinutes(duration) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(duration)),
                    TimeUnit.SECONDS.toSeconds(duration) - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(duration)));
        }

        return result;
    }

    public boolean isAd() {
        boolean result = false;
        String regex = ">Ad[s]?(>|$)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher;
        String[] splitCategories;

        try {
            splitCategories = this.categories.split(",");
        } catch (Exception e) {
            splitCategories = new String[0];
        }
        
        for (String splitCategory : splitCategories) {
            matcher = pattern.matcher(splitCategory);
            if (matcher.find()) {
                result = true;
                break;
            }
        }
        return result;

    }

    public String getBroadcaster() {
        if (info != null && info.getUnknownProperties().get("broadcaster") != null) {
            return ((String) info.getUnknownProperties().get("broadcaster"));
        } else {
            return "";
        }
    }

    public String getBroadcastTime() {
        if (info != null && info.getUnknownProperties().get("broadcastTime") != null) {
            return ((String) info.getUnknownProperties().get("broadcastTime"));
        } else {
            return "";
        }
    }

    /**
     * The Poster Url String.
     */
    public String posterUrlString() {
        if(info.getUnknownProperties() != null && info.getUnknownProperties().containsKey("poster") &&
                info.getUnknownProperties().get("poster") != null && !info.getUnknownProperties().get("poster").equals("")) {
            return info.getUnknownProperties().get("poster");
        }

        return null;
    }

    /**
     * Parse from string pvp entry.
     *
     * @param string the string
     * @return the pvp config
     */
    public static PvpEntry parseFromString(String string) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        PvpEntry result = null;
        try {
            JsonNode node = mapper.readTree(string);
            result = parseOneFromJsonNode(node);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        String jsonString = "";
        try {
            JsonNode node = mapper.valueToTree(this);
            jsonString = node.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonString;
    }
}
