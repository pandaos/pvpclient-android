package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

/**
 *  The {@link PvpMeta} class represents a metadata object on the Bamboo platform. This object will contain metadata of an API request, like the total results.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpMeta {
    /**
     * The total results count.
     */
    public int total = 0;
    /**
     * Token for retrieving the previous page.
     */
    public String prevPageToken = null;
    /**
     * Token for retrieving the next page.
     */
    public String nextPageToken = null;

    /**
     * Default constructor.
     */
    public PvpMeta() {

    }

    /**
     * PVPMeta constructor
     * @param total The total amount of entries (or any other object) returned from the server
     */
    public PvpMeta(int total) {
        this.total = total;
    }

    /**
     * PVPMeta constructor
     * @param total The total amount of entries (or any other object) returned from the server
     * @param prevPageToken The previous page String token
     * @param nextPageToken The next page String token.
     */
    public PvpMeta(int total, String prevPageToken, String nextPageToken) {
        this(total);
        this.prevPageToken = prevPageToken;
        this.nextPageToken = nextPageToken;
    }

    /**
     * Gets the total results count.
     * @return The total results count.
     */
    public int getTotal() {
        return this.total;
    }

    /**
     * Returns the previous page token (used for youtube entries)
     *
     * @return prevPageToken String
     */
    public String getPrevPageToken() {
        return this.prevPageToken;

    }

    /**
     * Returns the next page token (used for youtube entries)
     *
     * @return nextPageToken String
     */
    public String getNextPageToken() {
        return this.nextPageToken;

    }

    /**
     * Parses JSON to PVPMeta
     * @param node - JSON of PVPMeta object
     * @return PVPMeta object.
     */
    public static PvpMeta parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpMeta meta = mapper.treeToValue(node, PvpMeta.class);
            return meta;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
