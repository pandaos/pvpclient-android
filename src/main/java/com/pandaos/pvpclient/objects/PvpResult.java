package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by orenkosto on 8/9/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpResult {

    public String id = "";

    public String result = "";

    private Map<String, Object> unknownProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        unknownProperties.put(name, value);
    }

    public boolean hasUnknownProperties() {
        return !unknownProperties.isEmpty();
    }

    /**
     * Parse list from json node list.
     *
     * @param node the target object to parse
     * @return List the parse result
     */
    public static List<PvpResult> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<List<PvpResult>> typeRef = new TypeReference<List<PvpResult >>(){};
            List<PvpResult> pvpResults = mapper.readValue(node.traverse(), typeRef);
            return pvpResults;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse one from json node pvp user.
     *
     * @param node the target object to parse
     * @return PvpResult the parse result
     */
    public static PvpResult parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpResult pvpResult = mapper.treeToValue(node, PvpResult.class);
            return pvpResult;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
