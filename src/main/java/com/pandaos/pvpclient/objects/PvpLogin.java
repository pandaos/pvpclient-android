package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * The {@link PvpLogin} object represents a login JSON on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpLogin {

    /**
     * The Id.
     */
    public String id = "";
    /**
     * The Password.
     */
    public String password = "";

    /**
     * Construct login object
     *
     * @param id       String
     * @param password String
     */
    public PvpLogin (String id, String password) {
        this.id = id;
        this.password = password;
    }
}
