package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaltura.client.types.KalturaUser;

import java.util.List;

/**
 * The {@link PvpUser} object represents a user on the Bamboo platform. This object inherits most properties from the {@link KalturaUser} object.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpUser extends KalturaUser {

    /**
     * Build pvp user.
     *
     * @param email     the email
     * @param firstName the first name
     * @param lastName  the last name
     * @param password  the password
     * @return the pvp user
     */
    public static PvpUser build(String email, String firstName, String lastName, String password) {
        PvpUser user = new PvpUser();

        user.id = email;
        user.email = email;
        user.firstName = firstName;
        user.lastName = lastName;
        user.password = password;
        user.info = new PvpUserInfo();

        return user;
    }

    public PvpUser(){}

    public PvpUser(PvpUser user) {
        this.id = user.email;
        this.email = user.email;
        this.firstName = user.firstName;
        this.lastName = user.lastName;
        this.password = user.password;
        this.info = new PvpUserInfo(user.info);
    }

    /**
     * The enum User role.
     */
    public enum userRole {
        /**
         * The Viewer role.
         */
        viewerRole {
            public String toString() {
                return "viewerRole";
            }
        },
        /**
         * The Contributor role.
         */
        contributorRole {
            public String toString() {
                return "contributorRole";
            }
        },
        /**
         * The Manager role
         */
        managerRole {
            public String toString() {
                return "managerRole";
            }
        },
        /**
         * The Admin role.
         */
        adminRole {
            public String toString() {
                return "adminRole";
            }
        },
        /**
         * The Root role.
         */
        rootRole {
            public String toString() {
                return "rootRole";
            }
        },
    }

    /**
     * The Mongo Id.
     */
    public PvpMongoId _id;

    /**
     * The Id.
     */
    public String id;
    /**
     * The Password.
     */
    public String password;
    /**
     * The Role.
     */
    public String role;

    /**
     * The First name.
     */
    public String firstName;
    /**
     * The Last name.
     */
    public String lastName;

    /**
     * The User Info.
     */
    public PvpUserInfo info;

    /**
     * The Instance.
     */
    public String instance;

    public void setUserPromoCode(String promoCode) {
        if(this.info == null) {
            this.info = new PvpUserInfo();
        }

        this.info.promoCode = promoCode;
    }

    public String getFullName() {
        String userFullName = this.fullName;
        if (userFullName == null || userFullName.length() == 0) {
            userFullName = this.firstName + " " + this.lastName;
        }

        return userFullName;
    }

    /**
     * Parse list from json node list.
     *
     * @param node the target object to parse
     * @return List the parse result
     */
    public static List<PvpUser> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<List<PvpUser>> typeRef = new TypeReference<List<PvpUser >>(){};
            List<PvpUser> pvpUsers = mapper.readValue(node.traverse(), typeRef);
            return pvpUsers;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse one from json node pvp user.
     *
     * @param node the target object to parse
     * @return PvpEntry the parse result
     */
    public static PvpUser parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            if(node != null) {
                PvpUser pvpUser = mapper.treeToValue(node, PvpUser.class);
                return pvpUser;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    public static PvpUser parseFromString(String string) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        PvpUser result = null;
        try {
            JsonNode node = mapper.readTree(string);
            result = parseOneFromJsonNode(node);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        String jsonString = "";
        try {
            JsonNode node = mapper.valueToTree(this);
            jsonString = node.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonString;
    }

    public boolean bypassCountryRestriction() {
        boolean result = false;

        try {
            if (info != null) {
                result = info.bypassCountryRestriction;
            }
        } catch (Exception e) {}

        return result;
    }

    public boolean bypassSchedulingRestriction() {
        boolean result = false;

        try {
            if (info != null) {
                result = info.bypassSchedulingRestriction;
            }
        } catch (Exception e) {}

        return result;
    }

    public boolean bypassPurchaseRestriction() {
        boolean result = false;

        try {
            if (info != null) {
                result = info.bypassPurchaseRestriction;
            }
        } catch (Exception e) {}

        return result;
    }

    public boolean hasRole(userRole role) {
        try {
            return this.role != null && userRole.valueOf(this.role).ordinal() >= role.ordinal();
        } catch (Exception e ) {
            return false; //can be thrown also in case of invalid values supplied to the
        }
    }

    public boolean isSpecialRightsUser() {
        try {
            return this.role != null && (this.role.equals(userRole.adminRole.toString()) || this.role.equals(userRole.managerRole.toString()) || this.role.equals(userRole.rootRole.toString()));
        } catch (Exception e ) {
            return false; //can be thrown also in case of invalid values supplied to the
        }
    }
}
