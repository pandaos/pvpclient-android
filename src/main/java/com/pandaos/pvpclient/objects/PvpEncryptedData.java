package com.pandaos.pvpclient.objects;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.pandaos.pvpclient.utils.PvpEncryptionHelper;
import com.pandaos.pvpclient.utils.PvpEncryptionHelper_;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by orenkosto on 7/17/17.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpEncryptedData implements Serializable {

    public List<String> data = new ArrayList<>();

    private PvpEncryptionHelper encryptionHelper;

    public PvpEncryptedData(Context context, List<HashMap> objects) {
        super();
        encryptionHelper = PvpEncryptionHelper_.getInstance_(context);
        for (HashMap object : objects) {
            data.add(encryptionHelper.encrypt(object));
        }
    }

    public PvpEncryptedData(Context context, HashMap object) {
        super();
        encryptionHelper = PvpEncryptionHelper_.getInstance_(context);
        data.add(encryptionHelper.encrypt(object));
    }
}
