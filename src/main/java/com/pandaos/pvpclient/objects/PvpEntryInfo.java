package com.pandaos.pvpclient.objects;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * The {@link PvpEntryInfo} object represents an entry's additional info on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpEntryInfo implements Serializable{
    /**
     * The Price.
     */
    public String price = "";
    /**
     * The Apple event product.
     */
    public String appleEventProduct = "";
    /**
     * The Google event product.
     */
    public String googleEventProduct = "";
    /**
     * The Is private.
     */
    public int isPrivate = 0;
    /**
     * The Has ended.
     */
    public int hasEnded = 0;
    /**
     * The Package entry id.
     */
    public String packageEntryId = null;

    // Youtube entery comes as String instead of json object!
    public String youtube = null;

    @Nullable
    public String hlsUrl = null;

    public int watchState;
    public long watchTime;

    private Map<String, String> unknownProperties = new HashMap<String, String>();

    /**
     * The Package id.
     * This field is returned as "package" from the API, but is mapped to "packageId" because "package" is a reserved word in Java.
     */
    @JsonProperty("package")
    public String packageId = "";

    @JsonAnyGetter
    public Map<String, String> getUnknownProperties() {
        return unknownProperties;
    }

    @JsonAnySetter
    public void set(String name, String value) {
        unknownProperties.put(name, value);
    }

    public boolean hasUnknownProperties() {
        return !unknownProperties.isEmpty();
    }

//    /**
//     * @param node the target object to parse
//     * @return List the parse result
//     */
//    public static List<PvpEntryInfo> parseListFromJsonNode(JsonNode node) {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);
//
//        try {
//            TypeReference<List<PvpEntryInfo>> typeRef = new TypeReference<List<PvpEntryInfo>>(){};
//            List<PvpEntryInfo> entryList = mapper.readValue(node.traverse(), typeRef);
//            return entryList;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
//
//    /**
//     * @param node the target object to parse
//     * @return PvpEntry the parse result
//     */
//    public static PvpEntryInfo parseOneFromJsonNode(JsonNode node) {
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);
//
//        try {
//            PvpEntryInfo entry = mapper.treeToValue(node, PvpEntryInfo.class);
//            return entry;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
//    }

    public PvpYoutubeId getYoutubeId() {
        return PvpYoutubeId.parseFromString(youtube);
    }

}
