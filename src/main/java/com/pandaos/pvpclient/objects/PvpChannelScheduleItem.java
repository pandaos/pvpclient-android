package com.pandaos.pvpclient.objects;

import androidx.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

/**
 * The {@link PvpChannelScheduleItem} object represents a channel schedule item on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpChannelScheduleItem {

    /**
     * The type.
     */
    public int type;
    /**
     * The Start time.
     */
    public int startTime;
    /**
     * The end time.
     */
    public int endTime;
    /**
     * The Entry.
     */
    public PvpEntry entry;

    public int scheduleStartTime = 0;

    public int alignedStartTime = 0;
    public int alignedEndTime = 0;

    public int getStartTimeFromSchedule() {
        if (alignedStartTime != 0) {
            return alignedStartTime;
        }
        return this.startTime + this.scheduleStartTime;
    }

    public int getEndTimeFromSchedule() {
        if (alignedEndTime != 0) {
            return alignedEndTime;
        }
        return this.endTime + this.scheduleStartTime;
    }

    public long getSeekOffset(long serverOffset) {
        long currentTime = (System.currentTimeMillis() / 1000L) - serverOffset;
        return currentTime - getStartTimeFromSchedule();
    }

    /**
     * Parse list from json node array list.
     *
     * @param node the target object to parse
     * @return List the parse result
     */
    public static ArrayList<PvpChannelScheduleItem> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<ArrayList<PvpChannelScheduleItem>> typeRef = new TypeReference<ArrayList<PvpChannelScheduleItem>>(){};
            ArrayList<PvpChannelScheduleItem> scheduleItemList = mapper.readValue(node.traverse(), typeRef);

            return scheduleItemList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse one from json node pvp channel schedule item.
     *
     * @param node the target object to parse
     * @return PvpEntry the parse result
     */
    public static PvpChannelScheduleItem parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpChannelScheduleItem scheduleItem = mapper.treeToValue(node, PvpChannelScheduleItem.class);
            return scheduleItem;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @NonNull
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("start = ").append(startTime);
        builder.append(" end = ").append(endTime);
        builder.append(" scheduleStart = ").append(scheduleStartTime);
        builder.append(" alignedStart = ").append(alignedStartTime);
        builder.append(" alignedEnd = ").append(alignedEndTime);
        return builder.toString();
    }

}
