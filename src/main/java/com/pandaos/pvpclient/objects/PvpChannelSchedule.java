package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;

/**
 *  The {@link PvpChannelSchedule} object represents a channel schedule on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpChannelSchedule {
    public int type;
    public int status;
    public int date;
    public String name;
    public String creator;
    public String owner;
    public String instanceId;
    public String channel;
    public ArrayList<PvpChannelScheduleItem> scheduleItems;

    public static ArrayList<PvpChannelSchedule> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<ArrayList<PvpChannelSchedule>> typeRef = new TypeReference<ArrayList<PvpChannelSchedule>>(){};
            ArrayList<PvpChannelSchedule> scheduleList = mapper.readValue(node.traverse(), typeRef);

            return scheduleList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PvpChannelSchedule parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpChannelSchedule schedule = mapper.treeToValue(node, PvpChannelSchedule.class);
            return schedule;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
