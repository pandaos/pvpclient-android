package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;

/**
 * The {@link PvpUserInfo} object represents a PVPUser additional info. every PVP user has this object under the "info" field.
 *
 * @see PvpUser
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpUserInfo  implements Serializable {

    public PvpUserInfo(){
        this.address = "";
        this.phone = "";
        this.profilePictureUrl = "";
        this.hasAcceptedTerms = "";
        this.promoCode = "";
        this.bypassCountryRestriction = false;
        this.bypassSchedulingRestriction = false;
        this.bypassPurchaseRestriction = false;
    }

    public PvpUserInfo(PvpUserInfo userInfo){
        this.address = userInfo.address;
        this.phone = userInfo.phone;
        this.profilePictureUrl = userInfo.profilePictureUrl;
        this.hasAcceptedTerms = userInfo.hasAcceptedTerms;
        this.promoCode = userInfo.promoCode;
        this.bypassCountryRestriction = userInfo.bypassCountryRestriction;
        this.bypassSchedulingRestriction = userInfo.bypassSchedulingRestriction;
        this.bypassPurchaseRestriction = userInfo.bypassPurchaseRestriction;
    }

    /**
     * The Address.
     */
    public String address = "";

    /**
     * The Phone.
     */
    public String phone = "";

    /**
     * The Profile picture url.
     */
    public String profilePictureUrl = "";

    /**
     * The Has accepted terms flag.
     */
    public String hasAcceptedTerms = "";

    /**
     * The Has accepted terms flag.
     */
    public String promoCode = "";

    public boolean bypassCountryRestriction = false;
    public boolean bypassSchedulingRestriction = false;
    public boolean bypassPurchaseRestriction = false;

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        String jsonString = "";
        try {
            JsonNode node = mapper.valueToTree(this);
            jsonString = node.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonString;
    }

//    public Map<String, Object> deviceTokens = null;
//    public Map<String, Object> subscription = null;
//    public Map<String, Object> products = null;
}