package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

/**
 * The {@link PvpConfig} object represents an instance configuration on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpConfig {

    /**
     * The User ks.
     */
    public String userKs = "";
    /**
     * The Current instance id.
     */
    public String currentInstanceId = "";
    /**
     * The Kaltura config.
     */
    public Map<String, Object> kaltura = new HashMap<String, Object>();
    /**
     * The Player config.
     */
    public Map<String, Object> player = new HashMap<String, Object>();
    /**
     * The Live config.
     */
    public Map<String, Object> live = new HashMap<String, Object>();
    /**
     * The Pushwoosh config.
     */
    public Map<String, Object> pushwoosh = new HashMap<String, Object>();
    /**
     * The Сhannels config.
     */
    public Map<String, Object> channels = new HashMap<String, Object>();
    /**
     * The Analytics config.
     */
    public Map<String, Object> analytics = new HashMap<String, Object>();
    /**
     * The Purchases config.
     */
    public Map<String, Object> purchases = new HashMap<String, Object>();
    /**
     * The Home config.
     */
    public Map<String, Object> home = new HashMap<String, Object>();
    /**
     * The Application config.
     */
    public Map<String, Object> application = new HashMap<String, Object>();
    /**
     * The Mobile config.
     */
    public Map<String, Object> mobile = new HashMap<String, Object>();
    /**
     * The Branding config.
     */
    public Map<String, Object> branding = new HashMap<String, Object>();
    /**
     * The Email config.
     */
    public Map<String, Object> email = new HashMap<String, Object>();

    public Map<String, Object> drm = new HashMap<String, Object>();
    public Map<String, Object> user = new HashMap<String, Object>();

    /**
     * The Social config.
     */
    public Map<String, Object> social = new HashMap<String, Object>();

    /**
     * Parse from string pvp config.
     *
     * @param string the string
     * @return the pvp config
     */
    public static PvpConfig parseFromString(String string) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        PvpConfig result = null;
        try {
            JsonNode node = mapper.readTree(string);
            result = parseFromJsonNode(node);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Parse from json node pvp config.
     *
     * @param node the target object to parse
     * @return config the parse result
     */
    public static PvpConfig parseFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        if (node == null) {
            return null;
        }
        try {
            PvpConfig config = mapper.treeToValue(node, PvpConfig.class);
            return config;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        String jsonString = "";
        try {
            JsonNode node = mapper.valueToTree(this);
            jsonString = node.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonString;
    }
}