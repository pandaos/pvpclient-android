package com.pandaos.pvpclient.objects;

import androidx.annotation.Nullable;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.Charset;

public class PvpHttpClientErrorException extends HttpClientErrorException {
    private HttpHeaders httpHeaders;

    public PvpHttpClientErrorException(HttpStatus statusCode, HttpHeaders httpHeaders) {
        super(statusCode);
        this.httpHeaders = httpHeaders;
    }

    public PvpHttpClientErrorException(HttpStatus statusCode, String statusText, HttpHeaders httpHeaders) {
        super(statusCode, statusText);
        this.httpHeaders = httpHeaders;
    }

    public PvpHttpClientErrorException(HttpStatus statusCode, String statusText, byte[] responseBody, Charset responseCharset, HttpHeaders httpHeaders) {
        super(statusCode, statusText, responseBody, responseCharset);
        this.httpHeaders = httpHeaders;
    }

    @Nullable
    public String getFirstHeader(String headerName) {
        return httpHeaders.getFirst(headerName);
    }
}
