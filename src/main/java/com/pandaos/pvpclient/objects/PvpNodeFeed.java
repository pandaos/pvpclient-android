package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@link PvpNodeFeed} class represents a category object on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpNodeFeed {

    public List<PvpNodeCategory> items;
    public Map<String, String> info;

    private Map<String, Object> unknownProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        unknownProperties.put(name, value);
    }

    public boolean hasUnknownProperties() {
        return !unknownProperties.isEmpty();
    }

    /**
     * @param node the target object to parse
     * @return PvpNodeFeed the parse result
     */
    public static PvpNodeFeed parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpNodeFeed pvpNodeCategory = mapper.treeToValue(node, PvpNodeFeed.class);
            return pvpNodeCategory;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
