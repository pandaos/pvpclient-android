package com.pandaos.pvpclient.objects;

/**
 * Created by orenkosto on 7/10/17.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@link PvpPackage} class represents a category object on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpPackage {

    public PvpMongoId _id;

    public String name;
    public String description;
    public String price;
    public String thumbnailUrl;

    public HashMap<String, String> info;

    private Map<String, Object> unknownProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        unknownProperties.put(name, value);
    }

    public boolean hasUnknownProperties() {
        return !unknownProperties.isEmpty();
    }

    /**
     * Parse list from json node list.
     *
     * @param node the target object to parse
     * @return List the parse result
     */
    public static List<PvpPackage> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<List<PvpPackage>> typeRef = new TypeReference<List<PvpPackage>>(){};
            return mapper.readValue(node.traverse(), typeRef);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param node the target object to parse
     * @return PvpEntry the parse result
     */
    public static PvpPackage parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpPackage pvpPackage = mapper.treeToValue(node, PvpPackage.class);
            return pvpPackage;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getAndroidProductId() {
        String result = "";
        try {
            result = info.get("androidProductId");
        } catch (Exception e) {

        }
        return result;
    }
}
