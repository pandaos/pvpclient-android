package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.List;


/**
 *  The {@link PvpCuePoint} object represents an entry cue point on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpCuePoint implements Serializable {
    /**
     * The Text.
     */
    public String text;
    /**
     * The Parent id.
     */
    public String parentId;
    /**
     * The End time.
     */
    public int endTime;
    /**
     * The Duration.
     */
    public int duration;
    /**
     * The Depth.
     */
    public int depth;
    /**
     * The Children count.
     */
    public int childrenCount;
    /**
     * The Direct children count.
     */
    public int directChildrenCount;

    /**
     * Parse list from json node list.
     *
     * @param node the target object to parse
     * @return List the parse result
     */
    public static List<PvpCuePoint> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<List<PvpCuePoint>> typeRef = new TypeReference<List<PvpCuePoint>>(){};
            List<PvpCuePoint> cuePointList = mapper.readValue(node.traverse(), typeRef);
            return cuePointList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
