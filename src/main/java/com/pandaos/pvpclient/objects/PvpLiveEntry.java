package com.pandaos.pvpclient.objects;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kaltura.client.types.KalturaLiveStreamEntry;
import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.models.PvpHelper_;
import com.pandaos.pvpclient.utils.PvpLocalizationHelper_;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  The {@link PvpLiveEntry} object represents a live entry on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpLiveEntry extends KalturaLiveStreamEntry implements Serializable {

    /**
     * The last played timestamp.
     */
    public String lastPlayedAt;

    /**
     * The redirect entry ID.
     */
    public String redirectEntryId;

    /**
     * The entry additional info.
     */
    public PvpEntryInfo info;

    private Map<String, Object> unknownProperties = new HashMap<String, Object>();

    @JsonAnyGetter
    public Map<String, Object> getUnknownProperties() {
        return unknownProperties;
    }

    @JsonAnySetter
    public void set(String name, Object value) {
        unknownProperties.put(name, value);
    }

    public boolean hasUnknownProperties() {
        return !unknownProperties.isEmpty();
    }

    /**
     * @param node the target object to parse
     * @return List the parse result
     */
    public static List<PvpLiveEntry> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<List<PvpLiveEntry>> typeRef = new TypeReference<List<PvpLiveEntry>>(){};
            List<PvpLiveEntry> entryList = mapper.readValue(node.traverse(), typeRef);
            return entryList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param node the target object to parse
     * @return PvpEntry the parse result
     */
    public static PvpLiveEntry parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpLiveEntry entry = mapper.treeToValue(node, PvpLiveEntry.class);
            return entry;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PvpLiveEntry initFromEntry(PvpEntry entry) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = mapper.valueToTree(entry);
        return PvpLiveEntry.parseOneFromJsonNode(node);
    }

    public String getName(@NonNull Context context) {
        String language = PvpLocalizationHelper_.getInstance_(context).getLanguage();
        String finalName;
        try {
            finalName = this.info.getUnknownProperties().get(language + "_name");
        } catch (Exception e) {
            finalName = this.name;
        }
        if (finalName == null || finalName.length() == 0) {
            finalName = this.name;
        }
        return finalName;
    }

    public String getDescription(@NonNull Context context) {
        String language = PvpLocalizationHelper_.getInstance_(context).getLanguage();
        String finalDesc;
        try {
            finalDesc = this.info.getUnknownProperties().get(language + "_description");
        } catch (Exception e) {
            finalDesc = this.description;
        }
        if (finalDesc == null || finalDesc.length() == 0) {
            finalDesc = this.description;
        }
        return finalDesc;
    }
    /**
     *
     * @param context - App context
     * @param width - The required width for the image
     * @param height - The required height for the image.
     * @param type - The type of method by which the image is generated in Kaltura(crop/stretch etc..)
     * @return
     */
    public Uri generateThumbnailURL(Context context, int width, int height, int type) {
        PvpHelper pvpHelper = PvpHelper_.getInstance_(context);
        PvpConfig config = pvpHelper.getConfig();

        String kalturaCdnUrl = (String) config.kaltura.get("cdnUrl");
        if(!kalturaCdnUrl.endsWith("/")) {
            kalturaCdnUrl = kalturaCdnUrl + "/";
        }
        String kalturaServicePartnerId = (String) config.kaltura.get("partnerId");

        String resultString = kalturaCdnUrl + "p/" + kalturaServicePartnerId + "/thumbnail/entry_id/" + this.id + "/width/" +Integer.toString(width) +"/height/"+ Integer.toString(height) + "/type/" + Integer.toString(type);

        return Uri.parse(resultString);
    }

    /**
     * Parse from string pvp live entry.
     *
     * @param string the string
     * @return the pvp config
     */
    public static PvpLiveEntry parseFromString(String string) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        PvpLiveEntry result = null;
        try {
            JsonNode node = mapper.readTree(string);
            result = parseOneFromJsonNode(node);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        String jsonString = "";
        try {
            JsonNode node = mapper.valueToTree(this);
            jsonString = node.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonString;
    }
}
