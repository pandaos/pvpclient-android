package com.pandaos.pvpclient.objects;

import android.graphics.Color;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * The {@link PvpCategory} class represents a category object on the Bamboo platform.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpCategory implements Serializable {

    /**
     * The Id.
     */
    public int id;
    /**
     * The Parent id.
     */
    public int parentId;
    /**
     * The Depth.
     */
    public int depth;
    /**
     * The Parenter id.
     */
    public int partnerId;
    /**
     * The Entries count.
     */
    public int entriesCount;
    /**
     * The Status.
     */
    public int status;
    /**
     * The Privacy.
     */
    public int privacy;

    /**
     * The Created at.
     */
    public long createdAt;
    /**
     * The Updated at.
     */
    public long updatedAt;

    /**
     * The Name.
     */
    public String name;
    /**
     * The Description.
     */
    public String description;
    /**
     * The Full name.
     */
    public String fullName;
    /**
     * The Fullids.
     */
    public String fullids;

    /**
     * The Children categories.
     */
    public ArrayList<PvpCategory> children;
    /**
     * The Entries.
     */
    public List<PvpEntry> entries;

    public Map<String, Object> info;

    /**
     * Parse list from json node array list.
     *
     * @param node the target object to parse
     * @return List the parse result
     */
    public static ArrayList<PvpCategory> parseListFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            TypeReference<ArrayList<PvpCategory>> typeRef = new TypeReference<ArrayList<PvpCategory>>(){};
            ArrayList<PvpCategory> category = mapper.readValue(node.traverse(), typeRef);
            return category;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse one from json node pvp category.
     *
     * @param node the target object to parse
     * @return PvpEntry the parse result
     */
    public static PvpCategory parseOneFromJsonNode(JsonNode node) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true);

        try {
            PvpCategory category = mapper.treeToValue(node, PvpCategory.class);
            return category;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * The Poster Url String.
     */
    public String posterUrlString() {
        if(info != null && info.containsKey("poster") && info.get("poster") != null && !info.get("poster").equals("")) {
            return (String)info.get("poster");
        }

        return null;
    }

    public String getCategoryName() {
        if (info != null && info.get("label") != null) {
            return ((String) info.get("label"));
        } else if (name != null){
            return name;
        } else {
            return "";
        }
    }

    public String getBroadcaster() {
        if (info != null && info.get("broadcaster") != null) {
            return ((String) info.get("broadcaster"));
        } else {
            return "";
        }
    }

    public String getBroadcastTime() {
        if (info != null && info.get("broadcastTime") != null) {
            return ((String) info.get("broadcastTime"));
        } else {
            return "";
        }
    }

    public String getCategoryDescription() {
        if (description != null){
            return description;
        } else {
            return "";
        }
    }

    @Nullable
    public String getThumbnailUrl() {
        if (info != null && info.get("thumbnailUrl") != null) {
            return ((String) info.get("thumbnailUrl"));
        } else {
            return null;
        }
    }

    @Nullable
    public String getColor() {
        if (info != null && info.get("colorPallet") != null) {
            return (String) info.get("colorPallet");
        } else {
            return null;
        }
    }
}