package com.pandaos.pvpclient.objects;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * The {@link PvpMongoId} object represents a Mongo ID.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PvpMongoId implements Serializable {

    /**
     * The Id.
     */
    public String $id;

    /**
     * Get mongo Id
     *
     * @return the Mongo ID
     */
    public String getMongoId() {
        return $id;
    }
}