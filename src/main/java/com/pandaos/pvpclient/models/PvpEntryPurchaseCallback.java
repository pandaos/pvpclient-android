package com.pandaos.pvpclient.models;

/**
 * Created by orenkosto on 7/10/17.
 */

public interface PvpEntryPurchaseCallback {

    /**
     * Entry purchase success
     */
    void entryPurchaseSuccess();

    /**
     * Entry purchase fail
     */
    void entryPurchaseFail();
}
