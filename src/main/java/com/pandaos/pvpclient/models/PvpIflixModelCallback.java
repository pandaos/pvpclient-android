package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * The {@link PvpIflixModelCallback} provides an interface for responding to events from the {@link PvpUserModel}.
 */
public interface PvpIflixModelCallback {

    /**
     * On voucher success
     */
    void voucherRequestSuccess(JsonNode voucher);

    /**
     * On voucher failed
     */
    void voucherRequestFail();
}
