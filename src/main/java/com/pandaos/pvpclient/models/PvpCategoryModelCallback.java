package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpCategory;

import java.util.ArrayList;

/**
 * The {@link PvpCategoryModelCallback} provides an interface for responding to events from the {@link PvpCategoryModel}.
 */
public interface PvpCategoryModelCallback {
    /**
     * Category request success.
     *
     * @param category the category
     */
    void categoryRequestSuccess(PvpCategory category);

    /**
     * Category request success.
     *
     * @param categories the categories
     */
    void categoryRequestSuccess(ArrayList<PvpCategory> categories);

    /**
     * Category request fail.
     */
    void categoryRequestFail();
}