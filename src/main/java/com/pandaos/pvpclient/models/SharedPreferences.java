package com.pandaos.pvpclient.models;

import org.androidannotations.annotations.sharedpreferences.DefaultBoolean;
import org.androidannotations.annotations.sharedpreferences.DefaultLong;
import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

/**
 * The PVPClient shared preferences.
 */
@SharedPref(SharedPref.Scope.UNIQUE)
public interface SharedPreferences {

    /**
     * KS string.
     *
     * @return the KS.
     */
    @DefaultString("")
    String ks();

    /**
     * Session Token string.
     *
     * @return the session token.
     */
    @DefaultString("")
    String token();

    /**
     * Logged in user id string.
     *
     * @return the current logged in user ID.
     */
    @DefaultString("")
    String loggedInUserId();

    @DefaultString("")
    String currentUser();

    @DefaultLong(0)
    long serverTimeOffset();

    @DefaultBoolean(false)
    boolean serverTimeOffsetEnabled();

    @DefaultString("")
    String currentCountry();

    @DefaultString("")
    String currentCountryGps();

    @DefaultString("")
    String userDeviceId();

    @DefaultString("")
    String userAdvertisingId();

    /**
     * Logged in user password string.
     *
     * @return the current logged in user password.
     */
    @DefaultString("")
    String loggedInUserPass();

    @DefaultBoolean(false)
    boolean isLoggedIn();

    /**
     * Pvp server base url string.
     *
     * @return the Pvp base URL.
     */
    @DefaultString(PvpHelper.PVP_SERVER_BASE_URL)
    String pvpServerBaseUrl();

    /**
     * Pvp server base url string.
     *
     * @return the Pvp base URL.
     */
    @DefaultString("")
    String pvpInstanceId();

    /**
     * Pvp server base url string.
     *
     * @return the Pvp base URL.
     */
    @DefaultString("")
    String pvpCdnBaseUrl();


    /**
     * Current Pvp server config str string.
     *
     * @return the current Pvp server config as a JSON string.
     */
    @DefaultString("")
    String pvpServerConfigStr();

    /**
     * Debug enabled boolean.
     *
     * @return the debug enabled flag.
     */
    @DefaultBoolean(PvpHelper.PVP_SERVER_DEBUG_ENABLED)
    boolean debugEnabled();

    @DefaultString("")
    String currentLanguage();

    @DefaultString("en")
    String defaultLanguage();

    @DefaultBoolean(false)
    boolean rtlEnabled();

    @DefaultBoolean(false)
    boolean hasVisitedApp();

    @DefaultBoolean(false)
    boolean onBoardingVisited();

    @DefaultString("")
    String purchasesSharedSecret();

    @DefaultString("")
    String currentlyPlayingItemId();

    @DefaultString("channel")
    String currentlyPlayingItemType();

    @DefaultString("channel")
    String userPurchases();
}
