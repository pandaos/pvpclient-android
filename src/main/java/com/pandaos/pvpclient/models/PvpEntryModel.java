package com.pandaos.pvpclient.models;

import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpCuePoint;
import com.pandaos.pvpclient.objects.PvpEntry;
import com.pandaos.pvpclient.objects.PvpLiveEntry;
import com.pandaos.pvpclient.objects.PvpMeta;
import com.pandaos.pvpclient.objects.PvpPackage;
import com.pandaos.pvpclient.utils.PvpConstants;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * The {@link PvpEntryModel} provides an interface for using the PVP entry service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpEntryModel extends PvpBaseModel {

    /**
     * Get entries by category.
     * @param category the category.
     * @param page the page index.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getEntriesByCategory(String category, int page, PvpEntryModelCallback callback) {
        getEntriesByCategory(category, page, null, callback);
    }

    @Background
    public void getEntriesByCategory(String category, int page, HashMap<String, String> filter, PvpEntryModelCallback callback) {
        try {
            List<PvpEntry> entryList;
            JsonNode result;

            if(useCdnApi) {
                result = restService.getFromCdn("category/" + category + "/entries", filter(filter), pager(page), sharedPreferences.pvpInstanceId().get());
            } else {
                result = restService.get("category/" + category + "/entries", filter(filter), pager(page), sharedPreferences.pvpInstanceId().get());
            }

            entryList = PvpEntry.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.entryRequestSuccess(entryList);
            }

        } catch (RestClientException e) {
            if (callback != null) {
                callback.entryRequestFail();
            }
        }
    }

    /**
     * Get entries
     * @param page the page index.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getEntries(int page, PvpEntryModelCallback callback) {
        getEntries(page, null, callback);
    }

    /**
     * Get entries
     * @param page the page index.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getEntries(int page, String searchText, PvpEntryModelCallback callback) {
        try {
            JSONObject filter = new JSONObject(filter(null));
            List<PvpEntry> entryList;
            JsonNode result;

            if (searchText != null && searchText.length() > 0) {
                filter.put("freeText", "*" + searchText + "*");
            }

            if (pvpHelper.getConfig().mobile.containsKey("showNoContent") && (Boolean) pvpHelper.getConfig().mobile.get("showNoContent") == true) {
                filter.put("statusIn", "2,7");
            }

            if(useCdnApi) {
                result = restService.getFromCdn("entry", filter.toString(), pager(page), sharedPreferences.pvpInstanceId().get());
            } else {
                result = restService.get("entry", filter.toString(), pager(page), sharedPreferences.pvpInstanceId().get());
            }

            entryList = PvpEntry.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.entryRequestSuccess(entryList);
            }
        } catch (Exception e) {
            if(callback != null) {
                callback.entryRequestFail();
            }
        }
    }

    @Background
    public void getEntries(@NonNull String endpoint, int page, @NonNull HashMap<String, String> filterMap, @Nullable PvpEntryModelCallback callback) {
        try {
            JSONObject filter = new JSONObject(filter(null));
            Iterator iterator = filterMap.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry pair = (Map.Entry<String, String>) iterator.next();
//                if(((String)pair.getValue()).equals("now()")) {
//                    long currentTime = (System.currentTimeMillis() / 1000L) - pvpHelper.getServerTimeOffset();
//                    filter.put((String) pair.getKey(), currentTime);
//                } else {
//                }

                filter.put((String) pair.getKey(), pair.getValue());
                iterator.remove();
            }

            boolean showNoContent = pvpHelper.getConfig().mobile.containsKey("showNoContent") && (Boolean) pvpHelper.getConfig().mobile.get("showNoContent") == true;
            if (filter.opt("statusIn") != null && showNoContent) {
                filter.put("statusIn", "2,7");
            }

            long currentTime = (System.currentTimeMillis() / 1000L) - pvpHelper.getServerTimeOffset();
            filter.put("endDateGreaterThanOrEqualOrNull", currentTime);

            List<PvpEntry> entryList;
            JsonNode result;

            if(useCdnApi) {
                result = restService.getFromCdn(endpoint, filter.toString(), pager(page), sharedPreferences.pvpInstanceId().get());
            } else {
                result = restService.get(endpoint, filter.toString(), pager(page), sharedPreferences.pvpInstanceId().get());
            }

            entryList = PvpEntry.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.entryRequestSuccess(entryList);
            }
        } catch (Exception e) {
            if(callback != null) {
                callback.entryRequestFail();
            }
        }
    }

    /**
     * Get entry by id
     *
     * @param entryId the entry ID.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getEntry(String entryId, PvpEntryModelCallback callback) {
        try {

            JsonNode result;
            if(useCdnApi) {
                result = restService.getFromCdn("entry/"+entryId, sharedPreferences.pvpInstanceId().get(), "");
            } else {
                result = restService.get("entry/"+entryId, sharedPreferences.pvpInstanceId().get());
            }

            PvpEntry entry = PvpEntry.parseOneFromJsonNode(result.get("data"));
            if(callback != null) {
                callback.entryRequestSuccess(entry);
            }
        } catch (RestClientException e) {
            if(callback != null) {
                callback.entryRequestFail();
            }
        }
    }

    /**
     * Get entries with meta data
     *
     * @param serviceType the entry service type (Bamboo, Youtube, etc).
     * @param filter the filter object.
     * @param pager the pager object.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getEntriesWithMeta(int serviceType, JSONObject filter, JSONObject pager, PvpEntryModelCallback callback) {
        try {
            String serviceTypeString = Integer.toString(serviceType);

            boolean showNoContent = pvpHelper.getConfig().mobile.containsKey("showNoContent") && (Boolean) pvpHelper.getConfig().mobile.get("showNoContent") == true;
            if (filter == null) {
                filter = new JSONObject();
            }
            if (filter.opt("statusIn") != null && showNoContent) {
                filter.put ("statusIn", "2,7");
            }

            String filterString = (filter != null) ? filter.toString() : "";
            String pagerString = (pager != null) ? pager.toString() : "";

            JsonNode result;
            if(useCdnApi) {
                result = restService.getFromCdn("entry", filterString, pagerString, serviceTypeString, sharedPreferences.pvpInstanceId().get());
            } else {
                result = restService.get("entry", filterString, pagerString, serviceTypeString, sharedPreferences.pvpInstanceId().get());
            }

            List<PvpEntry> entryList = PvpEntry.parseListFromJsonNode(result.get("data"));
            PvpMeta meta = PvpMeta.parseOneFromJsonNode(result.get("meta"));

            if(callback != null) {
                callback.entryRequestSuccessWithMeta(entryList, meta);
            }
        } catch (Exception e) {
            if(callback != null) {
                callback.entryRequestFail();
            }
        }
    }

    /**
     * Create live entry.
     *
     * @param entry the entry to create.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void createLiveEntry(PvpLiveEntry entry, PvpLiveEntryModelCallback callback) {
        try{

            JsonNode result = restService.post("live", entry);
            entry = PvpLiveEntry.parseOneFromJsonNode(result.get("data"));

            if(callback != null) {
                callback.liveEntryCreateRequestSuccess(entry);
            }

        } catch (RestClientException e) {
            if(callback != null) {
                callback.liveEntryRequestFail();
            }
        }
    }

    @Background
    public void verifyPurchaseForEntry(PvpEntry entry, PvpEntryPurchaseCallback callback) {
        verifyPurchaseForEntryId(entry.id, callback);
    }

    @Background
    public void verifyPurchaseLiveForEntry(PvpLiveEntry entry, PvpEntryPurchaseCallback callback) {
        verifyPurchaseForEntryId(entry.id, callback);
    }

    @Background
    public void verifyPurchaseForEntryId(String entryId, PvpEntryPurchaseCallback callback) {
        try {
            JsonNode result = restService.getUserPurchase("user/0/purchase/" + entryId, PvpConstants.PURCHASE_OBJECT_TYPE_ENTRY, sharedPreferences.pvpInstanceId().get());
            if (callback != null) {
                callback.entryPurchaseSuccess();
            }
        } catch (Exception e) {
            if (callback != null) {
                callback.entryPurchaseFail();
            }

        }
    }

    /**
     * Get the default filter.
     *
     * @param filterObj
     * @return the default filter string.
     */
    String filter(HashMap<String, String> filterObj) {
        try {
            JSONObject filter = new JSONObject();
            if(filterObj != null) {
                Iterator iterator = filterObj.entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry pair = (Map.Entry<String, String>) iterator.next();
                    filter.put((String) pair.getKey(), pair.getValue());
                    iterator.remove();
                }

            }
            if(filterObj == null || filter.get("orderBy") == null) {
                filter.put("orderBy", "-createdAt");
            }
            boolean showNoContent = pvpHelper.getConfig().mobile.containsKey("showNoContent") && (Boolean) pvpHelper.getConfig().mobile.get("showNoContent") == true;
            if (filter.opt("statusIn") != null && showNoContent) {
                filter.put ("statusIn", "2,7");
            }
//            if(filterObj == null || filter.get("endDateGreaterThanOrEqualOrNull") == null) {
//                filter.put("endDateGreaterThanOrEqualOrNull", (System.currentTimeMillis() / 1000L) - pvpHelper.getServerTimeOffset());
//            }
            return filter.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Background
    public void getPackage(PvpEntry entry, PvpEntryPackageCallback callback) {
        if (entry.info != null && entry.info.packageId != null && entry.info.packageId.length() > 0) {
            getPackage(entry.info.packageId, callback);
        } else {
            if (callback != null) {
                callback.entryPackageRequestFail();
            }
        }
    }

    @Background
    public void getPackage(String packageId, PvpEntryPackageCallback callback) {
        try {
            JsonNode result = restService.getFromCdn("package/" + packageId, sharedPreferences.pvpInstanceId().get());
            PvpPackage pvpPackage = PvpPackage.parseOneFromJsonNode(result.get("data"));
            if (callback != null) {
                callback.entryPackageRequestSuccess(pvpPackage);
            }
        } catch (Exception e) {
            if (callback != null) {
                callback.entryPackageRequestFail();
            }
        }
    }

    /**
     * Gets cue points for entry.
     *
     * @param entryId  the entry id
     * @param callback the callback to trigger when finished.
     */
    public void getCuePointsForEntry(String entryId, PvpCuePointCallback callback) {
        try {
            JSONObject filterObject = new JSONObject();
            filterObject.put("entryIdEqual", entryId);
            String filterString = filterObject.toString();
            JsonNode result = restService.get("cuepoint", filterString, null, sharedPreferences.pvpInstanceId().get());

            List<PvpCuePoint> cuePointList = PvpCuePoint.parseListFromJsonNode(result.get("data"));
            PvpMeta meta = PvpMeta.parseOneFromJsonNode(result.get("meta"));

            if(callback != null) {
                callback.cuepointRequestSuccessWithMeta(cuePointList, meta);
            }
        } catch (RestClientException e) {
            if(callback != null) {
                callback.cuepointRequestFail();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generates a thumbnail URL for a given entry ID.
     *
     * @param entry_id - The id of the entry.
     * @param width - The required width for the image.
     * @param height - The required height for the image.
     * @param type - The type of method by which the image is generated in Kaltura(crop/stretch etc..)
     * @param time - The time of the required thumbnail in the video.
     *
     * @return The thumbnail URL for the given entry ID.
     */
    public Uri getThumbnailURL(String entry_id, int width, int height, int type, int time) {

        time /= 1000;

        String resultString = sharedPreferences.pvpServerBaseUrl().get() + PvpHelper.PVP_SERVER_API_PREFIX + "/entry/" + entry_id + "/thumbnail/" + Integer.toString(time) + "?width=" + Integer.toString(width) + "&height=" + Integer.toString(height) + "&type=" + Integer.toString(type);

        return Uri.parse(resultString);
    }
}
