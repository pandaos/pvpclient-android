package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpCategory;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * The {@link PvpUserHistoryModel} provides an interface for using the PVP user service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpUserHistoryModel extends PvpBaseModel {

    @Background
    public void getUserHistoryTree(String expand, PvpUserHistoryTreeCallback callback) {
        try {
            JsonNode result = restService.getUserHistoryTree("user/0/historytree", "true", expand, sharedPreferences.pvpInstanceId().get());
            List<PvpCategory> pvpCategoryList = PvpCategory.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.userHistoryTreeSuccess(new ArrayList<PvpCategory>(pvpCategoryList));
            }
        } catch (Exception e) {
            PvpHelper.pvpLog("Pvp_User_History_Model", "Failed to getUserHistoryTree: " + e.getMessage(), PvpHelper.logType.error, true);
            if (callback != null) {
                callback.userHistoryTreeFail();
            }
        }
    }

    @Background
    public void postUserHistory(HashMap<String, Object> item) {
        try {
            restService.post("user/0/history", sharedPreferences.pvpInstanceId().get(), item);
        } catch (Exception e) {
            PvpHelper.pvpLog("Pvp_User_History_Model", "Failed to postUserHistory: " + e.getMessage(), PvpHelper.logType.error, true);
        }
    }

    public interface PvpUserHistoryTreeCallback {

        /**
         * get user purchase success
         */
        void userHistoryTreeSuccess(ArrayList<PvpCategory> pvpCategoryList);


        /**
         * get user purchase fail
         */
        void userHistoryTreeFail();
    }

}
