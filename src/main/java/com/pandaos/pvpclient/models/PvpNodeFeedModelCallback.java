package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpNodeFeed;

import java.util.List;

/**
 * The {@link PvpNodeFeedModelCallback} provides an interface for responding to events from the {@link PvpNodeFeedModel}.
 */
public interface PvpNodeFeedModelCallback {

    /**
     * Node request success.
     *
     * @param nodeFeed the entries array.
     */
    void nodeFeedRequestSuccess(PvpNodeFeed nodeFeed);

    /**
     * Node request fail.
     */
    void nodeFeedRequestFail();
}
