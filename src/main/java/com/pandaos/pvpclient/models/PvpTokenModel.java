package com.pandaos.pvpclient.models;

import android.content.Intent;
import android.provider.Settings;
import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pandaos.pvpclient.objects.PvpHttpClientErrorException;
import com.pandaos.pvpclient.objects.PvpLogin;
import com.pandaos.pvpclient.objects.PvpUser;
import com.pandaos.pvpclient.utils.PvpConstants;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.springframework.web.client.RestClientException;

import java.util.List;

/**
 * The {@link PvpTokenModel} provides an interface for using the token service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpTokenModel extends PvpBaseModel implements PvpUserModelCallback {

    /**
     * The Token model callback.
     */
    PvpTokenModelCallback tokenModelCallback;

    /**
     * The User model.
     */
    @Bean
    PvpUserModel userModel;

    @Bean
    PvpHelper pvpHelper;

    /**
     * Get token for user
     * @param loginUser the user to log in.
     * @param callback  the callback to trigger when finished.
     */
    @Background
    public void getToken(PvpLogin loginUser, PvpTokenModelCallback callback) {
        if(pvpHelper.isNetworkConnected()){
            refreshTokenOnline(loginUser, callback);
        } else {
            callback.tokenRequestSuccess();
        }
    }

    private void refreshTokenOnline(PvpLogin loginUser, PvpTokenModelCallback callback) {
        this.tokenModelCallback = callback;
        try {
            sharedPreferences.token().put("");
            JsonNode result = restService.post("token", loginUser);
            String token = result.get("data").get("token").toString();
            sharedPreferences.token().put(token);

            if(token != null) {
                userModel.getCurrentUser(this);
                Log.i("Token", token);
            } else {
                if(callback != null) {
                    callback.tokenRequestFail("");
                }
            }
        } catch (RestClientException e) {
            if(callback != null) {
                String errorMessage = e.getMessage();
                if (e instanceof PvpHttpClientErrorException) {
                    String header =  ((PvpHttpClientErrorException)e).getFirstHeader("X-Message");
                    if (header != null && !header.isEmpty()) {
                        errorMessage = header;
                    }
                }
                callback.tokenRequestFail(errorMessage);
            }
        }
    }

    @Background
    public void loginUserByUDID(PvpTokenModelCallback callback) {
        String udid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        PvpUser user = new PvpUser();
        user.id = udid;
        user.email = "";
        user.password = udid;
        try {
            JsonNode userResult = restService.post("user", user);
            PvpLogin login = new PvpLogin(udid, udid);
            getToken(login, callback);
        } catch (Exception e) {
            if(callback != null) {
                callback.tokenRequestFail("");
            }
        }
    }

    @Background
    public void convertUserByUDIDToPVPUser(PvpUser user, PvpTokenModelCallback callback) {
        try {
            ObjectMapper mapper = new ObjectMapper();
//            String infoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(privateInfo);
            JsonNode node = mapper.valueToTree(user);
            ((ObjectNode)node).put("convertFromUserId", pvpHelper.getCurrentUser()._id.getMongoId());
            JsonNode userResult = restService.post("user", node);
            PvpLogin login = new PvpLogin(user.id, user.password);
            getToken(login, callback);
        } catch (Exception e) {
            if(callback != null) {
                callback.tokenRequestFail("");
            }
        }
    }

    @Override
    public void userRequestSuccess(List<PvpUser> users) {}

    @Override
    public void userRequestSuccess(PvpUser user) {
        String udid = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        sharedPreferences.isLoggedIn().put(!udid.equals(user.id));
        if (this.tokenModelCallback != null) {
            this.tokenModelCallback.tokenRequestSuccess();
            context.getApplicationContext().sendBroadcast(new Intent(PvpConstants.BROADCAST_ACTION_USER_LOGIN_CHANGED));
        }
    }

    @Override
    public void userRequestFail() {
        if (this.tokenModelCallback != null) {
            this.tokenModelCallback.tokenRequestFail("");
            context.getApplicationContext().sendBroadcast(new Intent(PvpConstants.BROADCAST_ACTION_USER_LOGIN_CHANGED));
        }
    }

    /**
     * Logout current user
     */
    @Background
    public void logout() {
        logout(null);
    }

    @Background
    public void logout(PvpTokenModelCallback callback) {
        cleanUser();
        context.getApplicationContext().sendBroadcast(new Intent(PvpConstants.BROADCAST_ACTION_USER_LOGIN_CHANGED));

        if (pvpHelper.isLoginWithUdidEnabled()) {
            loginUserByUDID(callback);
        }
    }

    public void cleanUser() {
        sharedPreferences.token().put("");
        sharedPreferences.loggedInUserId().put("");
        sharedPreferences.loggedInUserPass().put("");
        sharedPreferences.isLoggedIn().put(false);
        sharedPreferences.currentUser().put("");
        sharedPreferences.userPurchases().put("");
        pvpHelper.setCurrentUser(null);
    }

    /**
     * Checks if we are logged in.
     *
     * @return true or false, depending on whether we are logged in
     */
    public boolean isLoggedIn() {
        return pvpHelper.getCurrentUser() != null && sharedPreferences.isLoggedIn().get();
    }
}
