package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpEntry;
import com.pandaos.pvpclient.objects.PvpMeta;

import java.util.List;

/**
 * The {@link PvpEntryModelCallback} provides an interface for responding to events from the {@link PvpEntryModel}.
 */
public interface PvpEntryModelCallback {

    /**
     * Entry request success
     * @param entry the entry.
     */
    void entryRequestSuccess(PvpEntry entry);

    /**
     * Entry request success
     * @param entries the entries.
     */
    void entryRequestSuccess(List<PvpEntry> entries);

    /**
     * Entry request success with metadata.
     * @param entries the entries.
     * @param meta the metadata.
     */
    void entryRequestSuccessWithMeta(List<PvpEntry> entries, PvpMeta meta);

    /**
     * Entry request fail
     */
    void entryRequestFail();
}
