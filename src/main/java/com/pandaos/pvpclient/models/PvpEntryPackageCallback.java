package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpPackage;

/**
 * Created by orenkosto on 7/10/17.
 */

public interface PvpEntryPackageCallback {

    /**
     * Entry request success
     */
    void entryPackageRequestSuccess(PvpPackage entryPackage);

    /**
     * Entry request fail
     */
    void entryPackageRequestFail();
}
