package com.pandaos.pvpclient.models;

import androidx.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static com.pandaos.pvpclient.models.PvpFontWeight.BOLD;
import static com.pandaos.pvpclient.models.PvpFontWeight.MEDIUM;
import static com.pandaos.pvpclient.models.PvpFontWeight.REGULAR;
import static com.pandaos.pvpclient.models.PvpFontWeight.SEMI_BOLD;

@Retention(RetentionPolicy.SOURCE)
@StringDef({REGULAR, MEDIUM, SEMI_BOLD, BOLD})
public @interface PvpFontWeight {
    String REGULAR = "400";
    String MEDIUM = "500";
    String SEMI_BOLD = "600";
    String BOLD = "700";
}