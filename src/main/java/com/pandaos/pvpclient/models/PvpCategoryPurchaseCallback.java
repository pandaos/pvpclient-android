package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpCategory;

import java.util.Map;

/**
 * Created by Aviya Vaitzman on 21/08/18.
 */

public interface PvpCategoryPurchaseCallback {

    /**
     * Category purchase success
     */
    void categoryPurchaseSuccess();

    /**
     * Category purchase fail
     */
    void categoryPurchaseFail();

    /**
     * Category purchase success
     */
    void categorySubscriptionSuccess(PvpCategory pvpCategory, Object pvpObject);

    /**
     * Category purchase fail
     */
    void categorySubscriptionFail(PvpCategory pvpCategory);
}
