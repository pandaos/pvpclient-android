package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpCategory;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * The {@link PvpCategoryModel} provides an interface for using the PVP category service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpCategoryModel extends PvpBaseModel {

    private ArrayList<PvpCategory> categoryTree = null;
    private PvpCategory category = null;

    /**
     * Retrieves a category tree with a given root category.
     *
     * @param category the root category.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getCategoryTree(String category, PvpCategoryModelCallback callback, Object entryFilter) {
        try {
            ArrayList<PvpCategory> categoryList = this.categoryTree;
            if (categoryList == null) {
                JsonNode result;
                if(useCdnApi) {
                    result = restService.getCategoryTreeExpandWithSortFromCdn("categorytree/" + category, filter(), expand(), entryFilter(entryFilter), sharedPreferences.pvpInstanceId().get());
                } else {
                    result = restService.getCategoryTreeExpandWithSort("categorytree/" + category, filter(), expand(), entryFilter(entryFilter), sharedPreferences.pvpInstanceId().get());
                }
                categoryList = PvpCategory.parseListFromJsonNode(result.get("data"));
            }

            if (callback != null) {
                callback.categoryRequestSuccess(categoryList);
            }

        } catch (RestClientException e) {
            if (callback != null) {
                callback.categoryRequestFail();
            }

        }
    }

    /**
     * Retrieves a category tree with a given root category.
     *
     * @param category the root category.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getCategoryTree(String category, PvpCategoryModelCallback callback, Object filter, Object entryFilter) {
        try {
            ArrayList<PvpCategory> categoryList = this.categoryTree;
            if (categoryList == null) {
                JsonNode result;
                if(useCdnApi) {
                    result = restService.getCategoryTreeExpandWithSortFromCdn("categorytree/" + category, filter(filter), expand(), entryFilter(entryFilter), sharedPreferences.pvpInstanceId().get());
                } else {
                    result = restService.getCategoryTreeExpandWithSort("categorytree/" + category, filter(filter), expand(), entryFilter(entryFilter), sharedPreferences.pvpInstanceId().get());
                }
                categoryList = PvpCategory.parseListFromJsonNode(result.get("data"));
            }

            if (callback != null) {
                callback.categoryRequestSuccess(categoryList);
            }

        } catch (RestClientException e) {
            if (callback != null) {
                callback.categoryRequestFail();
            }

        }
    }

    /**
     * Retrieves a category tree with a given root category.
     *
     * @param category the root category.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getCategory(String category, PvpCategoryModelCallback callback, Object entryFilter) {
        try {
            PvpCategory pvpCategory = this.category;
            if (pvpCategory == null) {
                JsonNode result;
                if(useCdnApi) {
                    result = restService.getCategoryTreeExpandWithSortFromCdn("category/" + category, filter(), expand(), entryFilter(entryFilter), sharedPreferences.pvpInstanceId().get());
                } else {
                    result = restService.getCategoryTreeExpandWithSort("category/" + category, filter(), expand(), entryFilter(entryFilter), sharedPreferences.pvpInstanceId().get());
                }
                pvpCategory = PvpCategory.parseOneFromJsonNode(result.get("data"));
            }

            if (callback != null) {
                callback.categoryRequestSuccess(pvpCategory);
            }

        } catch (RestClientException e) {
            if (callback != null) {
                callback.categoryRequestFail();
            }

        }
    }

    @Background
    public void verifyPurchaseForCategoryId(int categoryId, PvpCategoryPurchaseCallback callback) {
        try {
            //4 = category type
            JsonNode result = restService.getUserPurchase("user/0/purchase/" + categoryId, "4", sharedPreferences.pvpInstanceId().get());
            if (callback != null) {
                callback.categoryPurchaseSuccess();
            }
        } catch (Exception e) {
            if (callback != null) {
                callback.categoryPurchaseFail();
            }

        }
    }

    @Background
    public void verifySubscriptionForCategoryId(PvpCategory pvpCategory, Object pvpObject, PvpCategoryPurchaseCallback callback) {
        try {
            //4 = category type
            JsonNode result = restService.getUserPurchase("user/0/purchase/" + pvpCategory.id, "4", sharedPreferences.pvpInstanceId().get());
            if (callback != null) {
                callback.categorySubscriptionSuccess(pvpCategory, pvpObject);
            }
        } catch (Exception e) {
            if (callback != null) {
                callback.categorySubscriptionFail(pvpCategory);
            }

        }
    }

    /**
     * Get Expand params.
     * TODO: this should not be hard coded
     * @return String the params to expand.
     */
    public String expand() {
        return "entries,disableMergeChildrenContent,cleanup";
    }

    /**
     * Genetares an empty filter string.
     * TODO: This should move to the base object and let the user handle what filter he wants to send
     * @return filter a new empty filter string.
     */
    public String filter() {
        JSONObject filter = new JSONObject();
        return filter.toString();
    }

    public String filter(Object filter) {
        JSONObject filterJson = new JSONObject();
        try {
            if(filter != null && ((LinkedHashMap) filter).get("orderBy") != null) {
                filterJson.putOpt("orderBy", ((LinkedHashMap) filter).get("orderBy").toString());
            }

            if(filter != null && ((LinkedHashMap) filter).get("statusIn") != null) {
                filterJson.putOpt("statusIn", ((LinkedHashMap) filter).get("statusIn").toString());
            }

            if(filter != null && ((LinkedHashMap) filter).get("pageSize") != null) {
                filterJson.putOpt("pageSize", ((LinkedHashMap) filter).get("pageSize"));
            }

            if(filter != null && ((LinkedHashMap) filter).get("pageIndex") != null) {
                filterJson.putOpt("pageIndex", ((LinkedHashMap) filter).get("pageIndex"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return filterJson.toString();
    }

    public String entryFilter(Object entryFilter) {
        JSONObject entryFilterJson = new JSONObject();
        try {
            if(entryFilter != null && ((LinkedHashMap) entryFilter).get("orderBy") != null) {
                entryFilterJson.putOpt("orderBy", ((LinkedHashMap) entryFilter).get("orderBy").toString());
            }

            if(entryFilter != null && ((LinkedHashMap) entryFilter).get("pageSize") != null) {
                entryFilterJson.putOpt("pageSize", ((LinkedHashMap) entryFilter).get("pageSize"));
            }

            if(entryFilter != null && ((LinkedHashMap) entryFilter).get("pageIndex") != null) {
                entryFilterJson.putOpt("pageIndex", ((LinkedHashMap) entryFilter).get("pageIndex"));
            }

            if(entryFilter != null && ((LinkedHashMap) entryFilter).get("mediaTypeIn") != null) {
                entryFilterJson.putOpt("mediaTypeIn", ((LinkedHashMap) entryFilter).get("mediaTypeIn"));
            }

            if(entryFilter != null && ((LinkedHashMap) entryFilter).get("statusIn") != null) {
                entryFilterJson.putOpt("statusIn", ((LinkedHashMap) entryFilter).get("statusIn"));
            }

            if (entryFilter != null && ((LinkedHashMap) entryFilter).get("endDateGreaterThanOrEqualOrNull") != null) {
                long currentTime = (System.currentTimeMillis() / 1000L) - pvpHelper.getServerTimeOffset();
                entryFilterJson.putOpt("endDateGreaterThanOrEqualOrNull", currentTime);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return entryFilterJson.toString();
    }

    /**
     * Get Sort
     * @return
     */
    public String sort() {
        return "{\"name\": 1}";
    }
}
