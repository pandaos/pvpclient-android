package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpUserInstances;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@link PvpUserInstanceModel} provides an interface for using the PVP user service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpUserInstanceModel extends PvpBaseModel {

    @Background
    public void getUserInstanceModel(PvpPvpUserInstanceModelCallback callback) {
        try {
            String userId = sharedPreferences.loggedInUserId().get();
            String iid = sharedPreferences.pvpInstanceId().get();
            JsonNode result = restService.getUserInstances("user/0/instances", filter(), sort(), iid, userId);
            List<PvpUserInstances> pvpUserInstances = PvpUserInstances.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.userInstanceSuccess(new ArrayList<PvpUserInstances>(pvpUserInstances));
            }
        } catch (Exception e) {
            PvpHelper.pvpLog("Pvp_User_Instance_Model", e.getMessage(), PvpHelper.logType.error, true);
            if (callback != null) {
                callback.userInstanceFail();
            }
        }
    }

    /**
     * Get the default filter.
     *
     * @return the default filter string.
     */
    String filter() {
        JSONObject filter = new JSONObject();
        JSONObject existsObj = new JSONObject();
        try {
            existsObj.put("$nin", 1);
            filter.put("type", existsObj);
            return filter.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Get the default filter.
     *
     * @return the default filter string.
     */
    String sort() {
        JSONObject sort = new JSONObject();
        try {
            sort.put("name", 1);
            return sort.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    public interface PvpPvpUserInstanceModelCallback {

        /**
         * get user instances success
         */
        void userInstanceSuccess(ArrayList<PvpUserInstances> pvpUserInstances);


        /**
         * get user instances fail
         */
        void userInstanceFail();
    }

}
