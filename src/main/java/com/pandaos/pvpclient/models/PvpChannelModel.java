package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpChannel;
import com.pandaos.pvpclient.objects.PvpMeta;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * The {@link PvpChannelModel} provides an interface for using the PVP channel service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpChannelModel extends PvpBaseModel {

    @Bean
    PvpHelper pvpHelper;

    /**
     * Get channels
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getChannels(PvpChannelModelCallback callback) {
        getChannels(callback, false, null, null);
    }

    /**
     * Get channels - with EPG
     * @param callback - the callback to trigger when finished.
     * @param withEpg
     */
    @Background
    public void getChannels(PvpChannelModelCallback callback, boolean withEpg) {
        getChannels(callback, withEpg, null, null);
    }

    /**
     * Get channels - with EPG and using sort
     * @param callback
     * @param withEpg
     * @param sort
     */
    @Background
    public void getChannels(PvpChannelModelCallback callback, boolean withEpg, String sort, String pager) {
        try {
            JsonNode result;

            if(sort == null) {
                sort = sort();
            } else if(sort.contains("\"-1\"")) {
                sort = sort.replaceAll("\"-1\"", "-1");
            }

            if(pager == null) {
                pager = pager();
            }

            long now = System.currentTimeMillis() / 1000;
            long startDateLong = now - (pvpHelper.getCatchUpEnabled() ? 3600 * 13 : 3600 * 2);
            long endDateLong = now + 3600 * 4;
            String startDate = String.valueOf(startDateLong);
            String endDate = String.valueOf(endDateLong);

            if (withEpg) {
                if(useCdnApi) {
                    result = restService.getChannelExpandWithSortFromCdn("channels/epg", filter(), pager,  sort, sharedPreferences.pvpInstanceId().get(), startDate, endDate);
                } else {
                    result = restService.getChannelExpandWithSort("channels/epg", filter(), pager, sort, sharedPreferences.pvpInstanceId().get(), startDate, endDate);
                }
            } else {
                if(useCdnApi) {
                    result = restService.getExpandWithSortFromCdn("channels/channel", "", expand(), sort, sharedPreferences.pvpInstanceId().get());
                } else {
                    result = restService.getExpandWithSort("channels/channel", "", expand(), sort, sharedPreferences.pvpInstanceId().get());
                }
            }

            ArrayList<PvpChannel> pvpChannels = PvpChannel.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.channelRequestSuccess(pvpChannels);
            }

        } catch (Exception e) {
            if (callback != null) {
                callback.channelRequestFail();
            }
        }
    }

    /**
     * Get channels - with EPG and using sort
     * @param callback
     * @param sort
     */
    @Background
    public void getChannelsEpg(PvpChannelModelCallback callback, String sort, String pager, long start, long end) {
        try {
            JsonNode result;

            if(sort == null) {
                sort = sort();
            } else if(sort.contains("\"-1\"")) {
                sort = sort.replaceAll("\"-1\"", "-1");
            }

            if (pager == null) {
               pager = pager();
            }

            String startDate = String.valueOf(start);
            String endDate = String.valueOf(end);

            if(useCdnApi) {
                result = restService.getChannelExpandWithSortFromCdn("channels/epg", filter(), pager, sort,  sharedPreferences.pvpInstanceId().get(), startDate, endDate);
            } else {
                result = restService.getChannelExpandWithSort("channels/epg", filter(), pager, sort, sharedPreferences.pvpInstanceId().get(), startDate, endDate);
            }

            ArrayList<PvpChannel> pvpChannels = PvpChannel.parseListFromJsonNode(result.get("data"));
            PvpMeta meta = PvpMeta.parseOneFromJsonNode(result.get("meta"));

            if (callback != null) {
                callback.channelEpgRequestSuccess(pvpChannels, meta.total);
            }

        } catch (Exception e) {
            if (callback != null) {
                callback.channelRequestFail();
            }
        }
    }

    @Background
    public void getChannel(String channelId, PvpChannelModelCallback callback) {
        getChannel(channelId, callback, false);
    }
    public void getChannel(String channelId, PvpChannelModelCallback callback, boolean withEpg) {
        getChannel(channelId, callback, withEpg, false);
    }

    @Background
    public void getChannel(String channelId, PvpChannelModelCallback callback, boolean withEpg, boolean byEntryId) {
        try {
            JsonNode result;
            PvpChannel pvpChannel;
            if (withEpg) {

                String filter = byEntryId ? byEntryFilter(channelId) : filter();
                String path = byEntryId ? "channels/epg" : "channels/epg/" + channelId;

                if(useCdnApi) {
                    result = restService.getChannelExpandFromCdn(path, filter, sharedPreferences.pvpInstanceId().get());
                } else {
                    result = restService.getChannelExpand(path, filter, sharedPreferences.pvpInstanceId().get());
                }

                pvpChannel = PvpChannel.parseOneFromJsonNode(result.get("data").get(0));

            } else {

                if(useCdnApi) {
                    result = restService.getExpandFromCdn("channels/channel/" + channelId, "", expand(), sharedPreferences.pvpInstanceId().get());
                } else {
                    result = restService.getExpand("channels/channel/" + channelId, "", expand(), sharedPreferences.pvpInstanceId().get());
                }

                pvpChannel = PvpChannel.parseOneFromJsonNode(result.get("data"));

            }

            if (callback != null) {
                callback.channelRequestSuccess(pvpChannel);
            }

        } catch (RestClientException e) {
            if (callback != null) {
                callback.channelRequestFail();
            }
        }
    }

    /**
     * Gets schedule for channel.
     *
     * @param channel  the channel
     * @param callback the callback
     */
    @Background
    public void getScheduleForChannel(PvpChannel channel, PvpChannelModelCallback callback) {
        try {

            PvpChannel resultChannel;
            JsonNode result;

            if(useCdnApi) {
                result = restService.getChannelExpandFromCdn("channels/epg/" + channel.getId(), filter(), sharedPreferences.pvpInstanceId().get());
            } else {
                result = restService.getChannelExpand("channels/epg/" + channel.getId(), filter(), sharedPreferences.pvpInstanceId().get());
            }

            resultChannel = PvpChannel.parseOneFromJsonNode(result.get("data").get(0));

            if (callback != null) {
                callback.channelScheduleRequestSuccess(resultChannel);
            }

        } catch (Exception e) {
            if (callback != null) {
                callback.channelRequestFail();
            }
        }
    }

    /**
     * Get the default filter.
     *
     * @return the default filter string.
     */
    String byEntryFilter(String entryId) {
        JSONObject filter = new JSONObject();
        JSONObject existsObj = new JSONObject();
        try {
            filter.put("entry", entryId);
            return filter.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Get the default filter.
     *
     * @return the default filter string.
     */
    String filter() {
        JSONObject filter = new JSONObject();
        JSONObject existsObj = new JSONObject();
        try {
            existsObj.put("$exists", 1);
//            filter.put("orderBy", "-recent");
            filter.put("entry", existsObj);
            return filter.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Get Exapnd
     * TODO: this should not be hard coded
     * @return String
     */
    public String expand() {
        return "[{\"entry\":1}]";
    }

    /**
     * Get Sort
     * @return
     */
    public String sort() {
        return "{\"name\": 1}";
    }

    /**
     * Get Sort
     * @return
     */
    public String pager() {
        return "{\"pageSize\":200}";
    }

    /**
     * Get Sort
     * @return
     */
    public String sort(String sortBy, String sortOrder) {
        return "{\"" + sortBy + "\": " + sortOrder + "}";
    }

    public Map<String, String> additionalParams() {
        Map<String, String> paramsMap = new HashMap();
            paramsMap.put("jsonFormat", "json");
            paramsMap.put("getPastEntries", "true");
            paramsMap.put("includeEmptyChannels", "true");
            paramsMap.put("includeEmptySchedules", "true");
            paramsMap.put("resolveRemotePaths", "false");
            paramsMap.put("schedulesAsOneStream", "true");
            paramsMap.put("startDate", "0");
//            result = result.replaceAll("\"", "");
//            result = result.replaceAll("\\{", "");
//            result = result.replaceAll("\\}", "");
//            result = result.replaceAll(":", "=");
//            result = result.replaceAll(",", "&");

        return paramsMap;
    }
}