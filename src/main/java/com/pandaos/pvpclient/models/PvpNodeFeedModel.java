package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpNode;
import com.pandaos.pvpclient.objects.PvpNodeFeed;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.util.List;

/**
 * The {@link PvpNodeFeedModel} provides an interface for using the node service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpNodeFeedModel extends PvpBaseModel {

    /**
     *
     * @param nodeId
     * @param callback
     */
    @Background
    public void getNodeFeed(String nodeId, PvpNodeFeedModelCallback callback) {
        try {
            String path = nodeId != null ?  "node-category/" + nodeId : "node-category";
            PvpNodeFeed nodeFeed;
            JsonNode result;
            if(useCdnApi) {
                result = restService.getFromCdn(path, sharedPreferences.pvpInstanceId().get(), "");
            } else {
                result = restService.get(path, sharedPreferences.pvpInstanceId().get());
            }

            nodeFeed = PvpNodeFeed.parseOneFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.nodeFeedRequestSuccess(nodeFeed);
            }
        } catch (RestClientException e) {
            if(callback != null) {
                callback.nodeFeedRequestFail();
            }
        }
    }

    /**
     * Generate default filter string.
     *
     * @return the default filter string
     */
    String filter(String filterString) {
        JSONObject filter = new JSONObject();
        long currentTime = (System.currentTimeMillis() / 1000L) - pvpHelper.getServerTimeOffset();
        try {
            if(filterString == null) {
                filter.put("orderBy", "-createdAt");
            } else {
                //TODO: set values from filterString
            }
            return filter.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
