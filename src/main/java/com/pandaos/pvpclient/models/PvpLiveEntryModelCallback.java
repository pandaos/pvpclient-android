package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpLiveEntry;
import com.pandaos.pvpclient.objects.PvpResult;

import java.util.List;

/**
 * The {@link PvpLiveEntryModelCallback} provides an interface for responding to events from the {@link PvpLiveEntryModel}.
 */
public interface PvpLiveEntryModelCallback {

    /**
     * Live entry request success.
     *
     * @param entries the entries array.
     */
    void liveEntryRequestSuccess(List<PvpLiveEntry> entries);

    /**
     * Live entry request success.
     *
     * @param entry the entry.
     */
    void liveEntryRequestSuccess(PvpLiveEntry entry);

    /**
     * Live entry request success.
     *
     * @param entry the entry.
     */
    void liveEntryCreateRequestSuccess(PvpLiveEntry entry);

    /**
     * Live entry request fail.
     */
    void liveEntryRequestFail();

    void liveEntryIsLiveRequestSuccess(List<PvpResult> results);
}
