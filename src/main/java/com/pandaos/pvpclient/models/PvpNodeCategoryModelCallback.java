package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpNodeCategory;

import java.util.List;

/**
 * The {@link PvpNodeCategoryModelCallback} provides an interface for responding to events from the {@link PvpNodeCategoryModel}.
 */
public interface PvpNodeCategoryModelCallback {

    /**
     * Node request success.
     *
     * @param nodes the entries array.
     */
    void nodeCategoryRequestSuccess(List<PvpNodeCategory> nodes);

    /**
     * Node request success.
     *
     * @param nodes the entries array.
     */
    void nodeCategoryRequestSuccess(PvpNodeCategory nodes);

    /**
     * Node request fail.
     */
    void nodeCategoryRequestFail();
}
