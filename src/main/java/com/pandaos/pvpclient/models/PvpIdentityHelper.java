package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpUser;

import org.androidannotations.annotations.EBean;

/**
 * The {@link PvpIdentityHelper} provides helper functions for the session user identity.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpIdentityHelper {

    /**
     * The current user.
     */
    private static PvpUser currentUser;

    /**
     * Set current user
     * @param user the user to set.
     */
    public static void setCurrentUser(PvpUser user) {
        currentUser = user;
    }

    /**
     * Get current user.
     *
     * @return the current user.
     */
    public static PvpUser getCurrentUser() {
        return currentUser;
    }
}