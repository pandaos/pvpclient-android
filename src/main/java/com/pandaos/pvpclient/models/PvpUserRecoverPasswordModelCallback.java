package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpUser;

/**
 * The {@link PvpUserRecoverPasswordModelCallback} provides an interface for responding to events from the {@link PvpUserModel}.
 */
public interface PvpUserRecoverPasswordModelCallback {

    /**
     * Triggered upon a successful API request that returned a {@link PvpUser} object.
     */
    void passwordRecoveryRequestSuccess();

    /**
     * Triggered upon a failed API request.
     */
    void passwordRecoveryRequestFail();
}
