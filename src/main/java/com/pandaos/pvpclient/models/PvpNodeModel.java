package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpNode;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@link PvpNodeModel} provides an interface for using the node service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpNodeModel extends PvpBaseModel {

    /**
     *
     * @param page
     * @param filter
     * @param callback
     */
    @Background
    public void getNodes(int page, String filter, PvpNodeModelCallback callback) {
        try {
            List<PvpNode> nodeList;
            JsonNode result;
            if(useCdnApi) {
                result = restService.getFromCdn("node", filter, pager(page), sharedPreferences.pvpInstanceId().get());
            } else {
                result = restService.get("node", filter, pager(page), sharedPreferences.pvpInstanceId().get());
            }

            nodeList = PvpNode.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.nodeRequestSuccess(new ArrayList<PvpNode>(nodeList));
            }
        } catch (RestClientException e) {
            if(callback != null) {
                callback.nodeRequestFail();
            }
        }
    }

    /** get node by id
     *
     * @param nodeId - node id
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getNode(String nodeId, boolean byUrl, PvpNodeModelCallback callback) {
        try {
            PvpNode node;
            JsonNode result;
            if(useCdnApi) {
                result = restService.getNodeFromCdn("node/" + URLEncoder.encode(nodeId) , String.valueOf(byUrl), sharedPreferences.pvpInstanceId().get(), "");
            } else {
                result = restService.getNode("node/" + nodeId, String.valueOf(byUrl), sharedPreferences.pvpInstanceId().get());
            }

            node = PvpNode.parseOneFromJsonNode(result.get("data"));

            if(callback != null) {
                callback.nodeRequestSuccess(node);
            }
        } catch (RestClientException e) {
            if(callback != null) {
                callback.nodeRequestFail();
            }
        }
    }

    /**
     * Generate default filter string.
     *
     * @return the default filter string
     */
    String filter(String filterString) {
        JSONObject filter = new JSONObject();
        try {
            if(filterString == null) {
                filter.put("orderBy", "-createdAt");
            } else {
                //TODO: set values from filterString
            }
            return filter.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
