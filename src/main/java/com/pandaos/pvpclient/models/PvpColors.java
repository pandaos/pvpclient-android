package com.pandaos.pvpclient.models;

import android.content.Context;
import android.graphics.Color;

import androidx.core.content.ContextCompat;

import com.pandaos.pvpclient.R;
import com.pandaos.pvpclient.objects.PvpConfig;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * The {@link PvpColors} utility class provides many colors functions for managing different components of the library.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpColors {

    @Bean
    PvpHelper pvpHelper;

    /**
     * The Root Context.
     */
    @RootContext
    Context context;

    //Base colors
    public static final String PRIMARY_COLOR = "primaryColor";
    public static final String SECONDARY_COLOR = "secondaryColor";

    //Background colors
    public static final String BACKGROUND_COLOR = "backgroundColor";
    public static final String SECONDARY_BACKGROUND_COLOR = "secondaryBackgroundColor";

    //Highlights colors
    public static final String PRIMARY_HIGHLIGHT_COLOR = "primaryHighlightColor";
    public static final String SECONDARY_HIGHLIGHT_COLOR = "secondaryHighlightColor";
    public static final String THIRD_HIGHLIGHT_COLOR = "thirdHighlightColor";

    //text colors
    public static final String PRIMARY_TEXT_COLOR = "primaryTextColor";
    public static final String SECONDARY_TEXT_COLOR = "secondaryTextColor";
    public static final String EDIT_TEXT_COLOR = "editTextColor";

    //epg colors
    public static final String EPG_PRIMARY_COLOR = "epgPrimaryColor";
    public static final String EPG_HIGHLIGHTED_COLOR = "epgHighlightColor";
    public static final String EPG_BACKGROUND_COLOR = "epgBackgroundColor";
    public static final String EPG_TEXT_COLOR = "epgTextColor";

    //button colors
    public static final String BUTTON_PRIMARY_COLOR = "buttonPrimaryColor";
    public static final String BUTTON_SECONDARY_COLOR = "buttonSecondaryColor";
    public static final String BUTTON_TEXT_PRIMARY_COLOR = "buttonTextPrimaryColor";
    public static final String BUTTON_TEXT_SECONDARY_COLOR = "buttonTextSecondaryColor";

    //Menu colors
    public static final String MENU_BACKGROUND_COLOR = "menuBackgroundColor";
    public static final String MENU_PRIMARY_TEXT_COLOR = "menuPrimaryTextColor";
    public static final String MENU_PRIMARY_ICON_COLOR = "menuIconColor";
    public static final String MENU_SEPARATOR_COLOR = "menuSeparatorColor";

    //Toolbar colors
    public static final String TOOLBAR_BACKGROUND_COLOR = "toolbarBackgroundColor";
    public static final String TOOLBAR_TEXT_COLOR = "toolbarTextColor";

    //Play Page colors
    public static final String PLAY_PAGE_PRIMARY_TEXT_COLOR = "playPagePrimaryTextColor";
    public static final String PLAY_PAGE_SECONDARY_TEXT_COLOR = "playPageSecondaryTextColor";

    //Items List colors
    public static final String LIST_ITEM_TEXT_COLOR = "listItemTextColor";

    //User pages colors (Login \ registration \ forgot)
    public static final String LOGIN_PAGE_PRIMARY_TEXT_COLOR = "loginPagePrimaryTextColor";
    public static final String LOGIN_PAGE_SECONDARY_TEXT_COLOR = "loginPageSecondaryTextColor";

    //Radio colors
    public static final String RADIO_PRIMARY_COLOR = "radioPrimaryColor";
    public static final String RADIO_BACKGROUND_COLOR = "radioBackgroundColor";

    public static final String PACKAGE_TEXT_COLOR = "packageTextColor";


    public String getColorHexStringFromConfig(String colorName) {
        String colorStr = null;
        String defaultColorName = null;

        PvpConfig pvpConfig = pvpHelper.getConfig();
        if(pvpConfig != null && pvpConfig.mobile.containsKey(colorName)) {
            colorStr = pvpConfig.mobile.get(colorName).toString();
        }

        if(colorStr == null) {
            defaultColorName = getDefaultColorForConfigColor(colorName);
            if(defaultColorName != null && pvpConfig != null && pvpConfig.mobile.containsKey(defaultColorName)) {
                colorStr = pvpHelper.getConfig().mobile.get(defaultColorName).toString();
            }
        }

        if(colorStr == null) {
            if(defaultColorName == null) {
                defaultColorName = colorName;
            }
            int bambooDefaultColorName = getBambooDefaultColor(defaultColorName);
            colorStr = "#" + Integer.toHexString(ContextCompat.getColor(context, bambooDefaultColorName));
        }

        return colorStr;
    }

    public int getParsingColorFromConfig(String colorName) {
        return Color.parseColor(getColorHexStringFromConfig(colorName));
    }

    public int getParsingColorTransparencyFromConfig(String colorName, int transparencyPercentage) {
        String transparency = "#" + Integer.toHexString(transparencyPercentage);
        String color = getColorHexStringFromConfig(colorName);
        if(transparencyPercentage>0)
        color = color.replace("#", transparency);

        return Color.parseColor(color);
    }

    private String getDefaultColorForConfigColor(String confColorName) {
        switch (confColorName) {
            case PACKAGE_TEXT_COLOR:
                return PRIMARY_COLOR;
            case MENU_PRIMARY_ICON_COLOR:
                return MENU_PRIMARY_TEXT_COLOR;
            case MENU_BACKGROUND_COLOR:
                return BACKGROUND_COLOR;
            case SECONDARY_BACKGROUND_COLOR:
                return BACKGROUND_COLOR;
            case BUTTON_PRIMARY_COLOR:
                return PRIMARY_COLOR;
            case BUTTON_SECONDARY_COLOR:
                return SECONDARY_COLOR;
            case LIST_ITEM_TEXT_COLOR:
                return PRIMARY_HIGHLIGHT_COLOR;
            case TOOLBAR_BACKGROUND_COLOR:
                return PRIMARY_COLOR;
            case MENU_PRIMARY_TEXT_COLOR:
                return PRIMARY_TEXT_COLOR;
            case MENU_SEPARATOR_COLOR:
                return BACKGROUND_COLOR;
            case TOOLBAR_TEXT_COLOR:
                return PRIMARY_TEXT_COLOR;
            case PLAY_PAGE_PRIMARY_TEXT_COLOR:
                return PRIMARY_TEXT_COLOR;
            case PLAY_PAGE_SECONDARY_TEXT_COLOR:
                return SECONDARY_TEXT_COLOR;
            case EDIT_TEXT_COLOR:
                return PRIMARY_TEXT_COLOR;
            case BUTTON_TEXT_PRIMARY_COLOR:
                return PRIMARY_TEXT_COLOR;
            case BUTTON_TEXT_SECONDARY_COLOR:
                return PRIMARY_TEXT_COLOR;
            case LOGIN_PAGE_PRIMARY_TEXT_COLOR:
                return PRIMARY_TEXT_COLOR;
            case LOGIN_PAGE_SECONDARY_TEXT_COLOR:
                return SECONDARY_TEXT_COLOR;
            default:
                return null;
        }
    }


    private int getBambooDefaultColor(String confColorName) {
        switch (confColorName) {
            case BACKGROUND_COLOR:
            case SECONDARY_BACKGROUND_COLOR:
                return R.color.BambooBackgroundColor;
            case PRIMARY_COLOR:
            case PRIMARY_TEXT_COLOR:
                return R.color.BambooPrimaryColor;
            case SECONDARY_COLOR:
            case SECONDARY_TEXT_COLOR:
                return R.color.BambooSecondaryColor;
            case PRIMARY_HIGHLIGHT_COLOR:
                return R.color.BambooPrimaryHighlightColor;
            case SECONDARY_HIGHLIGHT_COLOR:
            case THIRD_HIGHLIGHT_COLOR:
                return R.color.BambooSecondaryHighlightColor;
            default:
                return R.color.BambooPrimaryColor;
        }
    }


}
