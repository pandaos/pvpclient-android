package com.pandaos.pvpclient.models;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.net.ConnectivityManager;
import android.util.Log;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpConfig;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.springframework.web.client.RestClientException;

/**
 * The {@link PvpConfigModel} provides an interface for using the PVP config service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpConfigModel extends PvpBaseModel {

    private boolean isOfflineMode;

    @Bean
    PvpHelper pvpHelper;

    /**
     * Init PVP config.
     */
    @Background
    public void initConfig() {
        initConfig(PvpHelper.PVP_SERVER_BASE_URL);
    }

    /**
     * Init PVP config.
     *
     * @param baseUrl the base url
     */
    @Background
    public void initConfig(String baseUrl) {
        initConfig(baseUrl, null);
    }

    /**
     * Init PVP config.
     *
     * @param baseUrl  the base url
     * @param callback the callback
     */
    @Background
    public void initConfig(String baseUrl, PvpConfigModelCallback callback) {
        sharedPreferences.pvpServerBaseUrl().put(baseUrl);
        restService.setRootUrl(baseUrl);
        getConfig(callback);
    }

    /**
     * Get the current config.
     *
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getConfig(PvpConfigModelCallback callback) {
        try {
            String url = restService.getRootUrl();

            PvpConfig config = null;

            String userId = sharedPreferences.loggedInUserId().get();
            JsonNode result;

            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            String versionCode = Integer.toString(pInfo.versionCode);

            if (pvpHelper.getConfig() != null) {
                isOfflineMode = pvpHelper.getOfflineModeEnabled();
            }

            if (pvpHelper.isNetworkConnected()) {
                if (useCdnApi) {
                    result = restService.getConfigFromCdn("config", sharedPreferences.pvpInstanceId().get(), userId, versionCode);
                } else {
                    result = restService.getConfig("config", sharedPreferences.pvpInstanceId().get(), userId, versionCode);
                }
                config = PvpConfig.parseFromJsonNode(result.get("config"));
                if (config != null) {
                    pvpHelper.setConfig(config);

                    if (config.userKs != null && config.userKs.length() > 0) {
                        sharedPreferences.ks().put(config.userKs);
                    }
                    if (callback != null) {
                        callback.configRequestSuccess();
                    }
                } else {
                    throw new RestClientException("No config retrieved from server");
                }
            } else {
                noInternetCase(callback);
            }
        } catch (Exception e) {
            if (callback != null) {
                callback.configRequestFail();
            }
        }
    }

    private void noInternetCase(PvpConfigModelCallback callback) {
        pvpHelper.setOfflineMenu();

        if (callback != null) {
            callback.configRequestSuccess();
        }
    }

    public boolean isOfflineMode() {
        return isOfflineMode;
    }

}
