package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpCuePoint;
import com.pandaos.pvpclient.objects.PvpMeta;

import java.util.List;

/**
 * The {@link PvpCuePointCallback} provides an interface for responding to cuepoint-related events from the {@link PvpEntryModel}.
 */
public interface PvpCuePointCallback {
    /**
     * Cuepoint request success.
     *
     * @param cuePoint the cue point
     */
    void cuepointRequestSuccess(PvpCuePoint cuePoint);

    /**
     * Cuepoint request success.
     *
     * @param cuePoints the cue points
     */
    void cuepointRequestSuccess(List<PvpCuePoint> cuePoints);

    /**
     * Cuepoint request success with metadata.
     *
     * @param cuePoints the cue points
     * @param meta      the metadata
     */
    void cuepointRequestSuccessWithMeta(List<PvpCuePoint> cuePoints, PvpMeta meta);

    /**
     * Cuepoint request fail.
     */
    void cuepointRequestFail();
}