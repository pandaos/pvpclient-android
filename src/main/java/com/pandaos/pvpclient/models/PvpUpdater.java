package com.pandaos.pvpclient.models;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * The {@link PvpUpdater} utility class provides many colors functions for managing different components of the library.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpUpdater {

    @Bean
    PvpHelper pvpHelper;

    /**
     * The Root Context.
     */
    @RootContext
    Context context;

    public void updatingAppIfNeeded() {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

            final String latestVersion = pvpHelper.getLatestVersion();
            if(!latestVersion.equals("") && !latestVersion.equals(pInfo.versionName)) {
                final String apkDownloadUrl = pvpHelper.getApkDownloadUrl();
                final String apkBaseUrl = pvpHelper.getApkBaseUrl();
                if(!apkDownloadUrl.equals("")) {
                    Thread thread = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            try  {
                                updateAppFromExternalSource(apkDownloadUrl, apkBaseUrl, latestVersion);
                                //Your code goes here
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    thread.start();
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    private void updateAppFromExternalSource(String downloadUrl, String baseUrl, String latestVersionName) {
        try{
            Log.v("DownloadAPK", "Trying to download APK");

            //Converting download url into URL
            URL url  = new URL(downloadUrl);
            //Opening Url Connection for data downloading
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            //Setting request method to server
            c.setRequestMethod("GET");
            //Connect the open connection
            c.connect();

            //Create a directory in SD Card if not present
            File apkStorage = new File(Environment.getExternalStorageDirectory() + "/downloadapk");
            if (!apkStorage.exists()) {
                apkStorage.mkdir();
                Log.v("DownloadAPK", "Directory Created.");
            }

            //Create an Output file i.e. download file inside above created directory
            String downloadFileName = downloadUrl.replace(baseUrl, "");//Create file name by picking download file name from URL
            File outputFile = new File(apkStorage, downloadFileName);
            if (!outputFile.exists()) {
                outputFile.createNewFile();
                Log.v("DownloadAPK", "File Created");
            }

            //Download File and write it over the outputFile created
            FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location
            InputStream is = c.getInputStream();//Get InputStream for connection
            byte[] buffer = new byte[1024];//Set buffer type
            int len1 = 0;//init length
            while ((len1 = is.read(buffer)) != -1) {
                fos.write(buffer, 0, len1);//Write new file
            }

            //Close all connection after downloading
            fos.close();
            is.close();

            Log.v("DownloadAPK", "downloading finished");
            installApk(Environment.getExternalStorageDirectory() + "/downloadapk" + "/tingo.apk");

        }catch(Exception e){
            Log.v("DownloadAPK", "exception in downloadData");
            e.printStackTrace();
        }
    }

    private void installApk(String path) {
        Log.v("DownloadAPK", "Trying to install new APK");
        Intent i = new Intent();
        i.setAction(Intent.ACTION_VIEW);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.setDataAndType(Uri.fromFile(new File(path)), "application/vnd.android.package-archive" );
        context.startActivity(i);
    }

}
