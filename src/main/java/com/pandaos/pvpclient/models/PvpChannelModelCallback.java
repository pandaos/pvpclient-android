package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpChannel;

import java.util.ArrayList;

/**
 * The {@link PvpChannelModelCallback} provides an interface for responding to events from the {@link PvpChannelModel}.
 */
public interface PvpChannelModelCallback {

    /**
     * On Channels request success
     *
     * @param channel the channel
     */
    void channelRequestSuccess(PvpChannel channel);

    /**
     * On Channels request success (List)
     *
     * @param channels List<PvpChannel>
     */
    void channelRequestSuccess(ArrayList<PvpChannel> channels);

    /**
     * On Channels request success (List)
     *
     * @param channels List<PvpChannel>
     */
    void channelEpgRequestSuccess(ArrayList<PvpChannel> channels, int total);

    /**
     * Channel schedule request success.
     *
     * @param channel the schedule
     */
    void channelScheduleRequestSuccess(PvpChannel channel);

    /**
     * On Channels request fail
     */
    void channelRequestFail();
}