package com.pandaos.pvpclient.models;

/**
 * The {@link PvpConfigModelCallback} provides an interface for responding to events from the {@link PvpConfigModel}.
 */
public interface PvpConfigModelCallback {

    /**
     * Config request success.
     */
    void configRequestSuccess();

    /**
     * Config request fail.
     */
    void configRequestFail();
}
