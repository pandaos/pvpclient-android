package com.pandaos.pvpclient.models;

/**
 * The {@link PvpTokenModelCallback} provides an interface for responding to events from the {@link PvpTokenModel}.
 */
public interface PvpTokenModelCallback {

    /**
     * On token success
     */
    void tokenRequestSuccess();

    /**
     * On token failed
     */
    void tokenRequestFail(String message);
}
