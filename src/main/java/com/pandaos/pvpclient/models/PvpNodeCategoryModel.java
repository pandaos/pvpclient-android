package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpNodeCategory;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.util.List;

/**
 * The {@link PvpNodeCategoryModel} provides an interface for using the node service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpNodeCategoryModel extends PvpBaseModel {

    /**
     *
     * @param nodeId
     * @param callback
     */
    @Background
    public void getNodeCategory(String nodeId, PvpNodeCategoryModelCallback callback) {
        try {
            JsonNode result;
            if(useCdnApi) {
                result = restService.getFromCdn("node-category/"+nodeId, sharedPreferences.pvpInstanceId().get(), "");
            } else {
                result = restService.get("node-category/"+nodeId, sharedPreferences.pvpInstanceId().get());
            }

            List<PvpNodeCategory> nodeCategories = PvpNodeCategory.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.nodeCategoryRequestSuccess(nodeCategories);
            }
        } catch (RestClientException e) {
            if(callback != null) {
                callback.nodeCategoryRequestFail();
            }
        }
    }

    /**
     *
     * @param page
     * @param filter
     * @param callback
     */
    @Background
    public void getNodeCategories(int page, String filter, PvpNodeCategoryModelCallback callback) {
        try {
            JsonNode result;
            if(useCdnApi) {
                result = restService.getFromCdn("node-category", filter(filter), pager(page), sharedPreferences.pvpInstanceId().get());
            } else {
                result = restService.get("node-category", filter(filter), pager(page), sharedPreferences.pvpInstanceId().get());
            }
            List<PvpNodeCategory> nodeCategories = PvpNodeCategory.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.nodeCategoryRequestSuccess(nodeCategories);
            }
        } catch (RestClientException e) {
            if(callback != null) {
                callback.nodeCategoryRequestFail();
            }
        }
    }

    /**
     * Generate default filter string.
     *
     * @return the default filter string
     */
    String filter(String filterString) {
        JSONObject filter = new JSONObject();
        long currentTime = (System.currentTimeMillis() / 1000L) - pvpHelper.getServerTimeOffset();
        try {
            if(filterString == null) {
                filter.put("orderBy", "-createdAt");
            } else {
                //TODO: set values from filterString
            }
            return filter.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
