package com.pandaos.pvpclient.models;

import android.app.UiModeManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Patterns;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpConfig;
import com.pandaos.pvpclient.objects.PvpEntry;
import com.pandaos.pvpclient.objects.PvpPackage;
import com.pandaos.pvpclient.objects.PvpPurchase;
import com.pandaos.pvpclient.objects.PvpUser;
import com.pandaos.pvpclient.services.PvpRestService;
import com.pandaos.pvpclient.utils.PvpMapHelper;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.annotations.RestService;
import org.springframework.web.client.RestClientException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.content.Context.UI_MODE_SERVICE;

/**
 * The {@link PvpHelper} utility class provides many helper functions for managing different components of the library.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpHelper {


    private boolean offlineMenu;

    /**
     * The enum Log type.
     */
    public enum logType {

        /**
         * Info log type.
         */
        info,

        /**
         * Debug log type.
         */
        debug,

        /**
         * Error log type.
         */
        error
    }

    /**
     * The default value the the PVP base URL.
     */
    public static final String PVP_SERVER_BASE_URL = "https:///demo.bamboo-video.com";

    /**
     * The default value the the PVP base URL.
     */
    public static final String PVP_CDN_BASE_URL = "https:///cdnapi.bamboo-video.com";

    /**
     * The default PVP API prefix.
     */
    public static final String PVP_SERVER_API_PREFIX = "/api";
    /**
     * The default PVP API suffix.
     */
    public static final String PVP_SERVER_API_SUFFIX = "?format=json";
    /**
     * The package name for the PVP Client library.
     */
    public static final String PVP_CLIENT_PACKAGE_NAME = "com.pandaos.pvpclient";
    /**
     * The default value for server debug mode (false).
     */
    public static final boolean PVP_SERVER_DEBUG_ENABLED = false;
    /**
     * The default value for client (logging) debug mode (false).
     */
    public static final boolean PVP_CLIENT_DEBUG_ENABLED = false;

    /**
     * The default page size.
     */
    public static final int PAGE_SIZE = 30;

    public static final int MOBILE_THEME_TYPE_BASIC = 1;
    public static final int MOBILE_THEME_TYPE_BG_IMAGE = 2;
    public static final int TV_THEME_TYPE_GALLERY_PLAY = 1;
    public static final int TV_THEME_TYPE_GALLERY_ENTRY = 2;
    /**
     * The constant SYSTEM_UI_AUTO_HIDE.
     *
     * @deprecated not used anymore.
     */
    public static final boolean SYSTEM_UI_AUTO_HIDE = true;
    /**
     * The constant SYSTEM_UI_AUTO_HIDE_DELAY_MILLIS.
     *
     * @deprecated not used anymore.
     */
    public static final int SYSTEM_UI_AUTO_HIDE_DELAY_MILLIS = 2000;
    /**
     * The constant SYSTEM_UI_HIDER_FLAGS.
     *
     * @deprecated not used anymore.
     */
    public static final int SYSTEM_UI_HIDER_FLAGS = 0;

    public static final String ONE_SIGNAL_ID_KEY = "onesignal_app_id";

    private boolean hasVisitedApp = false;
    /**
     * The current config.
     */
    private PvpConfig config;

    /**
     * The current user.
     */
    private PvpUser currentUser;

    /**
     * Checks if TV screen
     */
    private boolean isTvScreen;

    /**
     * Checks if TV has been proccessed
     */
    private boolean isTVProccessed;

    private String originalUserAgent;

    /**
     * The app packages.
     */
    private List<PvpPackage> packageList;

    /**
     * The Root Context.
     */
    @RootContext
    Context context;

    /**
     * The Shared preferences.
     */
    @Pref
    SharedPreferences_ sharedPreferences;

    /**
     * The REST service.
     */
    @RestService
    PvpRestService restService;

    /**
     * Init.
     */
    @AfterInject
    void init() {
        String configStr = sharedPreferences.pvpServerConfigStr().get();
        PvpConfig currentConfig = PvpConfig.parseFromString(configStr);
        this.config = currentConfig;

        String currentUserStr = sharedPreferences.currentUser().get();
        PvpUser currentUser = PvpUser.parseFromString(currentUserStr);
        this.currentUser = currentUser;

        this.hasVisitedApp = sharedPreferences.hasVisitedApp().get();
        sharedPreferences.hasVisitedApp().put(true);

        this.isTVProccessed = false;

        originalUserAgent = System.getProperty("http.agent");
    }

    public String getInstanceID() {
        String res = sharedPreferences.pvpInstanceId().get();
        if (res == null || res.equals("")) {
            Bundle bundle = new Bundle();
            String instanceId = bundle.getString(PvpHelper.PVP_CLIENT_PACKAGE_NAME + ".pvpInstanceId");
            sharedPreferences.pvpInstanceId().put(instanceId);
            res = instanceId;
        }
        return res;
    }

    public boolean isUsernameHint() {
        try {
            if (this.getConfig().user.containsKey("requireUserId")) {
                return (Boolean) this.getConfig().user.get("requireUserId");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return false;
    }

    public boolean isEmailValid(String email) {
        return email.trim().length() > 0 && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isEmpty(EditText editText) {
        return editText == null || editText.getText().toString().trim().length() == 0;
    }

    public int passwordValidation(String password, String passwordVerify) {
        final int PASSWORD_VALID = 1;
        final int PASSWORD_NOT_MATCH = 2;
        final int PASSWORD_TOO_SHORT = 3;
        final int PASSWORD_WITH_SPACES = 4;
        int result = PASSWORD_VALID;
        if (password.length() != passwordVerify.length()
                || !password.equals(passwordVerify)) {
            result = PASSWORD_NOT_MATCH;
        } else if (password.length() < 6) {
            result = PASSWORD_TOO_SHORT;
        } else if (password.trim().length() != password.length()) {
            result = PASSWORD_WITH_SPACES;
        }
        return result;
    }

    void updateUserAgent() {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;
            String userAgentPrefix = bundle.getString(PvpHelper.PVP_CLIENT_PACKAGE_NAME + ".userAgentPrefix");
            if (getCurrentUser() != null) {
                userAgentPrefix = userAgentPrefix + " " + getCurrentUser().id;
            }
            String currentUserAgent = System.getProperty("http.agent");
            String deviceType = PvpHelper.isEmulator() ? "Simulator" : "Device";
            String newUserAgent = userAgentPrefix + " " + deviceType + " " + originalUserAgent;
            if ((currentUserAgent.equals(originalUserAgent)) || (userAgentPrefix != null && userAgentPrefix.length() > 0 && !currentUserAgent.startsWith(userAgentPrefix))) {
                System.setProperty("http.agent", newUserAgent);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get config
     *
     * @return config
     */
    public PvpConfig getConfig() {
        return config;
    }

    /**
     * Set config
     *
     * @param config the config
     */
    public void setConfig(PvpConfig config) {
        this.config = config;
        if (config != null) {
            String configStr = this.config.toString();
            sharedPreferences.pvpServerConfigStr().put(configStr);
            loadPackages();
        }
    }

    /**
     * Gets current user.
     *
     * @return the current user
     */
    public PvpUser getCurrentUser() {
        return currentUser;
    }

    /**
     * Sets current user.
     *
     * @param currentUser the current user
     */
    public void setCurrentUser(PvpUser currentUser) {
        this.currentUser = currentUser;
        if (currentUser != null) {
            String userStr = this.currentUser.toString();
            sharedPreferences.currentUser().put(userStr);
        } else {
            sharedPreferences.currentUser().put("");
        }
    }

    private void loadPackages() {
        if (packageList == null || packageList.size() == 0) {
            try {
                String iid = sharedPreferences.pvpInstanceId().get();
                JsonNode result = restService.getFromCdn("package", iid);

                packageList = PvpPackage.parseListFromJsonNode(result.get("data"));
            } catch (RestClientException e) {

            }
        }
    }

    public void setAppPackages(List<PvpPackage> packs) {
        packageList = packs;
    }

    public List<PvpPackage> getAppPackages() {
        return packageList;
    }

    public List<PvpPackage> getLoginPackages() {
        List<PvpPackage> packs = new ArrayList<>();

        List<String> plans = this.getSubscriptionPlans();
        for (int i = 0; i < plans.size(); i++) {
            PvpPackage pack = getPackageforID(plans.get(i));
            if (pack != null) {
                packs.add(pack);
            }
        }
        return packs;
    }

    public PvpPackage getPackageforID(String packID) {

        for (int i = 0; i < packageList.size(); i++) {
            if (packageList.get(i).getAndroidProductId().equals(packID)) {
                return packageList.get(i);
            }
        }

        return null;
    }

    public void setOfflineMenu() {
        offlineMenu = true;
    }

    public String getStatsUrl() {
        String statsUrl = "https://videostats.panda-os.com/api_v3/index.php";
        try {
            if (this.getConfig().kaltura.containsKey("statsUrl")) {
                statsUrl = (String) this.getConfig().kaltura.get("statsUrl");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statsUrl;
    }

    public String getPlayerUiConfId() {
        String uiConfId = "";
        try {
            if (this.getConfig().player.containsKey("uiconfId")) {
                uiConfId = (String) this.getConfig().player.get("uiconfId");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return uiConfId;
    }

    public String getPartnerId() {
        String partnerId = "";
        try {
            if (this.getConfig().kaltura.containsKey("partnerId")) {
                partnerId = (String) this.getConfig().kaltura.get("partnerId");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return partnerId;
    }

    public String kalturaServiceUrl() {
        String serviceUrl = "";
        try {
            if (this.getConfig().kaltura.containsKey("serviceUrl")) {
                serviceUrl = (String) this.getConfig().kaltura.get("serviceUrl");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return serviceUrl;
    }

    public String kalturaCDNUrl() {
        String cdnUrl = null;
        try {
            if (this.getConfig().mobile.containsKey("cdnUrl")) {
                cdnUrl = (String) this.getConfig().mobile.get("cdnUrl");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (cdnUrl == null) {
            try {
                if (this.getConfig().kaltura.containsKey("cdnUrl")) {
                    cdnUrl = (String) this.getConfig().kaltura.get("cdnUrl");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (cdnUrl == null) {
            cdnUrl = kalturaServiceUrl();
        }

        return cdnUrl;
    }

    public String playerBeaconUrl() {
        String beaconUrl = null;
        try {
            if (this.getConfig().player.containsKey("beaconEnabled") && this.getConfig().player.get("beaconEnabled") != null && (Boolean) this.getConfig().player.get("beaconEnabled")) {
                beaconUrl = (String) this.getConfig().player.get("beaconUrl");
                if (beaconUrl != null && !beaconUrl.startsWith("http://") && !beaconUrl.startsWith("http://")) {
                    beaconUrl = "https:" + beaconUrl;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return beaconUrl;
    }

    /**
     * Gets application rsa public key.
     *
     * @return the application rsa public key
     */
    public String getApplicationRsaPublicKey() {
        String result = "";
        try {
            if (this.getConfig().purchases.containsKey("googleplay")) {
                HashMap purchaseServiceConfig = (HashMap) this.getConfig().purchases.get("googleplay");
                HashMap configProducts = (HashMap) purchaseServiceConfig.get("applicationRsaPublicKeys");
                result = (String) configProducts.get(context.getPackageName().replaceAll("\\.", "_"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Gets subscription plans.
     *
     * @return the subscription plans
     */
    public List<String> getSubscriptionPlans() {
        return getSubscriptionPlans(null);
    }

    /**
     * Gets subscription plans.
     *
     * @param service the service
     * @return the subscription plans
     */
    public List<String> getSubscriptionPlans(String service) {
        List<String> plans = new ArrayList<>();
        if (service == null) {
            service = "googleplay";
        }
        try {
            if (this.getConfig().purchases.containsKey(service.toLowerCase())) {
                HashMap purchaseServiceConfig = (HashMap) this.getConfig().purchases.get(service.toLowerCase());
                HashMap configProducts = (HashMap) purchaseServiceConfig.get("products");
                List<String> configPlans = (ArrayList<String>) configProducts.get("subscriptionPlans");
                plans.addAll(configPlans);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return plans;
    }

    public List<String> getPayPerViewProducts() {
        List<String> products = new ArrayList<>();

        try {
            if (this.getConfig().purchases.containsKey("googleplay")) {
                HashMap purchaseServiceConfig = (HashMap) this.getConfig().purchases.get("googleplay");
                HashMap configProducts = (HashMap) purchaseServiceConfig.get("products");
                List<String> configPlans = (ArrayList<String>) configProducts.get("ppv");
                products.addAll(configPlans);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return products;
    }

    /**
     * PvpLog function
     *
     * @param tag         the log tag
     * @param message     the log message
     * @param type        the log type
     * @param ignoreDebug the ignore debug flag
     */
    public static void pvpLog(String tag, Object message, logType type, boolean ignoreDebug) {
        if (PVP_CLIENT_DEBUG_ENABLED || ignoreDebug) {
            switch (type) {
                case info:
                    Log.i(tag, "" + message);
                    break;
                case debug:
                    Log.d(tag, "" + message);
                    break;
                case error:
                    Log.e(tag, "" + message);
                    break;
                default:
                    Log.d(tag, "" + message);
                    break;
            }
        }
    }

    public boolean isCanShowRegistrationUrlForm() {
        String registrationUrl = getRegistrationUrl();
        return registrationUrl != null && !registrationUrl.equals("")
                && ((!isAmazonDevice() && getSubscriptionPlans().size() == 0)
                || (isAmazonDevice() && getSubscriptionPlans("Amazon").size() == 0));
    }

    public String getRegistrationUrl() {
        String res = null;

        try {
            if (this.getConfig().mobile.containsKey("registrationUrl")) {
                res = this.getConfig().mobile.get("registrationUrl").toString();
            }
            if (!res.startsWith("http://") && !res.startsWith("https://")) {
                res = "http://" + res;
            }
        } catch (Exception e) {
        }

        return res;
    }

    public Typeface getCustomFont(@PvpFontWeight String weight) {
        return Typeface.createFromAsset(this.context.getAssets(), "fonts/custom-" + weight + ".ttf");
    }

    public boolean isCustomFontsEnabled() {
        boolean result = false;
        try {
            if (getConfig().mobile.containsKey("enableCustomFonts")) {
                result = Arrays.asList(context.getAssets().list("")).contains("fonts") &&
                        (Boolean) getConfig().mobile.get("enableCustomFonts");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public boolean isLoginWithUdidEnabled() {
        boolean result = false;

        try {
            if (this.getConfig().mobile.containsKey("loginWithUdid")) {
                result = (Boolean) this.getConfig().mobile.get("loginWithUdid");
            }
        } catch (Exception e) {

        }

        return result;
    }

    public boolean isPaidSubscriptionRequired() {
        boolean result = false;

        try {
            if (this.getConfig().mobile.containsKey("paidSubscriptionRequired")) {
                result = (Boolean) this.getConfig().mobile.get("paidSubscriptionRequired");
            }
        } catch (Exception e) {

        }

        return result;
    }

    public boolean isTvPurchaseEnabled() {
        boolean result = false;

        try {
            if (this.getConfig().mobile.containsKey("enableTvPurchases")) {
                result = (Boolean) this.getConfig().mobile.get("enableTvPurchases");
            }
        } catch (Exception e) {

        }

        return result;
    }

    public boolean isEnableTvRegistration() {
        boolean result = false;

        try {
            if (this.getConfig().mobile.containsKey("enableTvRegistration")) {
                result = (Boolean) this.getConfig().mobile.get("enableTvRegistration");
            }
        } catch (Exception e) {

        }

        return result;
    }

    public boolean isSearchEnabled() {
        boolean result = true;

        try {
            if (this.getConfig().mobile.containsKey("enableSearch")) {
                result = (Boolean) this.getConfig().mobile.get("enableSearch");
            }
        } catch (Exception e) {

        }

        return result;
    }

    public boolean isAllowPromoCode() {
        boolean result = false;

        try {
            if (this.getConfig().mobile.containsKey("allowPromoCode")) {
                result = (Boolean) this.getConfig().mobile.get("allowPromoCode");
            }
        } catch (Exception e) {
        }

        return result;
    }

    public boolean hideLogin() {
        boolean result = false;

        try {
            if (this.getConfig().mobile.containsKey("hideLogin")) {
                result = (Boolean) this.getConfig().mobile.get("hideLogin");
            }
        } catch (Exception e) {

        }

        return result;
    }

    public boolean hideMenuSettings() {
        boolean result = false;

        try {
            if (this.getConfig().mobile.containsKey("hideMenuSettings")) {
                result = (Boolean) this.getConfig().mobile.get("hideMenuSettings");
            }
        } catch (Exception e) {

        }

        return result;
    }

    public boolean isMenuSearchEnabled() {
        boolean result = true;

        try {
            result = this.getConfig().mobile.containsKey("enableMenuSearch") && (Boolean) this.getConfig().mobile.get("enableMenuSearch");
        } catch (Exception e) {

        }

        return result;
    }

    public String getMinimumVersion() {
        String result = "0";
        try {
            if (!isTvScreen()) {
                result = PvpMapHelper.getValueForKeyPath((HashMap) this.getConfig().mobile, "minimumVersion.android", "0");
            }
        } catch (Exception e) {
        }

        return result;
    }

    public String getAppStoreLink() {
        String result = null;
        if (!Build.MANUFACTURER.equals("Amazon")) {
            String keyPath = isTvScreen() ? "appStoreLink.androidtv" : "appStoreLink.android";
            result = PvpMapHelper.getValueForKeyPath((HashMap) this.getConfig().mobile, keyPath);
            if (result == null) {
                result = PvpMapHelper.getValueForKeyPath((HashMap) this.getConfig().mobile, "appStoreLink.universal");
            }
        }
        return result;
    }

    public boolean isGoogleIMAEnabled() {
        boolean result = true;
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
                result = playerConfig.containsKey("googleIMAEnabled") && (Boolean) playerConfig.get("googleIMAEnabled");
            }
        } catch (Exception e) {
        }

        return result;
    }

    public String getAdTagUrl() {
        String result = "";
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
            }
        } catch (Exception e) {
            return result;
        }
        try {
            if (isTvScreen() && playerConfig.containsKey("adTagUrlTV")) {
                result = playerConfig.get("adTagUrlTV").toString();
            }
        } catch (Exception e) {

        }
        try {
            if (result.length() == 0) {
                result = playerConfig.get("adTagUrl").toString();
            }
        } catch (Exception e) {

        }
        return result;
    }

    public int getSABannerID() {
        int result = -1;
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
            }
        } catch (Exception e) {
            return result;
        }
        try {
            if (!isTvScreen()) {
                result = Integer.parseInt(playerConfig.get("awesomeAdsBannerPlacementID_android").toString());
            }
        } catch (Exception e) {

        }
        return result;
    }

    public boolean isKidozEnable() {
        boolean result = false;
        try {
            if (this.getConfig().mobile.containsKey("kidozPubId") && this.getConfig().mobile.containsKey("kidozSecKey")) {
                int kidozPubId = (int) this.getConfig().mobile.get("kidozPubId");
                String kidozSecKey = (String) this.getConfig().mobile.get("kidozSecKey");
                result = kidozPubId > 0 && kidozSecKey != null && !kidozSecKey.equals("");
            }
        } catch (Exception e) {
        }

        return result;
    }

    public int getKidozPubId() {
        int result = -1;
        try {
            if (this.getConfig().mobile.containsKey("kidozPubId")) {
                int kidozPubId = (int) this.getConfig().mobile.get("kidozPubId");
                if (kidozPubId > 0) {
                    result = kidozPubId;
                }
            }
        } catch (Exception e) {
        }

        return result;
    }

    public boolean isKidozBannerGalleryEnabled() {
        boolean result = true;
        try {
            result = this.getConfig().mobile.containsKey("kidozBannerGalleryEnabled") && (Boolean) this.getConfig().mobile.get("kidozBannerGalleryEnabled");
        } catch (Exception e) {
        }

        return result;
    }

    public boolean isKidozInterstitialGalleryEnabled() {
        boolean result = true;
        try {
            result = this.getConfig().mobile.containsKey("kidozInterstitialGalleryEnabled") && (Boolean) this.getConfig().mobile.get("kidozInterstitialGalleryEnabled");
        } catch (Exception e) {
        }

        return result;
    }

    public boolean isKidozBannerPlayPageEnabled() {
        boolean result = true;
        try {
            result = this.getConfig().mobile.containsKey("kidozBannerPlayPageEnabled") && (Boolean) this.getConfig().mobile.get("kidozBannerPlayPageEnabled");
        } catch (Exception e) {
        }

        return result;
    }

    public boolean isKidozInterstitialPlayPageEnabled() {
        boolean result = true;
        try {
            result = this.getConfig().mobile.containsKey("kidozInterstitialPlayPageEnabled") && (Boolean) this.getConfig().mobile.get("kidozInterstitialPlayPageEnabled");
        } catch (Exception e) {
        }

        return result;
    }

    public String getKidozSecretKey() {
        String result = null;
        try {
            if (this.getConfig().mobile.containsKey("kidozSecKey")) {
                String kidozSecKey = (String) this.getConfig().mobile.get("kidozSecKey");
                if (kidozSecKey != null && !kidozSecKey.equals("")) {
                    result = kidozSecKey;
                }
            }
        } catch (Exception e) {
        }

        return result;
    }

    public boolean isSAVideoEnable() {
        boolean result = true;
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
                result = playerConfig.containsKey("awesomeAdsEnabled") && (Boolean) playerConfig.get("awesomeAdsEnabled");
            }
        } catch (Exception e) {
        }

        return result;
    }

    public int getSAVideoID() {
        int result = -1;
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
            }
        } catch (Exception e) {
            return result;
        }
        try {
            if (!isTvScreen()) {
                result = Integer.parseInt(playerConfig.get("awesomeAdsPrerollPlacementID_android").toString());
            }
        } catch (Exception e) {

        }
        return result;
    }

    public boolean isGoogleMobileAdsTagForChildDirectedTreatmentEnabled() {
        boolean result = true;
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
                result = playerConfig.containsKey("googleMobileAdsTagForChildDirectedTreatmentEnabled") && (Boolean) playerConfig.get("googleMobileAdsTagForChildDirectedTreatmentEnabled");
            }
        } catch (Exception e) {
        }

        return result;
    }

    public boolean isAwesomeAdsCloseButtonEnabled() {
        boolean result = true;
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
                result = playerConfig.containsKey("awesomeAdsCloseButtonEnabled") && (Boolean) playerConfig.get("awesomeAdsCloseButtonEnabled");
            }
        } catch (Exception e) {
        }

        return result;
    }

    public boolean isGoogleMobileAdsEnabled() {
        boolean result = false;
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
                result = playerConfig.containsKey("googleMobileAdsEnabled") && (Boolean) playerConfig.get("googleMobileAdsEnabled");
            }
        } catch (Exception e) {
        }

        return result;
    }

    public boolean isGalleryStickyGoogleMobileAdsEnabled() {
        boolean result = false;
        HashMap galleryBannerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("galleryStickyBanner")) {
                galleryBannerConfig = (HashMap) this.getConfig().mobile.get("galleryStickyBanner");
                result = galleryBannerConfig.containsKey("googleMobileAdsEnabled") && (Boolean) galleryBannerConfig.get("googleMobileAdsEnabled");
            }
        } catch (Exception e) {
        }

        return result;
    }

    public boolean isInternalBannerCloseButtonEnabled() {
        boolean result = false;
        HashMap galleryBannerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("galleryStickyBanner")) {
                galleryBannerConfig = (HashMap) this.getConfig().mobile.get("galleryStickyBanner");
                result = galleryBannerConfig.containsKey("internalBannerCloseButton") && (Boolean) galleryBannerConfig.get("internalBannerCloseButton");
            }
        } catch (Exception e) {
        }

        return result;
    }

    public String getGalleryStickyInternalBannerImage() {
        String result = "";
        HashMap galleryBannerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("galleryStickyBanner")) {
                galleryBannerConfig = (HashMap) this.getConfig().mobile.get("galleryStickyBanner");
                if (galleryBannerConfig.containsKey("internalBannerImage")) {
                    result = galleryBannerConfig.get("internalBannerImage").toString();
                }
            }
        } catch (Exception e) {
        }

        return result;
    }

    public String getGalleryStickyInternalBannerLink() {
        String result = "";
        HashMap galleryBannerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("galleryStickyBanner")) {
                galleryBannerConfig = (HashMap) this.getConfig().mobile.get("galleryStickyBanner");
                if (galleryBannerConfig.containsKey("internalBannerLink")) {
                    result = galleryBannerConfig.get("internalBannerLink").toString();
                }
            }
        } catch (Exception e) {
        }

        return result;
    }

    public String getGoogleMobileAdsAdUnitID() {
        String result = "";
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
            }
        } catch (Exception e) {
            return result;
        }
        try {
            if (!isTvScreen()) {
                result = playerConfig.get("googleMobileAdsAdUnitID_android").toString();
            }
        } catch (Exception e) {

        }
        return result;
    }

    public String getApplicationName() {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    public String getApplicationVersion() {
        PackageInfo pInfo = null;
        String version = "";
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } finally {
            return version;
        }
    }

    public String getMobileCountryCode() {
        String code = "";
        TelephonyManager tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String networkOperator = tel.getNetworkOperator();
        try {
            code = networkOperator.substring(0, 3);
        } catch (Exception e) {
            System.out.println("Failed to get mobile country code");
        }
        return code;
    }

    public String getMobileNetworkCode() {
        String code = "";
        TelephonyManager tel = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String networkOperator = tel.getNetworkOperator();
        try {
            code = networkOperator.substring(3);
        } catch (Exception e) {
            System.out.println("Failed to get mobile network code");
        }
        return code;
    }

    /**
     * Checks whether this device is a TV screen or TV Streamer.
     *
     * @return True or false, indicating whether this device is a TV screen or TV Streamer.
     */
    public boolean isTvScreen() {
        if (!isTVProccessed) {
            this.isTvScreen = false;

            UiModeManager uiModeManager = (UiModeManager) context.getSystemService(UI_MODE_SERVICE);
            if (uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION) {
                this.isTvScreen = true;
            }

            if (!this.isTvScreen && (context.getResources().getConfiguration().uiMode & Configuration.UI_MODE_TYPE_MASK) == Configuration.UI_MODE_TYPE_TELEVISION) {
                this.isTvScreen = true;
            }

            if (!this.isTvScreen && (context.getPackageManager().hasSystemFeature("com.google.android.tv")
                    || context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TELEVISION)
                    || context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_LEANBACK)
                    || !context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_TOUCHSCREEN))) {
                this.isTvScreen = true;
            }

            if (!this.isTvScreen) {
                boolean forceTVLayout = this.isTvScreen;
                try {
                    forceTVLayout = (Boolean) getBuildConfigValue(context, "forceTVLayout");

                    if (forceTVLayout) {
                        this.isTvScreen = true;
                    }
                } catch (Exception e) {

                }
            }

            if (!this.isTvScreen) {
                try {
                    ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
                    Bundle bundle = applicationInfo.metaData;
                    boolean forceTvLayoutStr = bundle.getBoolean(PvpHelper.PVP_CLIENT_PACKAGE_NAME + ".forceTVLayout", false);
                    if (forceTvLayoutStr) {
                        this.isTvScreen = true;
                    }
                } catch (Exception e) {
                }
            }

            this.isTVProccessed = true;
        }

        return this.isTvScreen;
    }

    public boolean isAmazonDevice() {
        return Build.MANUFACTURER.equals("Amazon");
    }

    public Locale getCurrentLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            return context.getResources().getConfiguration().locale;
        }
    }

    public static Object getBuildConfigValue(Context context, String fieldName) {
        try {
            Class<?> clazz = Class.forName(context.getPackageName() + ".BuildConfig");
            Field field = clazz.getField(fieldName);
            return field.get(null);
        } catch (Exception e) {
//            e.printStackTrace();
        }
        return null;
    }

    public boolean hasVisitedApp() {
        return hasVisitedApp;
    }

    public String getUrlFromMobileConfigs(String confName) {
        String result = null;

        try {
            if (this.getConfig().mobile != null) {
                result = (String) this.getConfig().mobile.get(confName);
            }
        } catch (Exception e) {

        }

        return result;
    }

    public boolean getBooleanFromMobileConfigs(String confName) {
        boolean result = false;

        try {
            if (this.getConfig().mobile != null) {
                result = (boolean) this.getConfig().mobile.get(confName);
            }
        } catch (Exception e) {

        }

        return result;
    }

    public String getLayoutIcon(String layoutType) {
        String res = null;
        HashMap<String, Object> node = (HashMap<String, Object>) this.getConfig().mobile.get("node");
        if (node != null) {
            ArrayList nodeLayouts = (ArrayList) node.get("layouts");
            if (nodeLayouts != null) {
                for (LinkedHashMap<String, String> layout : (ArrayList<LinkedHashMap<String, String>>) nodeLayouts) {
                    if (layout.containsKey("mobileType") && layout.get("mobileType").equals(layoutType)) {
                        if (layout.containsKey("icon")) {
                            res = layout.get("icon");
                        }
                        break;
                    }
                }
            }
        }
        return res;
    }

    public String getLayoutName(String layoutType) {
        String res = null;
        HashMap<String, Object> node = (HashMap<String, Object>) this.getConfig().mobile.get("node");
        if (node != null) {
            ArrayList nodeLayouts = (ArrayList) node.get("layouts");
            if (nodeLayouts != null) {
                for (LinkedHashMap<String, String> layout : (ArrayList<LinkedHashMap<String, String>>) nodeLayouts) {
                    if (layout.containsKey("mobileType") && layout.get("mobileType").equals(layoutType)) {
                        if (layout.containsKey("categoryName")) {
                            res = layout.get("categoryName");
                        }
                        break;
                    }
                }
            }
        }
        return res;
    }

    public boolean isReverseColorsLayout(String layoutType) {
        boolean res = false;
        HashMap<String, Object> node = (HashMap<String, Object>) this.getConfig().mobile.get("node");
        if (node != null) {
            ArrayList nodeLayouts = (ArrayList) node.get("layouts");
            if (nodeLayouts != null) {
                for (LinkedHashMap<String, String> layout : (ArrayList<LinkedHashMap<String, String>>) nodeLayouts) {
                    if (layout.containsKey("mobileType") && layout.get("mobileType").equals(layoutType)) {
                        if (layout.containsKey("categoryReverseColors")) {
                            return true;
                        }
                        break;
                    }
                }
            }
        }
        return res;
    }

    public String getLayoutNameIcon(String layoutType) {
        String res = null;
        HashMap<String, Object> node = (HashMap<String, Object>) this.getConfig().mobile.get("node");
        if (node != null) {
            ArrayList nodeLayouts = (ArrayList) node.get("layouts");
            if (nodeLayouts != null) {
                for (LinkedHashMap<String, String> layout : (ArrayList<LinkedHashMap<String, String>>) nodeLayouts) {
                    if (layout.containsKey("mobileType") && layout.get("mobileType").equals(layoutType)) {
                        if (layout.containsKey("categoryNameIcon")) {
                            res = layout.get("categoryNameIcon");
                        }
                        break;
                    }
                }
            }
        }
        return res;
    }

    public int getCategoryLayoutHeight(String layoutType) {
        int res = getDefaultCategoryLayoutHeight();
        HashMap<String, Object> node = (HashMap<String, Object>) this.getConfig().mobile.get("node");
        if (node != null) {
            ArrayList nodeLayouts = (ArrayList) node.get("layouts");
            if (nodeLayouts != null) {
                for (LinkedHashMap<String, Object> layout : (ArrayList<LinkedHashMap<String, Object>>) nodeLayouts) {
                    if (layout.containsKey("mobileType") && layout.get("mobileType").equals(layoutType)) {
                        if (layout.containsKey("categoryLayoutHeight")) {
                            res = (Integer) layout.get("categoryLayoutHeight");
                        }
                        break;
                    }
                }
            }
        }
        return res;
    }

    public int getDefaultCategoryLayoutHeight() {
        return 25;
    }

    public boolean isAllowSkip() {
        return getBooleanFromMobileConfigs("allowSkip");
    }

    public boolean isAllowShare() {
        return getBooleanFromMobileConfigs("allowShare");
    }

    public boolean isAllowCast() {
        return getBooleanFromMobileConfigs("allowCast");
    }

    public boolean isAllowBackgroundPlayback() {
        return getBooleanFromMobileConfigs("allowBackgroundPlayback");
    }

    public boolean isAmericanFormat() {
        return getBooleanFromMobileConfigs("americanDate");
    }

    public boolean isForgotPasswordEnabled() {
        return getUrlFromMobileConfigs("forgotPasswordUrl") != null;
    }

    public boolean isTermsOfUseEnabled() {
        return getUrlFromMobileConfigs("terms") != null;
    }

    public boolean isPrivacyPolicyEnabled() {
        return getUrlFromMobileConfigs("privacyPolicy") != null;
    }

    public boolean isFaqEnabled() {
        return getUrlFromMobileConfigs("faqUrl") != null;
    }

    public boolean isForceRtl() {
        return getBooleanFromMobileConfigs("forceRtl");
    }

    public boolean hasFreeTrial() {
        boolean result = false;

        try {
            result = this.getConfig().mobile.containsKey("hasFreeTrial") && (Boolean) this.getConfig().mobile.get("hasFreeTrial");
        } catch (Exception e) {

        }

        return result;
    }

    public boolean isAlwaysLive() {
        boolean isAlwaysLive = false;
        try {
            isAlwaysLive = this.getConfig().live.containsKey("isAlwaysLive") && (Boolean) this.getConfig().live.get("isAlwaysLive");
        } catch (Exception e) {

        }

        return isAlwaysLive;
    }

    public boolean isDrmEnabled() {
        boolean result = false;

        try {
            if (this.getConfig().drm != null) {
                result = (Boolean) this.getConfig().drm.get("enabled");
            }
        } catch (Exception e) {

        }

        return result;
    }

    public boolean isCountryAllowed() {
        boolean result = true;

        try {
            String currentCountry = sharedPreferences.currentCountry().get();
            String currentCountryGps = sharedPreferences.currentCountryGps().get();
            if (this.getConfig().application.containsKey("blockedCountries")) {
                List<String> blockedCountries = (List<String>) this.getConfig().application.get("blockedCountries");
                if (blockedCountries != null) {
                    for (String blockedCountry : blockedCountries) {
                        if (currentCountry != null) {
                            if (blockedCountry.toUpperCase().equals(currentCountry)) {
                                if (currentCountryGps == null || currentCountryGps.length() == 0 || blockedCountry.toUpperCase().equals(currentCountryGps)) {
                                    result = false;
                                    break;
                                }
                            }
                        } else if (currentCountryGps != null && currentCountryGps.length() > 0 && blockedCountry.toUpperCase().equals(currentCountryGps)) {
                            result = false;
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {

        }

        return result;
    }

    public long getServerTimeOffset() {
        if (sharedPreferences.serverTimeOffsetEnabled().get()) {
            return sharedPreferences.serverTimeOffset().get();
        }
        return 0;
    }

    public boolean poweredByEnabled() {
        return this.getConfig().mobile.containsKey("poweredBy") && (Boolean) this.getConfig().mobile.get("poweredBy");
    }

    public String googleAnalyticsTrackingId() {
        String result = null;
        try {
            if (!isTvScreen()) {
                result = PvpMapHelper.getValueForKeyPath((HashMap) this.getConfig().mobile, "analytics.google.trackingId", null);
            }
        } catch (Exception e) {
        }

        return result;
    }

    public static boolean isEmulator() {
        return Build.FINGERPRINT.startsWith("generic")
                || Build.FINGERPRINT.startsWith("unknown")
                || Build.MODEL.contains("google_sdk")
                || Build.MODEL.contains("Emulator")
                || Build.MODEL.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
                || "google_sdk".equals(Build.PRODUCT);
    }

    public static String getDeviceInfo() {
        String deviceType = PvpHelper.isEmulator() ? "Simulator" : "Device";
        return Build.MANUFACTURER + " " + Build.BRAND + " " + Build.MODEL + " " + Build.FINGERPRINT + " " + deviceType;
    }

    public boolean showStillWatchingDialog() {
        return showStillWatchingDialogDelay() > 0;
    }

    public int showStillWatchingDialogDelay() {
        int result = 0;

        try {
            result = (Integer) this.getConfig().mobile.get("stillWatchingDialogDelay");
        } catch (Exception e) {

        }
        return result * 1000;
    }

    /**
     * Open another app.
     *
     * @param packageName the full package name of the app to open
     * @return true if likely successful, false if unsuccessful
     */
    public boolean openApp(String packageName) {
        PackageManager manager = context.getPackageManager();
        try {
            Intent i = manager.getLaunchIntentForPackage(packageName);
            if (i == null) {
                return false;
                //throw new ActivityNotFoundException();
            }
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(i);
            return true;
        } catch (ActivityNotFoundException e) {
            return false;
        }
    }

    public String getRemoteControllerConfig(String keyName) {
        String result = "DEFAULT";
        HashMap remoteKeysConfig = null;
        try {
            if (this.getConfig().mobile != null) {
                remoteKeysConfig = (HashMap) this.getConfig().mobile.get("remoteKeys");
                if (isTvScreen()) {
                    result = remoteKeysConfig.get("KEYCODE_BACK").toString();
                }
            }
        } catch (Exception e) {

        }

        return result;
    }

    public HashMap<String, String> buildPopupFragmentItem(String type, String displayMode, String value, String label) {
        HashMap<String, String> item = new HashMap<>();
        item.put("type", type);
        item.put("displayMode", displayMode);
        item.put("value", value);
        item.put("label", label);
        return item;
    }

    public String convertDurationToTimeString(int durationInSeconds) {
        int mm, hh;
        String res = "";
        if (durationInSeconds / 360 > 0) {
            hh = durationInSeconds / 360;
            res = hh + ":";
            if (hh < 10) {
                res = "0" + res;
            }
        }
        durationInSeconds = durationInSeconds % 360;
        mm = durationInSeconds / 60;
        if (mm < 10) {
            res = res + "0" + mm + ":";
        } else {
            res = res + mm + ":";
        }

        durationInSeconds = durationInSeconds % 60;
        if (durationInSeconds < 10) {
            res = res + "0" + durationInSeconds;
        } else {
            res = res + durationInSeconds;
        }

        return res;
    }

    public void socialSharing(String network, String message, String url) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, message + " " + url);
//        share.putExtra(Intent.EXTRA_STREAM, message + " " + url);
        share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        boolean startActivity = false;
        switch (network) {
            case "whatsapp":
                share.setPackage("com.whatsapp");
                startActivity = true;
                break;
            case "facebook":
                share.setPackage("com.facebook.katana");
                startActivity = true;
                break;
            case "choose_app":
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{""});
                intent.putExtra(Intent.EXTRA_SUBJECT, message);
                intent.putExtra(Intent.EXTRA_TEXT, url);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                break;
            case "twitter":
                String twitterUrl = "http://www.twitter.com/intent/tweet?url=" + url + "&text=" + message;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(twitterUrl));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                break;
        }
        if (startActivity) {
            try {
                context.startActivity(share);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getShareSubject() {
        String res = "";
        if (this.getConfig().mobile.containsKey("shareSubject")) {
            res = (String) this.getConfig().mobile.get("shareSubject");
        }
        return res;
    }

    public String getShareText() {
        String res = "";
        if (this.getConfig().mobile.containsKey("shareText")) {
            res = (String) this.getConfig().mobile.get("shareText");
        }
        return res;
    }

    public int convertDpToPixel(float dp) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float px = dp * (metrics.density);
        return Math.round(px);
    }

    public int convertPixelsToDp(float px) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        float dp = px / (metrics.density);
        return Math.round(dp);
    }

    public int getScreenDimensions(String widthOrHeight) {
        DisplayMetrics metrics = Resources.getSystem().getDisplayMetrics();
        if (widthOrHeight.equals("width")) {
            return metrics.widthPixels;
        }
        return metrics.heightPixels;
    }

    public String getNewsletter() {
        String res = "";
        if (this.getConfig().mobile.containsKey("newsletterUrl")) {
            res = (String) this.getConfig().mobile.get("newsletterUrl");
        }
        return res;
    }

    public boolean isBranchEnabled() {
        boolean result = false;

        try {
            if (this.getConfig().mobile.containsKey("branch")) {
                result = (Boolean) this.getConfig().mobile.get("branch");
            }
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public boolean isForceUpdate() {
        boolean result = true;

        try {
            if (this.getConfig().mobile.containsKey("forceUpdate")) {
                result = (Boolean) this.getConfig().mobile.get("forceUpdate");
            }
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public boolean showFacebookComments() {
        boolean result = false;

        try {
            if (this.getConfig().mobile.containsKey("showFacebookComments")) {
                result = (Boolean) this.getConfig().mobile.get("showFacebookComments");
            }
        } catch (Exception e) {
            result = false;
        }

        return result;
    }

    public String getCmsBaseUrl() {
        String res = null;
        if (this.getConfig().mobile.containsKey("cmsBaseURL")) {
            res = (String) this.getConfig().mobile.get("cmsBaseURL");
        }
        return res;
    }

    public String getOfflinePackageId() {
        String res = null;
        if (this.getConfig().mobile.containsKey("offlinePackage")) {
            res = (String) this.getConfig().mobile.get("offlinePackage");
        }
        return res;
    }

    public String getCopyrightText() {
        String res = null;
        if (this.getConfig().mobile.containsKey("copyrightText")) {
            res = (String) this.getConfig().mobile.get("copyrightText");
        }
        return res;
    }

    public String getWebsiteText() {
        String res = null;
        if (this.getConfig().mobile.containsKey("websiteUrl")) {
            res = (String) this.getConfig().mobile.get("websiteUrl");
        }

        if (res == null || res.length() == 0) {
            return sharedPreferences.pvpServerBaseUrl().getOr("");
        }

        return res;
    }

    public String getFacebookAppId() {
        String res = null;
        if (this.getConfig().social.containsKey("facebook")) {
            Map<String, String> facebookConfig = (Map<String, String>) this.getConfig().social.get("facebook");
            if (facebookConfig.containsKey("applicationId")) {
                res = facebookConfig.get("applicationId");
            }
        }
        return res;
    }

    public String getLatestVersion() {
        String res = "";
        if (this.getConfig().mobile.containsKey("androidLatestVersion")) {
            res = (String) this.getConfig().mobile.get("androidLatestVersion");
        }
        return res;
    }

    public String getApkDownloadUrl() {
        String res = "";
        if (this.getConfig().mobile.containsKey("apkDownloadUrl")) {
            res = (String) this.getConfig().mobile.get("apkDownloadUrl");
        }
        return res;
    }

    public String getApkBaseUrl() {
        String res = "";
        if (this.getConfig().mobile.containsKey("apkBaseUrl")) {
            res = (String) this.getConfig().mobile.get("apkBaseUrl");
        }
        return res;
    }

    public boolean isTokenizationEnable() {
        boolean result = false;

        try {
            result = this.getConfig().mobile.containsKey("enableTokenization") && (Boolean) this.getConfig().mobile.get("enableTokenization");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public int getThemeType() {
        int res = PvpHelper.MOBILE_THEME_TYPE_BASIC;
        if (this.getConfig().mobile.containsKey("theme")) {
            HashMap<String, Object> theme = (HashMap<String, Object>) this.getConfig().mobile.get("theme");
            if (theme.containsKey("type")) {
                res = (int) theme.get("type");
            }
        }
        return res;
    }

    public String getBackgroundImage(String screenName) {
        String res = null;
        String keyName = "backgroundImage";
        if (screenName.equals("Login")) {
            keyName = keyName + "Login";
        } else if (screenName.equals("Registration")) {
            keyName = keyName + "Registration";
        }

        if (isTvScreen()) {
            if (this.getConfig().mobile.containsKey(keyName + "TV")) {
                res = (String) this.getConfig().mobile.get(keyName + "TV");
            }
        }

        if (res == null && this.getConfig().mobile.containsKey(keyName)) {
            res = (String) this.getConfig().mobile.get(keyName);
        }

        if (res == null && (keyName.equals("backgroundImageRegistration") || keyName.equals("backgroundImageLogin")
        )) {
            return res;
        }


        if (res == null && !keyName.equals("backgroundImage")) {
            keyName = "backgroundImage";
            if (isTvScreen()) {
                if (this.getConfig().mobile.containsKey(keyName + "TV")) {
                    res = (String) this.getConfig().mobile.get(keyName + "TV");
                }
            }

            if (res == null && this.getConfig().mobile.containsKey(keyName)) {
                res = (String) this.getConfig().mobile.get(keyName);
            }
        }

        return res;
    }

    public String getBlockingImage() {
        String res = null;
        String keyName = "blockingImage";
        if (this.getConfig().mobile.containsKey(keyName)) {
            res = (String) this.getConfig().mobile.get(keyName);
        }
        return res;
    }

    public String getBottomNavMenuBAckgroundImage() {
        String res = null;
        String keyName = "bottomNavMenuBackgroundImg";
        if (this.getConfig().mobile.containsKey(keyName)) {
            res = (String) this.getConfig().mobile.get(keyName);
        }

        return res;
    }

    public String getPaidRequiredMessage() {
        String res = null;
        if (this.getConfig().mobile.containsKey("paidRequiredMessage")) {
            res = (String) this.getConfig().mobile.get("paidRequiredMessage");
        }
        return res;
    }

    public String getExternalPaidUrlText() {
        String res = null;
        if (this.getConfig().mobile.containsKey("externalPaidUrl")) {
            res = (String) this.getConfig().mobile.get("externalPaidUrl");
        }
        return res;
    }

    public boolean disableRegistration() {
        boolean result = false;

        try {
            result = this.getConfig().mobile.containsKey("disableRegistration") && (Boolean) this.getConfig().mobile.get("disableRegistration");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public int getNavigationBarHeight() {
        int navigationBarHeight = 0;
        int id = context.getResources().getIdentifier("config_showNavigationBar", "bool", "android");
        if (id > 0 && context.getResources().getBoolean(id)) {
            int resourceId = context.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
            if (resourceId > 0) {
                navigationBarHeight = context.getResources().getDimensionPixelSize(resourceId);
            }
        }
        return navigationBarHeight;
    }

    /*  By default the Toolbar Title is set to visible even though no configs are set. To control the toolbar title visibility,
     'showToolbarTitle' config should be sent from server in mobile config with
      true or false value.*/
    public boolean showToolbarTitle() {
        boolean result = true;
        try {
            if (this.getConfig().mobile.containsKey("showToolbarTitle")) {
                result = (Boolean) this.getConfig().mobile.get("showToolbarTitle");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public Boolean isUseCdnApi() {
        boolean useCdnApi = false;

        if (this.getConfig() != null) {
            try {
                useCdnApi = this.getConfig().mobile.containsKey("useCdnApi") && (Boolean) this.getConfig().mobile.get("useCdnApi");

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return useCdnApi;
    }

    public Boolean isUseGalleryEntry() {
        boolean useGalleryEntry = false;

        try {
            useGalleryEntry = this.getConfig().mobile.containsKey("useGalleryEntry") && (Boolean) this.getConfig().mobile.get("useGalleryEntry");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return useGalleryEntry;
    }

    public Boolean isHistoryEnabled() {
        boolean isHistoryEnabled = false;

        try {
            isHistoryEnabled = this.getConfig().mobile.containsKey("historyEnabled") && (Boolean) this.getConfig().mobile.get("historyEnabled");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return isHistoryEnabled;
    }

    public Boolean isSwitchAccountEnabled() {
        boolean enableSwitchAccount = false;

        try {
            enableSwitchAccount = this.getConfig().mobile.containsKey("enableSwitchAccount") && (Boolean) this.getConfig().mobile.get("enableSwitchAccount");

        } catch (Exception e) {
            e.printStackTrace();
        }

        return enableSwitchAccount;
    }

    public int getTvThemeType() {
        int res = PvpHelper.TV_THEME_TYPE_GALLERY_PLAY;
        if (this.getConfig().mobile.containsKey("tvTheme")) {
            HashMap<String, Object> theme = (HashMap<String, Object>) this.getConfig().mobile.get("tvTheme");
            if (theme.containsKey("type")) {
                res = (int) theme.get("type");
            }
        }
        return res;
    }

    public String getForcePlayerType() {
        String result = "auto";
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
            }
        } catch (Exception e) {
            return result;
        }
        try {
            result = playerConfig.get("forceType").toString();
        } catch (Exception e) {

        }
        return result;
    }

    public Boolean getUpNextEnabled() {
        boolean upNextEnabled = false;
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
            }
            upNextEnabled = playerConfig != null && playerConfig.containsKey("upNextEnabled") && (Boolean) playerConfig.get("upNextEnabled");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return upNextEnabled;
    }

    public Boolean getCatchUpEnabled() {
        boolean catchUpEnabled = false;
        try {
            catchUpEnabled = this.getConfig().mobile.containsKey("catchUp")
                    && ((Boolean) this.getConfig().mobile.get("catchUp"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return catchUpEnabled;
    }

    public Boolean isEnableChannelMobileEpg() {
        return true;
    }

    public Integer getEpgTimeSlot() {
        Integer result = 0;
        try {
            if (getConfig().channels.containsKey("epgTimeSlot")) {
                result = (Integer) getConfig().channels.get("epgTimeSlot");
            }
        } catch (Exception e) {
            return result;
        }
        return result;
    }

    public String getLogoUrl() {
        try {
            return ((String) this.getConfig().branding.get("logoUrl"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public Boolean isHideChannelName() {
        boolean isHideChannel = false;
        try {
            isHideChannel = this.getConfig().channels.containsKey("hideChannelName")
                    && ((Boolean) this.getConfig().channels.get("hideChannelName"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isHideChannel;
    }


    public Boolean getOfflineModeEnabled() {
        boolean offlineMode = false;
        try {
            offlineMode = this.getConfig().mobile.containsKey("offlineMode")
                    && ((Boolean) this.getConfig().mobile.get("offlineMode"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return offlineMode;
    }

    public boolean isBottomPlayerEnabled() {
        try {
            List<HashMap<String, Object>> bottomNavMenu = getBottomNavMenu();
            if (bottomNavMenu != null) {
                for (HashMap<String, Object> menuItem : bottomNavMenu) {
                    if ("audio".equals(menuItem.get("type"))) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public HashMap<String, Object> getAudioMenu() {
        try {
            List<HashMap<String, Object>> bottomNavMenu = getBottomNavMenu();
            if (bottomNavMenu != null) {
                for (HashMap<String, Object> menuItem : bottomNavMenu) {
                    if ("audio".equals(menuItem.get("type"))) {
                        return menuItem;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    @Nullable
    public List<HashMap<String, Object>> getBottomNavMenu() {
        List<HashMap<String, Object>> bottomNavMenuItems = null;
        try {
            if (this.getConfig().mobile.containsKey("bottomNavMenu")) {
                bottomNavMenuItems = (List<HashMap<String, Object>>) this.getConfig().mobile.get("bottomNavMenu");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bottomNavMenuItems;
    }

    public String getEmbedPlayerSrc() {
        String result = "";
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
            }
        } catch (Exception e) {
            return result;
        }
        try {
            if (result.length() == 0) {
                result = playerConfig.get("embedSrc").toString();
            }
        } catch (Exception e) {

        }
        return result;
    }

    public String getEmbedPlayerBaseUrl() {
        String result = "";
        HashMap playerConfig = null;
        try {
            if (this.getConfig().mobile.containsKey("player")) {
                playerConfig = (HashMap) this.getConfig().mobile.get("player");
            }
        } catch (Exception e) {
            return result;
        }
        try {
            if (result.length() == 0) {
                result = playerConfig.get("baseUrl").toString();
            }
        } catch (Exception e) {

        }
        return result;
    }

    public String changeSettingsTitle() {
        String res = "Settings";

        try {
            if (this.getConfig().mobile.containsKey("changeSettingsTitle")) {
                String settingTitle = (String) this.getConfig().mobile.get("changeSettingsTitle");
                if (settingTitle != null && !settingTitle.equals("")) {
                    res = settingTitle;
                }

            }
        } catch (Exception e) {
        }

        return res;
    }

    public List<HashMap<String, Object>> getNavMenu() {
        List<HashMap<String, Object>> navItems = (List<HashMap<String, Object>>) this.getConfig().mobile.get("navMenuAndroid");
        if (navItems == null) {
            navItems = (List<HashMap<String, Object>>) this.getConfig().mobile.get("navMenu");
        }

        List<HashMap<String, Object>> res = new ArrayList<>();

        if (!offlineMenu) {
            for (HashMap<String, Object> curr : navItems) {
                try {
                    if (curr.containsKey("minVersionAndroid")) {
                        int minVer = (int) curr.get("minVersionAndroid");
                        PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                        int versionCode = pInfo.versionCode;
                        if (versionCode < minVer) {
                            continue;
                        }
                    }
                    res.add(curr);
                } catch (Exception e) {

                }
            }

            if (!this.hideMenuSettings()) {
                HashMap<String, Object> item = new HashMap<>();
                item.put("type", "settings");
                item.put("value", "settings");
                item.put("label", this.changeSettingsTitle());
                res.add(item);
            }

            if (this.getCurrentUser() != null && sharedPreferences.isLoggedIn().get()) {
                HashMap<String, Object> item = new HashMap<>();
                item.put("type", "logout");
                item.put("value", "logout");
                item.put("label", "Logout");
                res.add(item);
            } else if (!this.hideLogin()) {
                HashMap<String, Object> item = new HashMap<>();
                item.put("type", "login");
                item.put("value", "login");
                item.put("label", "Login");
                res.add(item);
            }

            if (this.getCopyrightText() != null) {
                HashMap<String, Object> item = new HashMap<>();
                item.put("type", "copyright");
                item.put("value", "copyright");
                item.put("label", this.getCopyrightText());
                res.add(item);
            }
        } else {
            HashMap<String, Object> item = new HashMap<>();
            item.put("type", "offline");
            item.put("value", "Offline");
            item.put("label", "Offline");
            res.add(item);
        }

        return res;
    }

    public boolean verifyUserPurchasePackage(ArrayList<String> packages) {
        boolean res = false;

        if (getCurrentUser() != null && sharedPreferences.isLoggedIn().get()) {

            if (userBypassPurchase()) {
                res = true;
            }
            String userPurchasesStr = sharedPreferences.userPurchases().get();
            List<PvpPurchase> pvpPurchaseList = PvpPurchase.getListFromString(userPurchasesStr);

            if (!res && pvpPurchaseList != null) {
                for (String categoryPackage : packages) {
                    for (PvpPurchase pvpPurchase : pvpPurchaseList) {
                        if (categoryPackage.equals(pvpPurchase.objectId) && pvpPurchase.status == 1) {
                            res = true;
                            break;
                        }
                    }
                    if (res) {
                        break;
                    }
                }
            }
        }

        return res;
    }

    public boolean userBypassPurchase() {
        if (getCurrentUser() != null && sharedPreferences.isLoggedIn().get()) {
            return currentUser.hasRole(PvpUser.userRole.managerRole) || currentUser.bypassPurchaseRestriction();
        }
        return false;
    }

    public String getPlayPageRelatedEntriesViewType() {
        String res = "category";

        try {
            if (this.getConfig().mobile.containsKey("playPageRelatedEntriesViewType")) {
                String viewType = (String) this.getConfig().mobile.get("playPageRelatedEntriesViewType");
                if (viewType != null && !viewType.equals("")) {
                    res = viewType;
                }

            }
        } catch (Exception e) {
        }

        return res;
    }

    public String getSearchViewType() {
        String res = "search";

        try {
            if (this.getConfig().mobile.containsKey("searchViewType")) {
                String viewType = (String) this.getConfig().mobile.get("searchViewType");
                if (viewType != null && !viewType.equals("")) {
                    res = viewType;
                }

            }
        } catch (Exception e) {}

        return res;
    }


    public boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}
