package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpUser;

import java.util.List;

/**
 * The {@link PvpUserModelCallback} provides an interface for responding to events from the {@link PvpUserModel}.
 */
public interface PvpUserModelCallback {

    /**
     * Triggered upon a successful API request that returned a {@link PvpUser} object.
     * @param user {@link PvpUser} object that has returned from the API request.
     */
    void userRequestSuccess(PvpUser user);

    /**
     * Triggered upon a successful API request that returned an array of {@link PvpUser} objects.
     * @param users {@link PvpUser} objects that were returned from the API request.
     */
    void userRequestSuccess(List<PvpUser> users);

    /**
     * Triggered upon a failed API request.
     */
    void userRequestFail();

}
