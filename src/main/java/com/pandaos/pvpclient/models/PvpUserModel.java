package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pandaos.pvpclient.objects.PvpEncryptedData;
import com.pandaos.pvpclient.objects.PvpResult;
import com.pandaos.pvpclient.objects.PvpUser;
import com.pandaos.pvpclient.utils.PvpMapHelper;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The {@link PvpUserModel} provides an interface for using the PVP user service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpUserModel extends PvpBaseModel {

    /**
     * Get user by id
     *
     * @param userId   the user ID we wish to retrieve.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getUser(String userId, PvpUserModelCallback callback) {
        try {
            JsonNode result = restService.get("user/" + userId, sharedPreferences.pvpInstanceId().get());
            PvpUser user = PvpUser.parseOneFromJsonNode(result.get("data"));

            if (user != null) {
                PvpHelper.pvpLog("USER_MODEL", user, PvpHelper.logType.info, false);

                callback.userRequestSuccess(user);
            } else {
                callback.userRequestFail();
            }

        } catch (RestClientException e) {
            callback.userRequestFail();
        }
    }

    public boolean getUser(String userId) {
        try {
            JsonNode result = restService.get("user/" + userId, sharedPreferences.pvpInstanceId().get());
            PvpUser user = PvpUser.parseOneFromJsonNode(result.get("data"));

            if (user != null) {
                PvpHelper.pvpLog("USER_MODEL", user, PvpHelper.logType.info, false);
                return false;
            } else {
                return true;
            }
        } catch (RestClientException e) {
            return true;
        }
    }

    /**
     * Get user by id
     *
     * @param userId   the user ID we wish to retrieve.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getIflixVoucher(String userId, PvpIflixModelCallback callback) {
        try {
            JsonNode result = restService.get("iflix/" + userId, sharedPreferences.pvpInstanceId().get());

            if (result != null) {
                PvpHelper.pvpLog("USER_MODEL", result, PvpHelper.logType.info, false);
                callback.voucherRequestSuccess(result);
            } else {
                callback.voucherRequestFail();
            }

        } catch (RestClientException e) {
            callback.voucherRequestFail();
        }
    }

    /**
     * Get current user
     *
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getCurrentUser(PvpUserModelCallback callback) {
        try {
            JsonNode result = restService.get("user/0", sharedPreferences.pvpInstanceId().get());
            PvpUser user = PvpUser.parseOneFromJsonNode(result.get("data"));

            if (user != null) {
                PvpHelper.pvpLog("USER_MODEL", user, PvpHelper.logType.info, false);
                pvpHelper.setCurrentUser(user);
                if (callback != null) {
                    callback.userRequestSuccess(user);
                }
            } else {
                if (callback != null) {
                    callback.userRequestFail();
                }
            }

        } catch (RestClientException e) {
            if (callback != null) {
                callback.userRequestFail();
            }
        }
    }

    /**
     * Registering a new user
     * PvpUser registerUser = new PvpUser(String email, String firstName, String lastName, String password, String passwordVerify);
     * userModel.registerUser(registerUser, Callback);
     *
     * @param user     the user to register.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void registerUser(PvpUser user, PvpUserModelCallback callback) {
        try {
            JsonNode result = restService.post("user", user);
            user = PvpUser.parseOneFromJsonNode(result.get("data"));

            if (user != null) {
                callback.userRequestSuccess(user);
            } else {
                callback.userRequestFail();
            }

        } catch (RestClientException e) {
            if (callback != null) {
                callback.userRequestFail();
            }
        }
    }

    /**
     * Register a new user with a private info object.
     *
     * @param user        the user to register.
     * @param privateInfo the user private info.
     * @param callback    the callback to trigger when finished.
     */
    @Background
    public void registerUser(PvpUser user, JsonNode privateInfo, PvpUserModelCallback callback) {
        try {
            ObjectMapper mapper = new ObjectMapper();
//            String infoJsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(privateInfo);
            JsonNode node = mapper.valueToTree(user);
            ((ObjectNode) node).put("privateInfo", privateInfo);

            JsonNode result = restService.post("user", node);
            user = PvpUser.parseOneFromJsonNode(result.get("data"));

            if (user != null) {
                if (callback != null) {
                    callback.userRequestSuccess(user);
                }
            } else {
                if (callback != null) {
                    callback.userRequestFail();
                }
            }

        } catch (Exception e) {
            if (callback != null) {
                callback.userRequestFail();
            }
        }
    }

    @Background
    public void recoverUserPassword(String email, PvpUserRecoverPasswordModelCallback callback) {
        try {
            restService.post("user/" + email + "/password", null);
            callback.passwordRecoveryRequestSuccess();
        } catch (Exception e) {
            callback.passwordRecoveryRequestFail();
        }
    }

    /**
     * Updating current logged in user
     * PvpUser registerUser = new PvpUser(String email, String firstName, String lastName, String password, String passwordVerify);
     * userModel.registerUser(registerUser, Callback);
     *
     * @param userUpdates user details to update.
     * @param callback    the callback to trigger when finished.
     */
    @Background
    public void updateUser(Map<String, String> userUpdates, PvpUserModelCallback callback) {
        try {
            JsonNode result = restService.put("user/0", userUpdates);
            PvpUser user = PvpUser.parseOneFromJsonNode(result.get("data"));

            if (user != null) {
                pvpHelper.setCurrentUser(user);
                callback.userRequestSuccess(user);
            } else {
                callback.userRequestFail();
            }

        } catch (RestClientException e) {
            if (callback != null) {
                callback.userRequestFail();
            }
        }
    }

    @Background
    public void finalizePurchase(HashMap<String, String> info, PvpUserPurchaseCallback callback) {
        try {
            PvpEncryptedData data = new PvpEncryptedData(context, info);
            ObjectMapper mapper = new ObjectMapper();
//            JsonNode node = mapper.valueToTree(data);
            ObjectNode node = mapper.createObjectNode();
            node.put("action", "purchase");
            node.put("data", data.data.get(0));

            JsonNode result = restService.post("user/0/purchase", sharedPreferences.pvpInstanceId().get(), node);

            ObjectNode resultData = (ObjectNode) result.get("data");
            if (resultData != null) {
                PvpResult results = PvpResult.parseOneFromJsonNode(resultData);
                if (results == null || "fail".equals(results.result)) {
                    if (callback != null) {
                        callback.userPurchaseFail();
                    }
                } else {
                    if (callback != null) {
                        callback.userPurchaseSuccess();
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            if (callback != null) {
                callback.userPurchaseFail();
            }
        }
    }

    @Background
    public void restorePurchases(ArrayList<String> purchasesData, PvpUserPurchaseRestoreCallback callback) {
        try {
            List<HashMap> purchases = new ArrayList<>();
            for (String purchaseData : purchasesData) {
                HashMap purchaseMap = new HashMap();
                HashMap<String, String> purchaseInfoMap = PvpMapHelper.jsonToMap(purchaseData);

                purchaseMap.put("productId", purchaseInfoMap.get("productId"));
                purchaseMap.put("userId", pvpHelper.getCurrentUser()._id.getMongoId());
                purchaseMap.put("service", pvpHelper.isAmazonDevice() ? "6" : "7");
                purchaseMap.put("info", purchaseInfoMap);
                purchases.add(purchaseMap);
            }
            restorePurchases(purchases, callback);

        } catch (Exception e) {
            callback.userPurchaseRestoreFail();
        }
    }

    @Background
    public void restorePurchases(List<HashMap> purchases, PvpUserPurchaseRestoreCallback callback) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            PvpEncryptedData data = new PvpEncryptedData(context, purchases);
            ObjectNode node = mapper.createObjectNode();
            ArrayNode purchasesNode = mapper.valueToTree(data.data);
            node.put("action", "restore");
            node.putArray("data").addAll(purchasesNode);
            JsonNode result = restService.post("user/0/purchase", node);

            if (callback != null) {
                callback.userPurchaseRestoreSuccess();
            }
        } catch (Exception e) {
            PvpHelper.pvpLog("USER_MODEL", e.getMessage(), PvpHelper.logType.error, true);
            if (callback != null) {
                callback.userPurchaseRestoreFail();
            }
        }
    }

    @Background
    public void ping(PvpUserPingCallback callback) {
        try {
            restService.post("user/0/ping", new HashMap<>());
            if (callback != null) {
                callback.userPingSuccess();
            }
        } catch (Exception e) {
            if (callback != null) {
                callback.userPingFail();
            }
        }
    }

    public interface PvpUserPurchaseCallback {

        /**
         * Entry purchase success
         */
        void userPurchaseSuccess();

        /**
         * Entry purchase fail
         */
        void userPurchaseFail();
    }

    public interface PvpUserPurchaseRestoreCallback {

        /**
         * Entry purchase success
         */
        void userPurchaseRestoreSuccess();

        /**
         * Entry purchase fail
         */
        void userPurchaseRestoreFail();
    }

    public interface PvpUserPingCallback {

        /**
         * Ping success
         */
        void userPingSuccess();

        /**
         * Ping fail
         */
        void userPingFail();

    }
}
