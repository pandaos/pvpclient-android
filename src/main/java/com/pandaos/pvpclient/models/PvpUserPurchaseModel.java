package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.objects.PvpCategory;
import com.pandaos.pvpclient.objects.PvpPackage;
import com.pandaos.pvpclient.objects.PvpPurchase;
import com.pandaos.pvpclient.objects.PvpResult;
import com.pandaos.pvpclient.utils.PvpConstants;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@link PvpUserPurchaseModel} provides an interface for using the PVP user service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpUserPurchaseModel extends PvpBaseModel {

    @Background
    public void getUserPurchasesTree(String expand, PvpUserPurchasesTreeCallback callback) {
        try {
            JsonNode result = restService.getUserPurchaseTree("user/0/purchasetree", "true", expand, sharedPreferences.pvpInstanceId().get());
            List<PvpCategory> pvpCategoryList = PvpCategory.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.userPurchaseTreeSuccess(new ArrayList<PvpCategory>(pvpCategoryList));
            }
        } catch (Exception e) {
            PvpHelper.pvpLog("Pvp_User_Purchase_Model", e.getMessage(), PvpHelper.logType.error, true);
            if (callback != null) {
                callback.userPurchaseTreeFail();
            }
        }
    }


    @Background
    public void getUserPurchase(PvpUserPurchasesCallback callback) {
        try {
            sharedPreferences.userPurchases().put("");
            JsonNode result = restService.getUserPurchase("user/0/purchase/", sharedPreferences.pvpInstanceId().get());
            sharedPreferences.userPurchases().put(result.toString());

            if (callback != null) {
                callback.userPurchaseCallback();
            }

        } catch (RestClientException e) {
            if(callback != null) {
                callback.userPurchaseCallback();
            }
        }
    }

    @Background
    public void verifyLoginPurchase(PvpUserLoginPurchaseVerifyCallback callback) {

        try {
            sharedPreferences.userPurchases().put("");
            JsonNode result = restService.getUserPurchase("user/0/purchase/", sharedPreferences.pvpInstanceId().get());
            sharedPreferences.userPurchases().put(result.toString());

            List<PvpPurchase> pvpPurchaseList = PvpPurchase.getListFromString(result.toString());

            if (pvpPurchaseList == null || pvpPurchaseList.size() == 0) {
                callback.userLoginPurchaseVerifyCallback(false);
                return;
            }

            String iid = sharedPreferences.pvpInstanceId().get();
            JsonNode packResult = restService.get("package", iid);

            pvpHelper.setAppPackages(PvpPackage.parseListFromJsonNode(packResult.get("data")));

            List<PvpPackage> packs = pvpHelper.getLoginPackages();

            for (int i = 0; i < packs.size(); i++) {
                for (int j = 0; j < pvpPurchaseList.size(); j++) {
                    if (packs.get(i)._id.getMongoId().equals(pvpPurchaseList.get(j).objectId) && pvpPurchaseList.get(j).status == 1) {
                        if (callback != null) {
                            callback.userLoginPurchaseVerifyCallback(true);
                            return;
                        }
                    }
                }
            }

            if (callback != null) {
                callback.userLoginPurchaseVerifyCallback(false);
            }

        } catch (RestClientException e) {
            if(callback != null) {
                callback.userLoginPurchaseVerifyFail();
            }
        }
    }


    @Background
    public void verifyPackagePurchased(String packageId, PvpVerifyPackagePurchasedCallback callback) {
        try {
            JsonNode result = restService.getUserPurchase("user/0/purchase/" + packageId, PvpConstants.PURCHASE_OBJECT_TYPE_PACKAGE, sharedPreferences.pvpInstanceId().get());
            JsonNode dataNode = result.get("data");
            if (dataNode != null) {
                PvpResult pvpResult = PvpResult.parseOneFromJsonNode(dataNode);
                if (callback != null) {
                    if (pvpResult != null) {
                        callback.verifyPackagePurchasedSuccess(Boolean.parseBoolean(pvpResult.result));
                    } else {
                        callback.verifyPackagePurchasedFail();
                    }
                }
            }
        } catch (RestClientException e) {
            if (callback != null) {
                callback.verifyPackagePurchasedFail();
            }
            e.printStackTrace();
        }
    }

    public interface PvpUserPurchasesCallback {

        /**
         * get user purchase success
         */
        void userPurchaseCallback();

    }

    public interface PvpUserLoginPurchaseVerifyCallback {

        /**
         * get user purchase success
         */
        void userLoginPurchaseVerifyCallback(Boolean isPurchased);
        void userLoginPurchaseVerifyFail();
    }

    public interface PvpUserPurchasesTreeCallback {

        /**
         * get user purchase success
         */
        void userPurchaseTreeSuccess(ArrayList<PvpCategory> pvpCategoryList);


        /**
         * get user purchase fail
         */
        void userPurchaseTreeFail();
    }

    public interface PvpVerifyPackagePurchasedCallback {

        void verifyPackagePurchasedSuccess(boolean isPurchased);

        void verifyPackagePurchasedFail();

    }

}
