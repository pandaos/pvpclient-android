package com.pandaos.pvpclient.models;

import com.pandaos.pvpclient.objects.PvpNode;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@link PvpNodeModelCallback} provides an interface for responding to events from the {@link PvpNodeModel}.
 */
public interface PvpNodeModelCallback {

    /**
     * Node request success.
     *
     * @param nodes the entries array.
     */
    void nodeRequestSuccess(ArrayList<PvpNode> nodes);

    /**
     * Node request success.
     *
     * @param node the entry.
     */
    void nodeRequestSuccess(PvpNode node);


    /**
     * Node request fail.
     */
    void nodeRequestFail();
}
