package com.pandaos.pvpclient.models;


import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.pandaos.pvpclient.services.PvpRestService;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.androidannotations.rest.spring.annotations.RestService;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * The {@link PvpBaseModel} provides an interface for using a service on the PVP system.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpBaseModel {

    /**
     * @name Private Properties
     */

    /**
     * The Pvp helper.
     */
    @Bean
    PvpHelper pvpHelper;

    /**
     * The REST service.
     */
    @RestService
    PvpRestService restService;

    /**
     * The Shared preferences.
     */
    @Pref
    SharedPreferences_ sharedPreferences;

    /**
     * The Root Context.
     */
    @RootContext
    Context context;

    public boolean useCdnApi = false;

    /**
     * @name Private Methods
     */

    /**
     * The init function, responsible for initializing the model.
     */
    @AfterInject
    void init() {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = applicationInfo.metaData;
            String url = bundle.getString(PvpHelper.PVP_CLIENT_PACKAGE_NAME + ".pvpServerBaseUrl");
            if (url != null && url.length() > 0) {
                restService.setRootUrl(url);
                sharedPreferences.pvpServerBaseUrl().put(url);
            }

            String cdnUrl = bundle.getString(PvpHelper.PVP_CLIENT_PACKAGE_NAME + ".pvpCdnBaseUrl");
            if (cdnUrl != null && url.length() > 0) {
                sharedPreferences.pvpCdnBaseUrl().put(cdnUrl);
            }

            if(sharedPreferences.pvpInstanceId() == null || sharedPreferences.pvpInstanceId().get().equals("")) {
                String instanceId = bundle.getString(PvpHelper.PVP_CLIENT_PACKAGE_NAME + ".pvpInstanceId");
                sharedPreferences.pvpInstanceId().put(instanceId);
            }

            useCdnApi = pvpHelper.isUseCdnApi() && sharedPreferences.pvpInstanceId().get() != null && !sharedPreferences.pvpInstanceId().get().equals("");

            pvpHelper.updateUserAgent();
        } catch (Exception e) {

        }
    }

    /**
     * Reloads the model.
     */
    public void reload() {
        init();
    }

    /**
     * Get a pager string by page number.
     *
     * @param page The page number.
     *
     * @return A new pager string for the given page number.
     */
    public String pager(int page) {
        JSONObject pager = new JSONObject();
        try {
            pager.put("pageIndex", page);
            pager.put("pageSize", PvpHelper.PAGE_SIZE);

            return pager.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }

}