package com.pandaos.pvpclient.models;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.pandaos.pvpclient.objects.PvpLiveEntry;
import com.pandaos.pvpclient.objects.PvpResult;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.client.RestClientException;

import java.util.ArrayList;
import java.util.List;

/**
 * The {@link PvpLiveEntryModel} provides an interface for using the live entry service.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpLiveEntryModel extends PvpBaseModel {

    /**
     * Gets live entries by category.
     *
     * @param category the category
     * @param page     the page
     * @param callback the callback
     */
    @Background
    public void getLiveEntriesByCategory(String category, int page, PvpLiveEntryModelCallback callback) {
        try {
            JsonNode result;
            if(useCdnApi) {
                result = restService.getFromCdn("category/" + category + "/entries", filter(), pager(page), sharedPreferences.pvpInstanceId().get());
            } else {
                result = restService.get("category/" + category + "/entries", filter(), pager(page), sharedPreferences.pvpInstanceId().get());
            }

            List<PvpLiveEntry> entryList = PvpLiveEntry.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.liveEntryRequestSuccess(entryList);
            }

        } catch (RestClientException e) {
            if (callback != null) {
                callback.liveEntryRequestFail();
            }
        }
    }

    /**
     * Gets live entries.
     *
     * @param page     the page
     * @param callback the callback
     */
    @Background
    public void getLiveEntries(int page, PvpLiveEntryModelCallback callback) {
        getLiveEntries(page, null, callback);
    }

    /**
     * Gets live entries.
     *
     * @param page     the page
     * @param callback the callback
     */
    @Background
    public void getLiveEntries(int page, String filter, PvpLiveEntryModelCallback callback) {
        try {
            JsonNode result;
            if(useCdnApi) {
                result = restService.getFromCdn("live", filter(filter), pager(page), sharedPreferences.pvpInstanceId().get());
            } else {
                result = restService.get("live", filter(filter), pager(page), sharedPreferences.pvpInstanceId().get());
            }
            List<PvpLiveEntry> entryList = PvpLiveEntry.parseListFromJsonNode(result.get("data"));

            if (callback != null) {
                callback.liveEntryRequestSuccess(entryList);
            }
        } catch (RestClientException e) {
            if(callback != null) {
                callback.liveEntryRequestFail();
            }
        }
    }

    /**
     * Get entry by id
     *
     * @param entryId the entry ID.
     * @param callback the callback to trigger when finished.
     */
    @Background
    public void getLiveEntry(String entryId, PvpLiveEntryModelCallback callback) {
        try {

            JsonNode result;
            if(useCdnApi) {
                result = restService.getFromCdn("entry/"+entryId, sharedPreferences.pvpInstanceId().get(), "");
            } else {
                result = restService.get("entry/"+entryId, sharedPreferences.pvpInstanceId().get());
            }

            PvpLiveEntry entry = PvpLiveEntry.parseOneFromJsonNode(result.get("data"));

            if(callback != null) {
                callback.liveEntryRequestSuccess(entry);
            }
        } catch (RestClientException e) {
            if(callback != null) {
                callback.liveEntryRequestFail();
            }
        }
    }

    /**
     * Create live entry.
     *
     * @param entry    the entry
     * @param callback the callback
     */
    @Background
    public void createLiveEntry(PvpLiveEntry entry, PvpLiveEntryModelCallback callback) {
        try{

            JsonNode result = restService.post("live", entry);
            entry = PvpLiveEntry.parseOneFromJsonNode(result.get("data"));

            if(callback != null) {
                callback.liveEntryCreateRequestSuccess(entry);
            }

        } catch (RestClientException e) {
            if(callback != null) {
                callback.liveEntryRequestFail();
            }
        }
    }

    @Background
    public void checkIfLive(String entryId, PvpLiveEntryModelCallback callback) {
        try{
            JsonNode result = restService.get("livepublish/" + entryId, sharedPreferences.pvpInstanceId().get());
            ObjectNode resultData = (ObjectNode) result.get("data");
            if(resultData != null) {
                PvpResult results = PvpResult.parseOneFromJsonNode(resultData);
                List<PvpResult> pvpResultList = new ArrayList<>();
                pvpResultList.add(results);
                if(callback != null) {
                    callback.liveEntryIsLiveRequestSuccess(pvpResultList);
                }
            }

        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }

    /**
     * Generate default filter string.
     *
     * @return the default filter string
     */
    String filter() {
        return filter(null);
    }

    /**
     * Generate default filter string.
     *
     * @return the default filter string
     */
    String filter(String filterString) {
        JSONObject filter = new JSONObject();
        long currentTime = (System.currentTimeMillis() / 1000L) - pvpHelper.getServerTimeOffset();
        try {
            filter.put("mediaTypeIn", 201);
            filter.put("startDateLessThanOrEqualOrNull", currentTime);
            filter.put("endDateGreaterThanOrEqualOrNull", currentTime);
            if(filterString == null) {
                filter.put("orderBy", "-createdAt");
            } else {
                //TODO: set values from filterString
            }
            return filter.toString();

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
