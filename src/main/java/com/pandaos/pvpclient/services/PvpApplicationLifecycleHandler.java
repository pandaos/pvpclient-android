package com.pandaos.pvpclient.services;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by orenkosto on 7/26/17.
 */
public class PvpApplicationLifecycleHandler implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

    public static final String BROADCAST_ACTION_APP_BACKROUND = "PvpAppBackgroundNotification";
    public static final String BROADCAST_ACTION_APP_FOREROUND = "PvpAppForegroundNotification";

    private static final String TAG = PvpApplicationLifecycleHandler.class.getSimpleName();

    private static boolean isInBackground = false;

    private Context context;

    public PvpApplicationLifecycleHandler(Context ctx) {
        super();
        this.context = ctx;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        if(isInBackground){
            Log.d(TAG, "app went to foreground");
            isInBackground = false;
            activity.sendBroadcast(new Intent(PvpApplicationLifecycleHandler.BROADCAST_ACTION_APP_FOREROUND));
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    @Override
    public void onTrimMemory(int level) {
        if(level == ComponentCallbacks2.TRIM_MEMORY_UI_HIDDEN){
            Log.d(TAG, "app went to background");
            isInBackground = true;
            if (context != null) {
                context.sendBroadcast(new Intent(PvpApplicationLifecycleHandler.BROADCAST_ACTION_APP_BACKROUND));
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

    }

    @Override
    public void onLowMemory() {

    }
}
