package com.pandaos.pvpclient.services;

import android.util.Log;

import com.pandaos.pvpclient.BuildConfig;
import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.models.SharedPreferences_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * The {@link PvpHttpRequestInterceptor} class intercepts all HTTP requests before they happen, in order to set our own headers, cookies, etc.
 * This class is managed by the {@link PvpRestService} and shouldn't be accessed directly.
 */
@EBean
public class PvpHttpRequestInterceptor implements ClientHttpRequestInterceptor {

    /**
     * The Shared preferences.
     */
    @Pref
    SharedPreferences_ sharedPreferences;

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        String debugCookie = sharedPreferences.debugEnabled().get() ? "XDEBUG_SESSION=PHPSTORM" : "";
        String token = sharedPreferences.token().get().replace("\"", "");
        String cookieString = token.length() > 0 ? "token=" + token + ";" + debugCookie : debugCookie;

        request.getHeaders().set("X-Requested-With", "XMLHttpRequest");
        request.getHeaders().set("X-Bamboo-Token", token + ";");
        request.getHeaders().set("bamboo-req", PvpHelper.getDeviceInfo());
        try {
            if (BuildConfig.DEBUG) {
                Log.e("HttpRequest", "----------" + request.getMethod().toString() + "----------");
                Log.e("HttpRequest", "--------------------");
                Log.e("HttpRequest", request.getURI().toString());
                Log.e("HttpRequest--Headers", request.getHeaders().toString());
                Log.e("HttpRequest--Body", request.toString());
                Log.e("HttpRequest", "-----------------------");
            }
        }catch (Exception e){

        }
        ClientHttpResponse response = execution.execute(request, body); //execute the HTTP request

        if (request.getURI().toString().indexOf("cdnapi.") < 0 && request.getURI().toString().indexOf("cdn.") < 0) {
            HttpHeaders headers = response.getHeaders(); //extract the response headers
            try {
                List<String> setCookieHeader = headers.get("Set-Cookie");
                if (setCookieHeader != null) {
                    for (String headerValue : setCookieHeader) {
                        if (headerValue.startsWith("token=")) {
                            String newToken = headerValue.replaceAll("token=", "");
                            String[] splitToken = newToken.split("; ");
                            sharedPreferences.token().put(splitToken[0]); //update the new token from the cookie
                            break;
                        }
                    }
                }
                List<String> dateHeader = headers.get("Date");
                if (dateHeader != null && dateHeader.size() > 0) {
                    String dateStr = dateHeader.get(0);
                    Date date = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", new Locale("en")).parse(dateStr);

                    Calendar.getInstance().setTime(date);
                    long time = Calendar.getInstance().getTimeInMillis();
                    long curr = System.currentTimeMillis();
                    long offset = curr - time; //Time difference in milliseconds
                    sharedPreferences.serverTimeOffset().put(offset / 1000L);
                }

                List<String> countryHeader = headers.get("X-Bamboo-Country");
                if (countryHeader != null && countryHeader.size() > 0) {
                    String country = countryHeader.get(0);
                    sharedPreferences.currentCountry().put(country.toUpperCase());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return response;
    }
}
