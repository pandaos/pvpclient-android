package com.pandaos.pvpclient.services;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.models.PvpTokenModel;
import com.pandaos.pvpclient.models.PvpUserModel;
import com.pandaos.pvpclient.objects.PvpConfig;
import com.pandaos.pvpclient.utils.PvpConstants;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.Timer;
import java.util.TimerTask;

@EBean(scope = EBean.Scope.Singleton)
public class PvpApplicationPingHandler implements  Application.ActivityLifecycleCallbacks {

    private Application application;
    private BroadcastReceiver loginReceiver;

    @Bean
    PvpHelper pvpHelper;
    @Bean
    PvpTokenModel pvpTokenModel;
    @Bean
    PvpUserModel pvpUserModel;
    @RootContext
    Context context;

    @Nullable
    private Timer pingTimer;
    @Nullable
    private TimerTask pingTimerTask;

    private int startedActivities = 0;

    @AfterInject
    void afterInject() {
        application = (Application) context.getApplicationContext();
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        startedActivities++;
        startPing();
        initLoginChangeBroadcastReceiver();
    }

    private void initLoginChangeBroadcastReceiver() {
        if (loginReceiver != null) {
            return;
        }
        loginReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                startPing();
            }
        };
        application.registerReceiver(loginReceiver, new IntentFilter(PvpConstants.BROADCAST_ACTION_USER_LOGIN_CHANGED));
    }

    private void startPing() {
        if (pvpTokenModel.isLoggedIn()) {
            int sessionKeepAlive = getSessionKeepAlive();
            if (sessionKeepAlive > 0) {
                startPingTimer(sessionKeepAlive);
            } else {
                cancelPingTimer();
            }
        } else {
            cancelPingTimer();
        }
    }

    private int getSessionKeepAlive() {
        PvpConfig pvpConfig = pvpHelper.getConfig();
        if (pvpConfig != null && pvpConfig.mobile.containsKey("sessionKeepAlive")) {
            Object sessionKeepAlive = pvpHelper.getConfig().mobile.get("sessionKeepAlive");
            if (sessionKeepAlive instanceof Integer) {
                return (int) sessionKeepAlive * 1000;
            }
        }
        return 0;
    }

    private void startPingTimer(int period) {
        if (pingTimer != null) {
            return;
        }
        pingTimer = new Timer();
        pingTimerTask = new TimerTask() {
            @Override
            public void run() {
                if (pingTimer != null) {
                    pvpUserModel.ping(null);
                }
            }
        };
        pingTimer.schedule(pingTimerTask, 0, period);
    }
    private void cancelPingTimer() {
        if (pingTimer != null) {
            pingTimer.cancel();
            pingTimer = null;
        }
    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {
        startedActivities--;
        unregisterLoginChangeReceiver();
        if (startedActivities == 0) {
            stopPing();
        }
    }

    private void stopPing() {
        cancelPingTimer();
    }

    private void unregisterLoginChangeReceiver() {
        if (startedActivities == 0 && loginReceiver != null) {
            application.unregisterReceiver(loginReceiver);
            loginReceiver = null;
        }
    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {
    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
    }
}
