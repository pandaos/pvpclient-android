package com.pandaos.pvpclient.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.pandaos.pvpclient.models.PvpHelper;

import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Delete;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Path;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Put;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

/**
 * The {@link PvpRestService} provides an interface for performing HTTP requests (GET, POST, PUT DELETE) and handling the responses.
 * This class is accessed by the different model classes in order to use different services of the platform.
 * <p>
 * In order to ensure correct library hierarchy, and that the API calls are sent and handled correctly, only the library should access and manage this class throughout the application's life cycle. This class should never be accessed directly by the developer using this library.
 */
@Rest(rootUrl = PvpHelper.PVP_SERVER_BASE_URL, converters = {MappingJackson2HttpMessageConverter.class}, interceptors = {PvpHttpRequestInterceptor.class}, responseErrorHandler = PvpHttpResponseErrorHandler.class)
@Accept(MediaType.APPLICATION_JSON)
public interface PvpRestService {

    /**
     * Sends a GET CONFIG HTTP request to the server
     *
     * @param path the query path
     * @param iid  instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&iid={iid}&uid={uid}&vcode={vcode}")
    JsonNode getConfig(@Path String path, @Path String iid, @Path String uid, @Path String vcode);

    /**
     * y
     * Sends a GET CONFIG HTTP request to the CDN server
     *
     * @param path the query path
     * @param iid  instanceId
     * @param uid  user Id
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&iid={iid}&uid={uid}&vcode={vcode}")
    JsonNode getConfigFromCdn(@Path String path, @Path String iid, @Path String uid, @Path String vcode);

    /**
     * Sends a GET HTTP request to the server
     *
     * @param path the query path
     * @param iid  instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&iid={iid}")
    JsonNode get(@Path String path, @Path String iid);

    /**
     * Sends a GET HTTP request to the server
     *
     * @param path the query path
     * @param iid  instanceId
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&iid={iid}")
    JsonNode getFromCdn(@Path String path, @Path String iid);

    /**
     * y
     * Sends a GET HTTP request to the server
     *
     * @param path the query path
     * @param iid  instanceId
     * @param uid  user Id
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&iid={iid}&uid={uid}")
    JsonNode getFromCdn(@Path String path, @Path String iid, @Path String uid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param filter the query params
     * @param pager  the query pager
     * @param iid    instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&filter={filter}&pager={pager}&iid={iid}")
    JsonNode get(@Path String path, @Path String filter, @Path String pager, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param filter the query params
     * @param pager  the query pager
     * @param iid    instanceId
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&filter={filter}&pager={pager}&iid={iid}")
    JsonNode getFromCdn(@Path String path, @Path String filter, @Path String pager, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param filter the query params
     * @param expand the query expand
     * @param iid    instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&filter={filter}&expand={expand}&iid={iid}")
    JsonNode getExpand(@Path String path, @Path String filter, @Path String expand, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param filter the query params
     * @param expand the query expand
     * @param iid    instanceId
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&filter={filter}&expand={expand}&iid={iid}")
    JsonNode getExpandFromCdn(@Path String path, @Path String filter, @Path String expand, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param filter the query params
     * @param iid    instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&channelFilter={filter}&channelExpand=entry&iid={iid}")
    JsonNode getChannelExpand(@Path String path, @Path String filter, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param filter the query params
     * @param iid    instanceId
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&channelFilter={filter}&channelExpand=entry&iid={iid}")
    JsonNode getChannelExpandFromCdn(@Path String path, @Path String filter, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param filter the query params
     * @param iid    instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&channelFilter={filter}&channelPager={pager}&channelExpand=entry&channelSort={sort}&iid={iid}&startDate={startDate}&endDate={endDate}")
    JsonNode getChannelExpandWithSort(@Path String path, @Path String filter, @Path String pager, @Path String sort, @Path String iid, @Path String startDate, @Path String endDate);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param filter the filter
     * @param pager  the pager
     * @param iid    instanceId
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&channelFilter={filter}&channelPager={pager}&channelExpand=entry&channelSort={sort}&iid={iid}&startDate={startDate}&endDate={endDate}")
    JsonNode getChannelExpandWithSortFromCdn(@Path String path, @Path String filter, @Path String pager, @Path String sort, @Path String iid, @Path String startDate, @Path String endDate);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param filter the query filter
     * @param expand the query expand
     * @param iid    instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&filter={filter}&expand={expand}&sort={sort}&iid={iid}")
    JsonNode getExpandWithSort(@Path String path, @Path String filter, @Path String expand, @Path String sort, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param filter the query filter
     * @param expand the query expand
     * @param sort   the query sort
     * @param iid    instanceId
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&filter={filter}&expand={expand}&sort={sort}&iid={iid}")
    JsonNode getExpandWithSortFromCdn(@Path String path, @Path String filter, @Path String expand, @Path String sort, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path        the query path
     * @param filter      the query filter
     * @param expand      the query expand
     * @param entryFilter the query entryFilter
     * @param iid         instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&filter={filter}&expand={expand}&entryFilter={entryFilter}&iid={iid}")
    JsonNode getCategoryTreeExpandWithSort(@Path String path, @Path String filter, @Path String expand, @Path Object entryFilter, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path        the query path
     * @param filter      the query filter
     * @param expand      the query expand
     * @param entryFilter the query entryFilter
     * @param iid         instanceId
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&filter={filter}&expand={expand}&entryFilter={entryFilter}&iid={iid}")
    JsonNode getCategoryTreeExpandWithSortFromCdn(@Path String path, @Path String filter, @Path String expand, @Path Object entryFilter, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path        the query path
     * @param historyTree the query purchasesTree
     * @param expand      the query expand
     * @param iid         instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&tree={historyTree}&expand={expand}&iid={iid}")
    JsonNode getUserHistoryTree(@Path String path, @Path String historyTree, @Path String expand, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path          the query path
     * @param purchasesTree the query purchasesTree
     * @param expand        the query expand
     * @param iid           instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&purchasesTree={purchasesTree}&expand={expand}&iid={iid}")
    JsonNode getUserPurchaseTree(@Path String path, @Path String purchasesTree, @Path String expand, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path the query path
     * @param type the query params
     * @param iid  instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&type={type}&iid={iid}")
    JsonNode getUserPurchase(@Path String path, @Path String type, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path the query path
     * @param iid  instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&iid={iid}")
    JsonNode getUserPurchase(@Path String path, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path        the query path
     * @param filter      the query filter
     * @param pager       the query pager
     * @param serviceType The service type enum of the entries (Kalture/Youtube/etc..)
     * @param iid         instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&serviceType={serviceType}&filter={filter}&pager={pager}&iid={iid}")
    JsonNode get(@Path String path, @Path String filter, @Path String pager, @Path String serviceType, @Path String iid);

    /**
     * Sends a GET HTTP request to the server, with query params
     *
     * @param path        the query path
     * @param filter      the query filter
     * @param pager       the query pager
     * @param serviceType The service type enum of the entries (Kalture/Youtube/etc..)
     * @param iid         instanceId
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&serviceType={serviceType}&filter={filter}&pager={pager}&iid={iid}")
    JsonNode getFromCdn(@Path String path, @Path String filter, @Path String pager, @Path String serviceType, @Path String iid);

    /**
     * Sends a GET HTTP request to the CDN server, with query params
     *
     * @param path   the query path
     * @param filter the query filter
     * @param sort   the query sort
     * @param iid    instance id
     * @param uid    user id
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&filter={filter}&sort={sort}&iid={iid}&uid={uid}")
    JsonNode getUserInstances(@Path String path, @Path String filter, @Path String sort, @Path String iid, @Path String uid);

    /**
     * Sends a GET HTTP request to the server
     *
     * @param path the query path
     * @param iid  instanceId
     * @return node the returned JSON response
     */
    @Get(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&iid={iid}&byUrl={byUrl}")
    JsonNode getNode(@Path String path, @Path String byUrl, @Path String iid);

    /**
     * y
     * Sends a GET HTTP request to the server
     *
     * @param path the query path
     * @param iid  instanceId
     * @param uid  user Id
     * @return node the returned JSON response
     */
    @Get("https://cdnapi.bamboo-video.com" + PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&iid={iid}&uid={uid}&byUrl={byUrl}")
    JsonNode getNodeFromCdn(@Path String path, @Path String byUrl, @Path String iid, @Path String uid);

    /**
     * Sends a POST HTTP request to the server, with query params
     *
     * @param path   the query path
     * @param object the object to send
     * @return node the returned JSON response
     */
    @Post(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX)
    JsonNode post(@Path String path, @Body Object object);

    @Post(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX + "&iid={iid}")
    JsonNode post(@Path String path, @Path String iid, @Body Object object);

    /**
     * Sends a PUT HTTP call to the server, with query params
     *
     * @param path    the query path
     * @param updated the object to update
     * @return node the returned JSON response
     */
    @Put(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX)
    JsonNode put(@Path String path, @Body Object updated);

    /**
     * Sends a DELETE HTTP call to the server, with query params
     *
     * @param path the query path
     * @return node the returned JSON object
     */
    @Delete(PvpHelper.PVP_SERVER_API_PREFIX + "/{path}" + PvpHelper.PVP_SERVER_API_SUFFIX)
    JsonNode delete(@Path String path);

    /**
     * Sets a custom request header.
     *
     * @param name  The header name.
     * @param value The header value.
     */
    void setHeader(String name, String value);

    /**
     * Gets a request header.
     *
     * @param name The header name.
     * @return the header
     */
    String getHeader(String name);

    /**
     * Sets a custom cookie.
     *
     * @param name  The cookie name.
     * @param value The cookie value.
     */
    void setCookie(String name, String value);

    /**
     * Gets a cookie.
     *
     * @param name The cookie name.
     * @return the header
     */
    String getCookie(String name);

    /**
     * Set the client root URL.
     *
     * @param rootUrl The new root URL to set.
     */
    void setRootUrl(String rootUrl);

    /**
     * Gets the client root URL.
     *
     * @return The client root URL.
     */
    String getRootUrl();
}

