package com.pandaos.pvpclient.utils;

import android.content.Context;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.pandaos.pvpclient.models.SharedPreferences_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.HashMap;
import java.util.TimeZone;

import tgio.rncryptor.RNCryptorNative;

/**
 * Created by orenkosto on 7/17/17.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpEncryptionHelper implements PvpNetworkStatusHelper.GetIpFromApiServiceCallback{
    @Pref
    SharedPreferences_ sharedPreferences;

    @RootContext
    Context context;
    @Bean
    PvpNetworkStatusHelper pvpNetworkStatusHelper;

    private String purchasesSharedSecret = "wxs8sDiHv41sCuLJ2REtRuCy9JMZOe9u";
    private String urlForTokenizing;
    private String devicePublicIp;
    private String TOKENIZATION_KEY = "5a30f50245475e72130fkjewhgqawhewbloiaw938y40-9238griufbn0op7q843m0ropr0789ym123o0u007c0";
    TokenizeUrlCallback tokenizeUrlCallback;

    @AfterInject
    void init() {
        purchasesSharedSecret = sharedPreferences.purchasesSharedSecret().get();
    }

    public boolean encryptPurchases() {
        return purchasesSharedSecret != null && purchasesSharedSecret.length() > 0;
    }

    public String encrypt(String plainText) {
        RNCryptorNative rnCryptor = new RNCryptorNative();
        String result = plainText;
        if (purchasesSharedSecret.length() > 0) {
            result = new String(rnCryptor.encrypt(plainText, purchasesSharedSecret));
        }
        return result;
    }

    public String encrypt(HashMap map) {
        String result = "";
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonFromMap = mapper.writeValueAsString(map);
            result = encrypt(jsonFromMap);
        } catch (Exception e) {

        }
        return result;
    }

    public void tokenizeUrl(String url, TokenizeUrlCallback callback) {
        try {
            tokenizeUrlCallback = callback;
            urlForTokenizing = url;
            if(devicePublicIp != null && !devicePublicIp.equals("")) {
                buildTokenizeUrl();
            } else {
                pvpNetworkStatusHelper.getIpFromUrl(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void buildTokenizeUrl() {
        String res;
        try {
            DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("M/d/y h:m:s a").withZone(DateTimeZone.UTC).withLocale(Locale.US);
            DateTime currentServerTime = new DateTime(DateTimeZone.UTC); // lets get localtime in UTC timezone
            String today = timeFormatter.print(currentServerTime);
            String validminutes = "1";
            String to_hash = devicePublicIp + TOKENIZATION_KEY + today + validminutes;
            byte[] ascii_to_hash = to_hash.getBytes("UTF-8");
            byte[] md5hash = DigestUtils.md5(ascii_to_hash);
            String base64hash = StringUtils.newStringUtf8(Base64.encodeBase64(md5hash, true));
            base64hash = base64hash.replace("\r\n","");
            String urlsignature = "server_time=" + today  + "&hash_value=" + base64hash + "&validminutes=" + validminutes;
            urlsignature = urlsignature.replace("\r\n","");
            String base64urlsignature = StringUtils.newStringUtf8(Base64.encodeBase64(urlsignature.getBytes("UTF-8"), true));
            base64urlsignature = base64urlsignature.replace("\r\n","");
            res = urlForTokenizing.contains("?") ? urlForTokenizing + "&" : urlForTokenizing + "?";
            res = res + "wmsAuthSign=" + base64urlsignature;

        } catch (Exception e) {
            e.printStackTrace();
            res = null;
        }

        tokenizeUrlCallback.tokenizeUrlCallback(res);
    }

    public void getIpFromUrlResponse(String response) {
        devicePublicIp = response;
        buildTokenizeUrl();
    }

    public interface TokenizeUrlCallback {
        void tokenizeUrlCallback(String tokenizeUrl);
    }

}
