package com.pandaos.pvpclient.utils;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;

import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.models.SharedPreferences_;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by orenkosto on 8/3/17.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpBeaconHelper {

    @RootContext
    Context context;

    @Bean
    PvpHelper pvpHelper;

    @Pref
    SharedPreferences_ sharedPreferences;

    Timer beaconRequestTimer;
    TimerTask beaconRequestTimerTask;

    String baseUrl;
    HashMap<String, String> params = new HashMap<>();

    HttpsURLConnection connection = null;

    @AfterInject
    void init() {
        baseUrl = pvpHelper.playerBeaconUrl();
        if (baseUrl != null) {
            if (baseUrl.startsWith("http://")) {
                baseUrl = baseUrl.replaceAll("http://", "https://");
            }
            params.put("sessionId", Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
            params.put("iid", pvpHelper.getConfig().currentInstanceId);
        }
    }

    public void start() {
        if (baseUrl != null) {
            stop();
            sendBeaconRequest(true);
            beaconRequestTimer = new Timer();
            beaconRequestTimerTask = new TimerTask() {
                @Override
                public void run() {
                    sendBeaconRequest();
                }
            };
            beaconRequestTimer.schedule(beaconRequestTimerTask, 60000, 60000);
        }
    }

    public void stop() {
        if (baseUrl != null) {
            if (beaconRequestTimer != null) {
                beaconRequestTimer.cancel();
                beaconRequestTimer = null;
            }
            if (beaconRequestTimerTask != null) {
                beaconRequestTimerTask.cancel();
                beaconRequestTimerTask = null;
            }
        }
    }

    private void sendBeaconRequest() {
        sendBeaconRequest(false);
    }

    @Background
    void sendBeaconRequest(boolean set) {
        if (this.baseUrl != null) {
            try {
                HashMap<String, String> requestParams = (HashMap<String, String>) this.params.clone();
                if (set) {
                    requestParams.put("set", "1");
                }
                requestParams.put("token", sharedPreferences.token().get());
                URL url = new URL(this.baseUrl + "?" + PvpMapHelper.urlParamsEncodeUTF8(requestParams));
                
                connection = (HttpsURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                //Do something with this InputStream
                // handle the response
                int status = connection.getResponseCode();
                // If response is not success
                if (status != 200) {
                    throw new IOException("Post failed with error code " + status);
                }
                String inputLine;
                boolean success = false;
                while ((inputLine = in.readLine()) != null) {
                    //consume response if any;
                    if (inputLine.equals("1")) {
                        System.out.println("Beacon success");
                        success = true;
                        break;
                    } else if (inputLine.equals("0")) {
                        System.out.println("Beacon failure");
                        success = false;
                        break;
                    }
                }
                in.close();
                
                if (success) {
                    context.sendBroadcast(new Intent(PvpConstants.BROADCAST_BEACON_SUCCESS));
                } else {
                    context.sendBroadcast(new Intent(PvpConstants.BROADCAST_BEACON_FAILURE));
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (connection != null)
                    connection.disconnect();
            }
        }
    }
}
