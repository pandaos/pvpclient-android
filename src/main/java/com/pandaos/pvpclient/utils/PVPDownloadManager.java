package com.pandaos.pvpclient.utils;

import android.app.DownloadManager;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.pandaos.pvpclient.R;
import com.pandaos.pvpclient.objects.PvpEntry;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;

/**
 * The {@link PVPDownloadManager} class provides media download functionality for the PVPClient library. Use this class to download media from Bamboo, as well as manage downloaded content.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PVPDownloadManager {
    private DownloadManager downloadManager;
    private NotificationManager notificationManager;
    static Hashtable<Long, PvpEntry> downloadsHashtable;
    private BroadcastReceiver receiverDownloadComplete;
    private String storageDirectoryPath;
    public File videoFiles[];
    public File imageFiles[];
    HashMap<String, PvpEntry> localEntriesHashMap;
    Context context;
    ObjectOutputStream outputStream;

    public OnDownloadingHashTableReady downloadingHashTableReadyListener;

    public static final int IMAGE_FILE = 1;
    public static final int VIDEO_FILE = 2;

    public static final String LOCAL_ENTRIES_FILENAME = "LocalEntries.data";
    public static final String DOWNLOADING_ENTRIES_FILENAME = "DownloadingEntries.data";
    /** We wanna make sure all the files  */

    public PVPDownloadManager(Context context) {
        this.context = context;
        PVPDownloadManager.downloadsHashtable = new Hashtable<>();
        this.localEntriesHashMap = new HashMap<>();
    }

    public void build(String storageDirectoryPath) {
        this.storageDirectoryPath = (storageDirectoryPath == null) ? "/Bamboo_Video" : storageDirectoryPath;
        this.downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        this.notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        this.getLocalEntries();
        this.getDownloadingEntries();
        this.refreshFiles();

        receiverDownloadComplete = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                PvpEntry entryToSave;
                if((entryToSave = getDownloadEntry(downloadId)) != null) {
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(downloadId);
                    Cursor cursor = downloadManager.query(query);

                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                    int status = cursor.getInt(columnIndex);

                    switch (status) {
                        case DownloadManager.STATUS_SUCCESSFUL:
                            Toast.makeText(context, context.getString(R.string.download_success), Toast.LENGTH_LONG).show();
                            //We remove the entry from the hashtable after successfully downloading the file
                            addEntryToLocalEntries(entryToSave);
                            downloadsHashtable.remove(downloadId);
                            saveDownloadingEntriesToFile();
                            break;
                        case DownloadManager.STATUS_PAUSED:
                            Toast.makeText(context, context.getString(R.string.download_paused), Toast.LENGTH_LONG).show();
                            break;
                        case DownloadManager.STATUS_PENDING:
                            Toast.makeText(context, context.getString(R.string.download_pending), Toast.LENGTH_LONG).show();
                            break;
                        case DownloadManager.STATUS_RUNNING:
                            Toast.makeText(context, context.getString(R.string.download_running), Toast.LENGTH_LONG).show();
                            break;
                        case DownloadManager.STATUS_FAILED:
                            Toast.makeText(context, context.getString(R.string.download_failed), Toast.LENGTH_LONG).show();
                            break;
                    }
                }
            }
        };
    }

    public void setDownloadingHashTableReadyListener(OnDownloadingHashTableReady onDownloadingHashTableReady) {
        this.downloadingHashTableReadyListener = onDownloadingHashTableReady;
    }

    public BroadcastReceiver getBroadcastReceiver() {
        return this.receiverDownloadComplete;
    }

    private PvpEntry getDownloadEntry (long downloadReference) {
        if(downloadsHashtable.containsKey(downloadReference)) {
            return downloadsHashtable.get(downloadReference);
        }
        return null;
    }
    @Background
    public void downloadEntry(PvpEntry entry) {
        if(!isEntryDownloaded(entry.id)) {
            this.localEntriesHashMap.put(entry.id, entry);
            saveLocalEntriesToFile();
            downloadImage(entry);
            downloadVideo(entry);
        }
    }

    public boolean isEntryDownloaded(String key) {
        return localEntriesHashMap.containsKey(key);
    }

    public static boolean isEntryDownloading(PvpEntry entry) {
        boolean result = false;
        Enumeration e = downloadsHashtable.keys();
        while (e.hasMoreElements()) {
            long i = (long) e.nextElement();
            PvpEntry downloadingEntry = downloadsHashtable.get(i);
            if (downloadingEntry.id.equals(entry.id)) {
                result = true;
            }
        }
        Log.d("IS_ENTRY_DOWNLOADING", result? "YES" : "NO");

        return result;
    }

    @Background
    void downloadVideo(PvpEntry entry) {
        String fileName =  entry.id + ".mp4";
        File f = getFile(fileName, VIDEO_FILE);
        if(f == null) {
            Uri downloadUri = entry.generateVideoDownloadURL(context, 4); //TODO: flavor asset id needs to come from the config
            final DownloadManager.Request requestVideo = new DownloadManager.Request(downloadUri);
            requestVideo.setDescription("Entry Video Download").setTitle(entry.name);
            requestVideo.setVisibleInDownloadsUi(true);
            requestVideo.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
//            requestVideo.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
//            requestVideo.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_MOBILE);
            requestVideo.allowScanningByMediaScanner();
            //TODO: Should check if there's a way to get the file type from the entry instead of keeping it hard coded.
            requestVideo.setDestinationInExternalPublicDir(storageDirectoryPath + "/Videos", fileName);
            // Push download request to queue and list it in the Hashtable.
            long downloadReference;
            downloadReference = downloadManager.enqueue(requestVideo);
            if(downloadReference != 0) {
                PVPDownloadManager.downloadsHashtable.put(downloadReference, entry);
            }
            if(downloadingHashTableReadyListener != null) {
                downloadingHashTableReadyListener.hashTableReady();
            }
            saveDownloadingEntriesToFile();
        }
    }

    @Background
    public void downloadImage(PvpEntry entry) {
        String fileName =  entry.id + ".jpg";
        File f = getFile(fileName, IMAGE_FILE);
        if(f == null){
            Uri downloadUri = entry.generateThumbnailURL(context, 800, 450, 3);
            final DownloadManager.Request requestImage = new DownloadManager.Request(downloadUri);
            requestImage.setDescription("Entry Image Download").setTitle(entry.name);
            //TODO: Should check if there's a way to get the file type from the entry instead of keeping it hard coded.
            requestImage.setDestinationInExternalPublicDir(storageDirectoryPath + "/Images", fileName);
            downloadManager.enqueue(requestImage);
        }
    }

    @Background
    void addEntryToLocalEntries(PvpEntry entry) {
        this.localEntriesHashMap.put(entry.id, entry);
        saveLocalEntriesToFile();
        //Update static local video files array.
        getLocalVideoFiles();
        getLocalImageFiles();
    }

    @Background
    void saveLocalEntriesToFile() {
        File outputFile = new File(Environment.getExternalStoragePublicDirectory(storageDirectoryPath).toString() + "/" + LOCAL_ENTRIES_FILENAME);
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream(outputFile));
            outputStream.writeObject(localEntriesHashMap);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Background
    void saveDownloadingEntriesToFile() {
        File outputFile = new File(Environment.getExternalStoragePublicDirectory(storageDirectoryPath).toString() + "/" + DOWNLOADING_ENTRIES_FILENAME);
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream(outputFile));
            outputStream.writeObject(downloadsHashtable);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void refreshFiles() {
        getLocalImageFiles();
        getLocalVideoFiles();
    }

    @Background
    void getLocalVideoFiles() {
        String path = Environment.getExternalStoragePublicDirectory(storageDirectoryPath+ "/Videos").toString();
        File f = new File(path);
        this.videoFiles = f.listFiles();
    }

    @Background
    void getLocalImageFiles() {
        String path = Environment.getExternalStoragePublicDirectory(storageDirectoryPath+ "/Images").toString();
        File f = new File(path);
        this.imageFiles = f.listFiles();
    }

    public HashMap<String, PvpEntry> getLocalEntries() {
        File inputFile = new File(Environment.getExternalStoragePublicDirectory(storageDirectoryPath).toString() + "/" + LOCAL_ENTRIES_FILENAME);
        if(inputFile.isFile()) {
            try {
                ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(inputFile));
                localEntriesHashMap = (HashMap<String, PvpEntry>) inputStream.readObject();
                inputStream.close();
            } catch (Exception e) {
                Log.e("PANDA_EXCEPTION", "Unable to get local entries - " + storageDirectoryPath + "/" + LOCAL_ENTRIES_FILENAME);
            }
        } else {
            Log.e("PANDA_EXCEPTION", "File doesn't exist in local files - " + storageDirectoryPath + "/" + LOCAL_ENTRIES_FILENAME);
        }

        return localEntriesHashMap;
    }

    private Hashtable <Long, PvpEntry> getDownloadingEntries() {
        File inputFile = new File(Environment.getExternalStoragePublicDirectory(storageDirectoryPath).toString() + "/" + DOWNLOADING_ENTRIES_FILENAME);

        if(inputFile.isFile()) {
            try {
                ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(inputFile));
                downloadsHashtable = (Hashtable<Long, PvpEntry>) inputStream.readObject();
                inputStream.close();
            } catch (Exception e) {
                Log.e("PANDA_EXCEPTION", "Unable to get local entries - " + storageDirectoryPath + "/" + DOWNLOADING_ENTRIES_FILENAME);
            }

            DownloadManager.Query query;
            Cursor cursor;
            ArrayList<Long> finishedDownloads = new ArrayList<>();

            /**
             * If no download in queue but for some reason downloadsHashTable isn't empty, we empty it.
             */
//            query = new DownloadManager.Query();
//            cursor = downloadManager.query(query);
//            if(cursor.getCount() == 0) {
//                downloadsHashtable.clear();
//                saveDownloadingEntriesToFile();
//            }

            query = new DownloadManager.Query();
            cursor = downloadManager.query(query);
            if(cursor.getCount() > 0) {
                for (Long key: downloadsHashtable.keySet()) {
                    query = new DownloadManager.Query();
                    query.setFilterById(key);
                    cursor = downloadManager.query(query);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
                    int status = cursor.getInt(columnIndex);
                    if(status == DownloadManager.STATUS_SUCCESSFUL) {
                        finishedDownloads.add(key);
                    }
                }
            }

            for (long downloadId: finishedDownloads) {
                downloadsHashtable.remove(downloadId);
            }

            saveDownloadingEntriesToFile();

        } else {
            Log.e("PANDA_EXCEPTION", "File doesn't exist in Downloading content - " + storageDirectoryPath + "/" + DOWNLOADING_ENTRIES_FILENAME);
        }

        return downloadsHashtable;
    }

    //TODO: fix this function!!!
    public void verifyAllFilesHaveBeenDownloaded() {
//        if(imageFiles == null || localEntriesHashMap.size() != imageFiles.length) {
//            for (PvpEntry entry : localEntriesHashMap.values()) {
//                downloadImage(entry);
//            }
//        }
//        if(videoFiles == null || localEntriesHashMap.size() != videoFiles.length) {
//            for (PvpEntry entry : localEntriesHashMap.values()) {
//                downloadVideo(entry);
//            }
//        }
    }

    public File getFile(String fileName, int fileType) {
        File[] filesToSearch = fileType == IMAGE_FILE ? this.imageFiles : this.videoFiles;
        if(filesToSearch != null) {
            for (File file : filesToSearch) {
                 if(file.getName().equals(fileName)) {
                     return file;
                 }
            }
        }
        return null;
    }

    public HashMap<String, PvpEntry> getLocalEntriesHashMap() {
        return this.localEntriesHashMap;
    }

    public interface OnDownloadingHashTableReady {
        void hashTableReady();
    }
}
