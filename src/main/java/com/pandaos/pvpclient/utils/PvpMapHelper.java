package com.pandaos.pvpclient.utils;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Created by orenkosto on 6/5/17.
 */
public class PvpMapHelper {

    public static String getValueForKeyPath(@NonNull HashMap map, @NonNull String keyPath){
        return getValueForKeyPath(map, keyPath, null);
    }

    public static String getValueForKeyPath(@NonNull HashMap map, @NonNull String keyPath, @Nullable String defaultValue){
        String result = defaultValue;
        List<String> keyPathComponents = new ArrayList<>(Arrays.asList(keyPath.split("\\.")));
        if (keyPathComponents.size() > 0) {
            if (keyPathComponents.size() == 1) {
                result = map.get(keyPathComponents.get(0)).toString();
            } else {
                try {
                    HashMap newMap = (HashMap) map.get(keyPathComponents.get(0));
                    keyPathComponents.remove(0);
                    String newKeyPath = TextUtils.join(".", keyPathComponents);

                    return getValueForKeyPath(newMap, newKeyPath, defaultValue);
                } catch (Exception e) {}
            }
        }

        return result;
    }

    public static HashMap<String, String> jsonToMap(String t) {
        HashMap<String, String> map = new HashMap<>();
        try {
            JSONObject jObject = new JSONObject(t);
            Iterator<?> keys = jObject.keys();

            while( keys.hasNext() ){
                String key = (String)keys.next();
                String value = jObject.getString(key);
                map.put(key, value);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return map;
    }

    static String urlEncodeUTF8(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedOperationException(e);
        }
    }
    static String urlParamsEncodeUTF8(Map<?,?> map) {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<?,?> entry : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(String.format("%s=%s",
                    urlEncodeUTF8(entry.getKey().toString()),
                    urlEncodeUTF8(entry.getValue().toString())
            ));
        }
        return sb.toString();
    }
}
