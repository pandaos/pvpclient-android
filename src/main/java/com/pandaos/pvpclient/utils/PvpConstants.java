package com.pandaos.pvpclient.utils;

/**
 * The PVPClient constants utility lass.
 */
public final class PvpConstants {

    /**
     * The Kaltura backend service (default). Entries will be retrieved from Bamboo/Kaltura, according to the inctance config.
     */
    public static final int ENTRYBACKEND_SERVICE_KALTURA = 0;

    /**
     * The YouTube backend service. Entries will be retrieved from YouTube if the instance is configured accordingly.
     */
    public static final int ENTRYBACKEND_SERVICE_YOUTUBE = 1;

    public static final String ANDROID_ATTRS_RES_SCHEMA = "http://schemas.android.com/apk/res-auto";

    public static final String BROADCAST_ACTION_USER_LOGIN_CHANGED = "userLoginChanged";
    public static final String BROADCAST_BEACON_SUCCESS = "BeaconSuccessBroadcast";
    public static final String BROADCAST_BEACON_FAILURE = "BeaconFailedBroadcast";

    public static final String PURCHASE_OBJECT_TYPE_ENTRY = "1";
    public static final String PURCHASE_SERVICE_AMAZON = "6";
    public static final String PURCHASE_SERVICE_GOOGLEPLAY = "7";
    public static final String PURCHASE_OBJECT_TYPE_INSTANCE = "6";
    public static final String PURCHASE_OBJECT_TYPE_PACKAGE = "8";

    public static final String DRM_SCHEME_WIDEVINE = "edef8ba9-79d6-4ace-a3c8-27dcd51d21ed";


    public static final String CONFIG_ONBOARDING="onboarding";
}
