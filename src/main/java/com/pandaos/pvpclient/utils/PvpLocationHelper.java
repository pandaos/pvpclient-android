package com.pandaos.pvpclient.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;

import com.pandaos.pvpclient.models.SharedPreferences_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.List;
import java.util.Locale;

/**
 * Created by orenkosto on 12/7/17.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpLocationHelper {

    private LocationManager locationManager;
    private Geocoder geocoder;
    public Context context;

    @Pref
    SharedPreferences_ sharedPreferences;

    LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            try {
                Geocoder listnerGeocoder = new Geocoder(context, Locale.getDefault());
                List<Address> addresses = listnerGeocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                sharedPreferences.currentCountryGps().put(addresses.get(0).getCountryCode());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) { }

        @Override
        public void onProviderEnabled(String provider) { }

        @Override
        public void onProviderDisabled(String provider) { }
    };

    private Location getCurrentLocation(Context context) {
        this.context = context;
        Location currLocation = null;
        try {
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M || context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);
                String provider = this.locationManager.getBestProvider(criteria, true);

                Boolean isProviderEnabled = this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

                if(isProviderEnabled && provider != null) {
                    if (!(ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
                        currLocation = locationManager.getLastKnownLocation(provider);
//                        locationManager.requestLocationUpdates(provider, 12000, 7, locationListenerGPS, Looper.getMainLooper());
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return currLocation;
    }

    public String getCurrentCountryCode(Context context) {
        String result = "";
        Location curLocation = getCurrentLocation(context);
        List<Address> addresses = null;
        this.geocoder = new Geocoder(context, Locale.getDefault());
        try {
            if (curLocation != null) {
                addresses = this.geocoder.getFromLocation(curLocation.getLatitude(), curLocation.getLongitude(), 1);
                result = addresses.get(0).getCountryCode();
            }
        } catch (Exception e) {
            if (addresses != null && !addresses.isEmpty()) {
                result = addresses.get(0).getCountryCode();
            }
        }

        return result;
    }
}
