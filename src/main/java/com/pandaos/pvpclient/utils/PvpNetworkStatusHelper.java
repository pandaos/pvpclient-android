package com.pandaos.pvpclient.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.androidannotations.annotations.EBean;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

/**
 * The {@link PvpNetworkStatusHelper} utility class provides an interface for reading the device network status at any given time.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpNetworkStatusHelper {

    private boolean isConnected;
    private int networkType;

    private ConnectivityManager cm;
    private WifiManager wm;
    private BroadcastReceiver networkChangeReceiver;
    Context context;

    /**
     * Instantiates a new Pvp network status helper.
     *
     * @param rootContext the root context
     */
//TODO: Maybe this implementation is problematic when supporting multiple contexts (activities)
    public PvpNetworkStatusHelper(Context rootContext) {
        context = rootContext;
        cm = (ConnectivityManager) rootContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        networkChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateNetworkStatus();
                if(isConnected()) {
                    Log.i("NETWORK", "Connected");
                    switch (getNetworkType()) {
                        case ConnectivityManager.TYPE_WIFI:
                            Log.i("NETWORK", "WIFI");
                            break;
                        case ConnectivityManager.TYPE_MOBILE:
                            Log.i("NETWORK", "Mobile");
                            break;
                    }
                } else {
                    Log.i("NETWORK", "Disconnected");
                }
            }
        };
        updateNetworkStatus();
    }

    /**
     * Gets the current network type.
     *
     * @return the network type.
     *
     * @see ConnectivityManager
     */
    public int getNetworkType() {
        return this.networkType;
    }

    /**
     * Checks whether the device is currently connected to the internet.
     *
     * @return true or false, depending on whether the device is connected to the internet.
     */
    public boolean isConnected() {
        return this.isConnected;
    }

    /**
     * Update network status.
     */
    public void updateNetworkStatus() {
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            isConnected = true;
            networkType = activeNetwork.getType();
        } else {
            // not connected to the internet
            isConnected = false;
            networkType = -1;
        }
    }

    /**
     * Checks whether the device is currently connected a WiFi network.
     *
     * @return true or false, depending on whether the device is connected to a WiFi network.
     */
    public boolean isConnectedToWifi() {
        updateNetworkStatus();
        return networkType == ConnectivityManager.TYPE_WIFI;
    }

    public static String getIpAddress() {
        return getIpAddress(true);
    }

    public static String getIpAddress(boolean useIPv4) {
        try {
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
                for (InetAddress addr : addrs) {
                    if (!addr.isLoopbackAddress()) {
                        String sAddr = addr.getHostAddress();
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        boolean isIPv4 = sAddr.indexOf(':')<0;

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr;
                        } else {
                            if (!isIPv4) {
                                int delim = sAddr.indexOf('%'); // drop ip6 zone suffix
                                return delim<0 ? sAddr.toUpperCase() : sAddr.substring(0, delim).toUpperCase();
                            }
                        }
                    }
                }
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }

    /**
     * Gets the {@link PvpNetworkStatusHelper} broadcast receiver.
     *
     * @return the broadcast receiver.
     */
    public BroadcastReceiver getBroadcastReceiver() {
        return this.networkChangeReceiver;
    }

    public void getIpFromUrl(final GetIpFromApiServiceCallback callback){
        RequestQueue queue = Volley.newRequestQueue(context);
        String url ="http://checkip.amazonaws.com/";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, response -> {
            response = response.replace("\n", "");
            callback.getIpFromUrlResponse(response);
        }, error -> callback.getIpFromUrlResponse(error.toString()));
        queue.add(stringRequest);
    }

    interface GetIpFromApiServiceCallback {
        void getIpFromUrlResponse(String response);
    }
}



