package com.pandaos.pvpclient.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;

import androidx.annotation.NonNull;

import com.pandaos.pvpclient.models.PvpHelper;
import com.pandaos.pvpclient.models.SharedPreferences_;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.Bidi;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/**
 * Created by orenkosto on 7/3/17.
 */
@EBean(scope = EBean.Scope.Singleton)
public class PvpLocalizationHelper {

    public static String PREFS_KEY_USER_LANGUAGE = "userLanguage";
    String PVP_LOCALIZATION_HELPER = "PVP_LOCALIZATION_HELPER";

    @Pref
    SharedPreferences_ sharedPreferences;

    @RootContext
    Context context;

    public String getLanguage() {
        String result = sharedPreferences.currentLanguage().get();
        if (result.equals("iw")) {
            result = "he";
        }
        if (result == null || result.length() == 0) {
            result = sharedPreferences.defaultLanguage().getOr("en");
        }

        return result;
    }

    public String getLanguageName() {
        return getLanguageName(getLanguage());
    }

    public String getLanguageName(@NonNull String language) {
        Locale locale = new Locale(language);
        String languageName = locale.getDisplayLanguage(locale);

        return languageName.substring(0, 1).toUpperCase() + languageName.substring(1);
    }

    public Locale getLocale() {
        Locale locale = new Locale(getLanguage());
        return locale;
    }

    public boolean isRtlEnabled() {
        return sharedPreferences.rtlEnabled().get();
    }

    public boolean isRtl() {
        String language = getLanguage();
        return language.contains("iw") || language.contains("he") || language.contains("ar");
    }

    public void setCurrentLanguage() {
        setLanguage(getLanguage());
    }

    public void setLanguage(@NonNull String language) {
        sharedPreferences.currentLanguage().put(language);
        PvpHelper.pvpLog(PVP_LOCALIZATION_HELPER, "Changing currentLanguage to: " + language, PvpHelper.logType.info, true);
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale = locale;
        context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());

        if (isRtl() && isRtlEnabled()) {
            Bidi bidi = new Bidi(language, Bidi.DIRECTION_DEFAULT_RIGHT_TO_LEFT);
            bidi.isRightToLeft();
        } else {
            Bidi bidi = new Bidi(language, Bidi.DIRECTION_DEFAULT_LEFT_TO_RIGHT);
            bidi.isLeftToRight();
        }

        setRtlIfNeeded();
        //TODO: post broadcast
    }

    public void setRtlIfNeeded() {
        if (isRtl() && isRtlEnabled()) {
            //TODO: change to RTL
        } else {
            //TODO: change to TLR
        }
    }

    public String localizedString(String s) {
        if(s != null && !s.equals("")) {
            JSONObject translationJson = loadTranslationJSON(getLanguage());
            try {
                Iterator<String> keysIterator = translationJson.keys();
                while (keysIterator.hasNext()) {
                    String key = keysIterator.next();
                    if (key.equalsIgnoreCase(s)) {
                        return translationJson.getString(key);
                    }
                }
            } catch (Exception e) {
            }
        }

        return s;
    }

    public List<String> availableLanguages() {
        List<String> languages = new ArrayList<>();
        languages.add("en");
        AssetManager assetManager = context.getAssets();
        try {
            for (String file : assetManager.list("")) {
                if (file.startsWith("translation-") && file.endsWith(".json")) {
                    String language = file.replaceAll("translation-", "");
                    language = language.replaceAll(".json", "");
                    if (!language.equals("en")) {
                        languages.add(language);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return languages;
    }

    private JSONObject loadTranslationJSON(@NonNull String language) {
        String jsonStr;
        JSONObject json = new JSONObject();
        try {
            InputStream is = context.getAssets().open("translation-" + language + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            jsonStr = new String(buffer, "UTF-8");
            json = new JSONObject(jsonStr);
        } catch (Exception e) {

        }

        return json;
    }
}
