package com.pandaos.pvpclient.utils;

import android.content.Context;

import com.pandaos.pvpclient.models.PvpHelper_;

/**
 * Created by orenkosto on 6/5/17.
 */

public class PvpAppUpdateHelper {

    public static boolean isUpdateAvailable(final Context context) {
        PvpHelper_ pvpHelper = PvpHelper_.getInstance_(context);
        String minimumVersion = pvpHelper.getMinimumVersion();
        String appVersion = pvpHelper.getApplicationVersion();
        if (versionCompare(appVersion, minimumVersion) < 0) {
            System.out.println("update available");
            return true;
        }
        return false;
    }

    private static int versionCompare(String str1, String str2) {
        String[] vals1 = str1.split("\\.");
        String[] vals2 = str2.split("\\.");
        int i = 0;
        // set index to first non-equal ordinal or length of shortest version string
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
            i++;
        }
        // compare first non-equal ordinal number
        if (i < vals1.length && i < vals2.length) {
            int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
            return Integer.signum(diff);
        }
        // the strings are equal or one string is a substring of the other
        // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
        return Integer.signum(vals1.length - vals2.length);
    }
}
