**The Official Android library for the Panda Video Platform.**

To install, add this line to your app's build.gradle:

    compile 'com.pandaos:pvpclient:0.1.7'